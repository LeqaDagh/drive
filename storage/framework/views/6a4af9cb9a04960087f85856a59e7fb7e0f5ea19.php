<div class="float-right row" id="addCar" class="addCar">
    <div class="col-lg-6">
        <label>الهيكل </label>
        <input style="border: 0 !important; box-shadow: none !important;background-color: white 
            !important;cursor: default !important;" disabled >
        <input id="bodyEdit" name="body" style="display:none; border: 0 !important; 
            box-shadow: none !important;background-color: white !important;cursor: default !important;"
        placeholder="<?php echo e(__('  ')); ?>" type="text" value="<?php echo e($vehicle[0]->body); ?>"
        data-parsley-required-message="   الهيكل مطلوب" disabled required>
    </div>
</div></br></br></br>
<div class="bodyType row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="hatchback card-body" style="">
                    <label>
                        <input class="bodyEdit" type="radio" name="bodyEdit" value="hat"
                        <?php echo e($vehicle[0]->body == 'hat' ? 'checked' : ''); ?> >
                        <img src="<?php echo e(url('/images/body-type/hatchback.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    hatchback
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="coupe card-body ">
                    <label>
                        <input class="bodyEdit" type="radio" name="bodyEdit" value="cou" 
                        <?php echo e($vehicle[0]->body == 'cou' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/coupe.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    coupe
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="sedan card-body ">
                    <label>
                        <input class="bodyEdit" type="radio" name="bodyEdit" value="sed" 
                        <?php echo e($vehicle[0]->body == 'sed' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/sedan.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    sedan
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="station card-body ">
                    <label>
                        <input class="bodyEdit" type="radio" name="bodyEdit" value="sta" 
                        <?php echo e($vehicle[0]->body == 'sta' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/station.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    station
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="sport-car card-body ">
                    <label>
                        <input class="bodyEdit" type="radio" name="bodyEdit" value="spo" 
                        <?php echo e($vehicle[0]->body == 'spo' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/sport-car.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    sport-car
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="cabriolet card-body ">
                    <label>
                        <input type="radio" name="bodyEdit" value="cab" 
                        <?php echo e($vehicle[0]->body == 'cab' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/cabriolet.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    cabriolet
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="pickup card-body ">
                    <label>
                        <input type="radio" name="bodyEdit" value="pic" 
                        <?php echo e($vehicle[0]->body == 'pic' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/pickup.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    pickup
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class=" card-stats">
            <center>
                <div class="off-road card-body ">
                    <label>
                        <input type="radio" name="bodyEdit" value="off" 
                        <?php echo e($vehicle[0]->body == 'off' ? 'checked' : ''); ?>>
                        <img src="<?php echo e(url('/images/body-type/off-road.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    off-road
                </div>
            </center>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="card-stats">
            <center>
                <div class="station-wagon card-body ">
                    <label>
                        <input type="radio" name="bodyEdit" value="stw"
                        <?php echo e($vehicle[0]->body == 'stw' ? 'checked' : ''); ?> >
                        <img src="<?php echo e(url('/images/body-type/station-wagon.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    station-wagon
                </div>
            </center>
        </div>
    </div>
</div></br><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/more/body-type-edit.blade.php ENDPATH**/ ?>