<div class="float-right row" id="addCar" class="addCar">
    <div class="col-md-12">
        <label>الهيكل </label>
        <input style="border: 0 !important; box-shadow: none !important;background-color: white 
            !important;cursor: default !important;" disabled >
        <input id="body" name="body" style="display:none; border: 0 !important; 
            box-shadow: none !important;background-color: white !important;cursor: default !important;"
        placeholder="<?php echo e(__('  ')); ?>" type="text" data-parsley-required-message="   الهيكل مطلوب"
        disabled required>
    </div>
</div></br></br></br>
<div class="bodyType justify-content-around flex-wrap row ">
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="hatchback card-body" style="">
                    <label>
                        <input class="bodyType" type="radio" name="test" value="hat" >
                        <img src="<?php echo e(url('/images/body-type/hatchback.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    hatchback
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="coupe card-body ">
                    <label>
                        <input class="bodyType" type="radio" name="test" value="cou" >
                        <img src="<?php echo e(url('/images/body-type/coupe.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    coupe
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="sedan card-body ">
                    <label>
                        <input class="bodyType" type="radio" name="test" value="sed" >
                        <img src="<?php echo e(url('/images/body-type/sedan.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    sedan
                </div>
            </center>
        </div>
    </div>

    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="station card-body ">
                    <label>
                        <input class="bodyType" type="radio" name="test" value="sta" >
                        <img src="<?php echo e(url('/images/body-type/station.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    station
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="sport-car card-body ">
                    <label>
                        <input class="bodyType" type="radio" name="test" value="spo" >
                        <img src="<?php echo e(url('/images/body-type/sport-car.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    sport-car
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="cabriolet card-body ">
                    <label>
                        <input type="radio" name="test" value="cab" >
                        <img src="<?php echo e(url('/images/body-type/cabriolet.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    cabriolet
                </div>
            </center>
        </div>
    </div>

    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="pickup card-body ">
                    <label>
                        <input type="radio" name="test" value="pic" >
                        <img src="<?php echo e(url('/images/body-type/pickup.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    pickup
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class=" card-stats">
            <center>
                <div class="off-road card-body ">
                    <label>
                        <input type="radio" name="test" value="off" >
                        <img src="<?php echo e(url('/images/body-type/off-road.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    off-road
                </div>
            </center>
        </div>
    </div>
    <div  class="col-lg-4 justify-content-around bodySmallScreen">
        <div class="card-stats">
            <center>
                <div class="station-wagon card-body ">
                    <label>
                        <input type="radio" name="test" value="stw" >
                        <img src="<?php echo e(url('/images/body-type/station-wagon.png')); ?>" width=90>
                    </label>
                </div>
                <div class="stats">
                    station-wagon
                </div>
            </center>
        </div>
    </div>
</div>
</br>
<script>
    $(window).resize(function() {
        if ($(this).width() < 500) {
            $('.bodySmallScreen').removeClass( "col-lg-4 justify-content-around " ).addClass( "row-lg-3 " );
        } else if ($(this).width() > 500) {
            $('.bodySmallScreen').removeClass( "row-lg-3" ).addClass( "col-lg-4 justify-content-around " );
        }
    });

</script><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/vehicle/body-type.blade.php ENDPATH**/ ?>