

<?php $__env->startSection('content'); ?>

<style>
input[type='range'] {
  width: 93%;
  height: 30px;
  overflow: hidden;
  cursor: pointer;
    outline: none;
    
    border-radius: 5px;
}
.form-group input[type='text'] {
    font-size:14px
}
.form-group input[type='number'] {
    font-size:14px
}
input[type='range'],
input[type='range']::-webkit-slider-runnable-track,
input[type='range']::-webkit-slider-thumb {
  -webkit-appearance: none;
    background: none;
}
input[type='range']::-webkit-slider-runnable-track {
  width: 200px;
  height: 8px;
  background: #003D7C;
}

input[type='range']:nth-child(2)::-webkit-slider-runnable-track{
  background: none;
}

input[type='range']::-webkit-slider-thumb {
  position: relative;
  height: 21px;
  width: 21px;
  margin-top: -7px;
  background: #fff;
  border: 1px solid #003D7C;
  border-radius: 25px;
  z-index: 1;
}


input[type='range']:nth-child(1)::-webkit-slider-thumb{
  z-index: 2;
}

.rangeslider{
    position: relative;
    height: 80px;
    width: 100%;
    display: inline-block;
    margin-top: -10px;
    margin-left: 20px;
}
.rangeslider input{
    position: absolute;
}
.rangeslider{
    position: absolute;
}

.rangeslider span{
    position: absolute;
    margin-top: -15px;
    left: 0;
}

.rangeslider input[type='text']{
    position: absolute;
    margin-top: -45px;
    left: 0;
}
.rangeslider .right{
   position: relative;
   float: right;
   margin-right: -5px;
}



</style>


    <div id="" class="row" > 
        <div class="col-lg-2 col-md-2 col-sm-2">
            <a id="resetSearch" style="border-radius: 20px;color:white" class="toggle btn btn-danger btn-block" >
                <span> بحث جديد </span>
            </a>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <form id="searchForm" class="form" method="POST" action="<?php echo e(route ('vehicle.search')); ?>"novalidate>
            <?php echo csrf_field(); ?>
                <div class="card ">
                    <div class="card-header ">
                        <center>
                            <h5>
                             البحث
                            </h5>
                        </center>
                    </div>
                    <div class="card-body ">

                        

                        <?php echo $__env->make('pages.search.logos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
                        <?php echo $__env->make('pages.search.body-type', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
                        </br>
                        <div class="d-flex d-row">
                            <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                                
                            </div>
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="form-group">
                                    <select class="form-control" style="font-size:13px" name="search_option">
                                    <option selected disabled>خيارات البحث</option>
                                    <option value="ascPrice">السعر تصاعدي</option>
                                    <option value="desPrice">السعر تنازلي</option>
                                    <option value="ascYear">السنة تصاعدي</option>
                                    <option value="desYear">السنة تنازلي</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="form-group">
                                    <select id="multiSelectpicker" class="selectpicker " name="region_id[]"  multiple data-live-search="true"
                                            title="المنطقة" style="text-align:right">
                                        <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                            
                            <input name="all_model" id="all_model" type="text" hidden>

                            </div>
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                            
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__(' سنة الانتاج')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input  id="range_min" name="range_year_from" class="range_min light left" type="text" placeholder="الكل" >
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <span class="">الى</span>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input id="range_max" name="range_year_to" class="range_max light right"type="text" placeholder="الكل" >
                                        </div> 
                                    </div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <div class="rangeslider">
                                                <input class="min"  type="range" min="1990" 
                                                    max="2020" value="1990" data-id="range_1"/>
                                                <input class="max"  type="range" min="1990" 
                                                    max="2020" value="2020" data-id="range_1"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('  السعر')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input  id="range_min_price" name="range_price_min" class="range_min_price light left" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <span class="">الى</span>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input id="range_max_price" name="range_price_max" class="range_max_price light right" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <div class="rangeslider">
                                                <input id="min_price" class="min_price"  type="range" min="0" 
                                                    max="400000" value="0" data-id="range_price"/>
                                                <input id="max_price" class="max_price"  type="range" min="0" 
                                                    max="400000" value="400000" data-id="range_price"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('  القوة')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input  id="range_min_power" name="range_power_min"  class="range_min_power light left" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <span class="">الى</span>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input id="range_max_power" name="range_power_max" class="range_max_power light right" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <div class="rangeslider">
                                                <input id="min_power" class="min_power" 
                                                type="range" min="900" max="4400" value="900" data-id="range_power"/>
                                                <input id="max_power" class="max_power"  
                                                type="range" min="900" max="4400" value="4400" data-id="range_power"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('  عدد الأحصنة')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input  id="range_min_hp" name="range_hp_min"  class="range_min_hp light left" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <span class="">الى</span>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input id="range_max_hp" name="range_hp_max" class="range_max_hp light right" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <div class="rangeslider">
                                                <input id="min_hp" class="min_hp" type="range" min="80" 
                                                    max="600" value="80" data-id="range_hp"/>
                                                <input id="max_hp" class="max_hp"  type="range" min="80" 
                                                    max="600" value="600" data-id="range_hp"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('  المسافة المقطوعة')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input  id="range_min_distance" name="range_distance_min"class="range_min_distance light left" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <span class="">الى</span>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input id="range_max_distance" name="range_distance_max" class="range_max_distance light right" type="text" 
                                            placeholder="الكل" >
                                        </div> 
                                    </div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <div class="rangeslider">
                                                <input id="min_distance" class="min_distance"  type="range" min="0" 
                                                    max="500000" value="0" data-id="distanceRange"/>
                                                <input id="max_distance" class="max_distance"  type="range" min="0" 
                                                    max="500000" value="500000" data-id="distanceRange"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('ناقل الحركة')); ?>" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear1" type="radio" name="gear" value="aut" data-parsley-required-message="ناقل الحركة  مطلوب"
                                     required>
                                            <label for="gear1" class="choice-button-text">أوتماتيك</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear2" type="radio" name="gear" value="man" >
                                            <label for="gear2" class="choice-button-text">يدوي</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear3" type="radio" name="gear" value="sau" >
                                            <label for="gear3" class="choice-button-text">نصف أوتماتيك</label>
                                        </div> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__('الوقود')); ?>" type="text" disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="fuel1" type="radio" name="fuel" value="bin" data-parsley-required-message="الوقود  مطلوب"
                                            required>
                                            <label for="fuel1" class="choice-button-text">بنزين</label>
                                            
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel2" type="radio" name="fuel" value="des" >
                                            <label for="fuel2" class="choice-button-text">ديزل</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel3" type="radio" name="fuel" value="binelec" >
                                            <label for="fuel3" class="choice-button-text">بنزين / كهرباء</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel4" type="radio" name="fuel" value="deselec">
                                            <label for="fuel4" class="choice-button-text">ديزل / كهرباء</label>
                                       
                                        </div> 
                                    </div>
                                    
                                    
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel5" type="radio" name="fuel" value="gaz" >
                                            <label for="fuel5" class="choice-button-text">غاز</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel6" type="radio" name="fuel" value="elec" >
                                            <label for="fuel6" class="choice-button-text">كهرباء</label>
                                        
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__(' نظام الدفع')); ?>" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrains1" type="radio" name="drivetrain_system" value="fbf" data-parsley-required-message="نظام الدفع مطلوب"
                                     required >
                                            <label for="drivetrains1" class="choice-button-text">4X4</label>
                                         
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrains2" type="radio" name="drivetrain_system" value="tbf" >
                                            <label for="drivetrains2" class="choice-button-text">2X4</label>
                                          
                                        </div> 
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__(' نوع الدفع')); ?>" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="drivetrain_type1" type="radio" name="drivetrain_type" data-parsley-required-message="نوع الدفع مطلوب"
                                        value="fro" required>
                                            <label for="drivetrain_type1" class="choice-button-text">أمامي</label>
                                         
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrain_type2" type="radio" name="drivetrain_type" value="bak" >
                                            <label for="drivetrain_type2" class="choice-button-text">خلفي</label>
                                        
                                        </div> 
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="input-group ">
                                        <input class="form-control" type="text" 
                                        placeholder="<?php echo e(__(' طريفة الدفعات')); ?>"   disabled>
                                        <input class="input-group-addon" type="checkbox" 
                                        id="price_type_check" >
                                       
                                    </div>
                                </div> 

                                

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="price_type_checkbox  radiochoosen"  id="priceType1" type="checkbox" 
                                            name="payment_method[]" value="payment_method_types_pay1" 
                                        >
                                            <label for="priceType1" class="choice-button-text">نقدا</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="price_type_checkbox radiochoosen"  id="priceType2" type="checkbox" 
                                            name="payment_method[]" value="payment_method_types_pay2" >
                                            <label for="priceType2" class="choice-button-text">عن طريق البنك</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="price_type_checkbox radiochoosen"  id="priceType3" type="checkbox" 
                                            name="payment_method[]" value="payment_method_types_pay3" >
                                            <label for="priceType3" class="choice-button-text">تقسيط مباشر</label>
                                        </div> 
                                    </div>
                                </div>

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="price_type_checkbox radiochoosen"  id="priceType4" type="checkbox" 
                                            name="payment_method[]" value="payment_method_types_pay4" >
                                            <label for="priceType4" class="choice-button-text">دفعة أولى + شيكات شخصية</label>
                                        
                                        </div> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__(' عدد المقاعد')); ?>" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat1" type="radio" name="num_of_seats" value="s1" data-parsley-required-message="عدد المقاعد مطلوب"
                                        required>
                                            <label for="seat1" class="choice-button-text">2+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat2" type="radio" name="num_of_seats" value="s2" >
                                            <label for="seat2" class="choice-button-text">3+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat3" type="radio" name="num_of_seats" value="s3" >
                                            <label for="seat3" class="choice-button-text">4+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat4" type="radio" name="num_of_seats" value="s4" >
                                            <label for="seat4" class="choice-button-text">5+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat5" type="radio" name="num_of_seats" value="s5" >
                                            <label for="seat5" class="choice-button-text">6+1</label>
                                        </div> 
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="<?php echo e(__(' حالة المركبة')); ?>" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="radio1" type="radio" name="vehicle_status" value="new" data-parsley-required-message=" حالة المركبة مطلوب"
                                        required>
                                            <label for="radio1"class="choice-button-text">جديد</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="radio2" type="radio" name="vehicle_status" value="usd" >
                                            <label for="radio2" class="choice-button-text">مستعمل</label>
                                        </div> 
                                    </div>

                                </div>


                                <div class="d-flex flex-row">
                                    <div class="col-lg-6 col-sm-4 ml-auto mr-auto">

                                        <div class="form-group">
                                            <label>لون السيارة </label>
                                            <input class="form-control" name="body_color" id="body_color-search" style="display:none; border: 0 !important;
                                             box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                            type="text" 
                                             >
                                        </div> 
                                        
                                        <div class="form-group">
                                            <a class="btn btn-light btn-block" data-toggle="modal" 
                                                data-target="#car-color-modal-search" id="color-car-search"
                                                style="border-radius: 30px;">
                                                <i id="cars-color-search" class="fa fa-check-circle-o">
                                                    <span id="color-car-search-text" > لم يحدد</span>
                                                </i> 
                                            </a>
                                        </div> 
                                    </div>
                                    <div class="col-lg-6 col-sm-4 ml-auto mr-auto">
                                        <div class="form-group">
                                            <label>لون الفرش </label>
                                            <input class="form-control" name="interior_color" id="interior_color-search" style="display:none; border: 0 !important;
                                             box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                            type="text" >

                                        </div> 
                                        <div class="form-group">
                                            <a class=" btn btn-light btn-block" data-toggle="modal" 
                                            data-target="#interior-color-modal-search" id="color-interior-search"
                                            style="border-radius: 30px;" >
                                                <i id="interior-color-search" class="fa fa-check-circle-o">
                                                    <span id="color-interior-search-text" class="color-interior-search-text"> لم يحدد</span>
                                                </i> 
                                            </a>
                                        </div> 
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-4 col-md-6 ml-auto mr-auto">

                                    
                            </div>
                        </div>

                        

                        <div class="row">
                            <div class="col-lg-12 ml-auto mr-auto">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <a class="toggle btn btn-secondary btn-block" style="cursor:pointer">
                                            <span class="toggle-text"> خيارات أخرى</span><i id="toggleicon" class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div></br>

                        <?php echo $__env->make('pages.vehicle.more', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

                    <div class="row">
                        <div class="col-lg-6 ml-auto mr-auto">
                            <center>
                                <button type="submit" id="" class="btn btn-danger btn-block" >
                                        <span>بحث </span>
                                    </button>
                            
                            </center>
                        </div>
                    </div></br>
                </form>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
        
        </div>
   </div>


    <?php echo $__env->make('pages.search.modals', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        
    <script>
    
    $(function() {

        

        $('.multiselect-ui').multiselect({
                includeSelectAllOption: true
            });
        });

        function rangeInputChangeEventHandlerYear(e){

            // year
            var minBtn = $(this).parent().children('.min'),
                maxBtn = $(this).parent().children('.max'),
                minYear = $("#range_min").parent().children('.range_min'),
                maxYear = $("#range_max").parent().children('.range_max'),
                minVal = parseInt($(minBtn).val()),
                maxVal = parseInt($(maxBtn).val()),
                origin = $(this).context.className;
            
            if(origin === 'min' && minVal > maxVal-5){
                $(minBtn).val(maxVal-5);
            }
            var minVal = parseInt($(minBtn).val());
            $(minYear).val((minVal));

            if(origin === 'max' && maxVal-5 < minVal){
                $(maxBtn).val(5+ minVal);
            }
            var maxVal = parseInt($(maxBtn).val());
            $(maxYear).val((maxVal));
        }

        function rangeInputChangeEventHandlerPrice(e){

            // price
            var minBtnPrice = $(this).parent().children('.min_price'),
                maxBtnPrice = $(this).parent().children('.max_price'),
                minPrice = $("#range_min_price").parent().children('.range_min_price'),
                maxPrice = $("#range_max_price").parent().children('.range_max_price'),
                minValPrice = parseInt($(minBtnPrice).val()),
                maxValPrice = parseInt($(maxBtnPrice).val()),
                originPrice = $(this).context.Name;

            if(originPrice === 'min_price' && minValPrice > maxValPrice-10){
                $(minBtnPrice).val(maxValPrice-10);
            }
            var minValPrice = parseInt($(minBtnPrice).val());
            $(minPrice).val((minValPrice));

            if(originPrice === 'max_price' && maxValPrice-10 < minValPrice){
                $(maxBtnPrice).val(10+ minValPrice);
            }
            var maxValPrice = parseInt($(maxBtnPrice).val());
            $(maxPrice).val((maxValPrice));
        }

        function rangeInputChangeEventHandlerPower(e){

            // power
            var minBtnPower = $(this).parent().children('.min_power'),
                maxBtnPower = $(this).parent().children('.max_power'),
                minPower = $("#range_min_power").parent().children('.range_min_power'),
                maxPower = $("#range_max_power").parent().children('.range_max_power'),
                minValPower = parseInt($(minBtnPower).val()),
                maxValPower = parseInt($(maxBtnPower).val()),
                originPower = $(this).context.Name;

            if(originPower === 'min_power' && minValPower > maxValPower-5){
                $(minBtnPower).val(maxValPower-5);
            }
            var minValPower = parseInt($(minBtnPower).val());
            $(minPower).val((minValPower));

            if(originPower === 'max_power' && maxValPower-5 < minValPower){
                $(maxBtnPower).val(5+ minValPower);
            }
            var maxValPower = parseInt($(maxBtnPower).val());
            $(maxPower).val((maxValPower));
        }

        function rangeInputChangeEventHandlerHP(e){

            // hp
            var minBtnHP = $(this).parent().children('.min_hp'),
                maxBtnHP = $(this).parent().children('.max_hp'),
                minHP = $("#range_min_hp").parent().children('.range_min_hp'),
                maxHP = $("#range_max_hp").parent().children('.range_max_hp'),
                minValHP = parseInt($(minBtnHP).val()),
                maxValHP = parseInt($(maxBtnHP).val()),
                originHP = $(this).context.Name;

            if(originHP === 'min_hp' && minValHP > maxValHP-1){
                $(minBtnHP).val(maxValHP-1);
            }
            var minValHP = parseInt($(minBtnHP).val());
            $(minHP).val((minValHP));

            if(originHP === 'max_hp' && maxValHP-1 < minValHP){
                $(maxBtnHP).val(1+ minValHP);
            }
            var maxValHP = parseInt($(maxBtnHP).val());
            $(maxHP).val((maxValHP));
        }

        function rangeInputChangeEventHandlerDistance(e){

            // distance
            var minBtnDistance = $(this).parent().children('.min_distance'),
                maxBtnDistance = $(this).parent().children('.max_distance'),
                minDistance = $("#range_min_distance").parent().children('.range_min_distance'),
                maxDistance = $("#range_max_distance").parent().children('.range_max_distance'),
                minValDistance = parseInt($(minBtnDistance).val()),
                maxValDistance = parseInt($(maxBtnDistance).val()),
                originDistance = $(this).context.Name;

            if(originDistance === 'min_distance' && minValDistance > maxValDistance-1){
                $(minBtnDistance).val(maxValDistance-1);
            }
            var minValDistance = parseInt($(minBtnDistance).val());
            $(minDistance).val((minValDistance));

            if(originDistance === 'max_distance' && maxValDistance-1 < minValDistance){
                $(maxBtnDistance).val(1+ minValDistance);
            }
            var maxValDistance = parseInt($(maxBtnDistance).val());
            $(maxDistance).val((maxValDistance));
        }

        $('input[data-id="range_1"]').on( 'input', rangeInputChangeEventHandlerYear);
        $('input[data-id="range_price"]').on( 'input', rangeInputChangeEventHandlerPrice);
        $('input[data-id="range_power"]').on( 'input', rangeInputChangeEventHandlerPower);
        $('input[data-id="range_hp"]').on( 'input', rangeInputChangeEventHandlerHP);
        $('input[data-id="distanceRange"]').on( 'input', rangeInputChangeEventHandlerDistance);
    

        var carcolorSearch="", carColorCodeSearch = "", carColorCodeValSearch = "";
        $('.ul-car-color-search').on('click', 'li', function(){
            var current = this;     
            carColorCodeSearch = $(this).attr("value");
            carColorCodeValSearch = carColorCodeSearch.substr(1);
            carColorCodeValSearch = 'c' + carColorCodeValSearch;
            carcolorSearch = $(this).attr('data-id');
            if(carColorCodeSearch.length > 0) {
                $(this).find('.checkafterSearch').toggleClass('fa fa-check');
            }

            $("#chooseCarColorSearch").click(function () {  
                $("#color-car-search").css('background-color',carColorCodeSearch);
                $("#color-car-search").css('color','white');
                $("#cars-color-search span").text(carcolorSearch);    
                $("#body_color-search").val(carColorCodeValSearch);  
                $(".checkafterSearch").removeClass('fa fa-check');
                //$(this).closest('.modal').modal('hide');
                $("#car-color-modal-search").modal('hide');
            });
            
        });

        var color="", interiorColor="" ,interiorColorCode="";
        $('.ul-interior-color-search').on('click', 'li', function(e){  
            var current = this;     
            interiorColor = $(this).attr("value");
            interiorColorCode = interiorColor.substr(1);
            interiorColorCode = 'c' + interiorColorCode;
            color = $(this).attr('data-id');
            if(color.length > 0) {
                $(this).find('.checkafterSearch').toggleClass('fa fa-check');
            }
        });

        $(document).on('click', '.chooseinteriorColorSearch', function(){ 
            $("#interior-color-modal-search").modal('hide');
            $("#color-interior-search").css('background-color',interiorColor);
            $("#color-interior-search").css('color','white');
            $("#interior-color-search span").text(color);    
            $("#interior_color-search").val(interiorColorCode); 
            $(".checkafterSearch").removeClass('fa fa-check');
        });


        var $make_id = "", $companyName="", companyLogo="", logoWidth= "", logoHeight= "";
        var model_id = [],  modelName = [], modelId= [], models = [];
        // Type & Modal values for the car
        $(document).on('click', '.searchModelsCars', function() { 
            id = $(this).attr('data-id');
            $(".company-model-body-search").html("");
            var html="";
            html += '<div class="row"><div class="col-lg-12"><input class="selectAllModels" type="checkbox"';
            html += 'data-id="'+ id +'"></input>';
            html += ' <span class="selectAllModelsIcon" style="font-size:15px;">';
            html += '  اختيار كافة الموديلات</span></div></div>';

            $make_id = id;
            $.ajax({
                url: '/make/'+ id,
                method:"GET",
                dataType:"json",
                
                success: function(data){
                    $companyName = data[0].name;
                    companyLogo = data[0].logo;
                    logoWidth = data[0].web_width;
                    logoHeight = data[0].web_height;
                }
            });

            $.ajax({
                url: '/vehicle/'+ id,
                method:"GET",
                dataType:"json",
                
                success: function(data) {
                    var datalength = data.length;
                    
                    html += '<table class="table table-bordered" >';
                    $(".company-model-title-search").html($companyName);
                    for(var count=0; count < datalength; count++) {
                        
                        var length = data[count].length;
                        if(length == 1) {
                            var c = count;
                            html += '<tr>';
                            html += '<td><input class="radioModelName" data-id="'+ data[c][0].name +'" value="'+ data[c][0].id +'" type="checkbox" name="optradio">  ' + '   ' + data[c][0].name +'</td>';  
                            html += '</tr>';
                        } else {
                            var cnt = count;
                            id = data[cnt][0].id;
                            html += '<tr rel="'+ data[cnt][0].id +'" class="rr"><td><a data-id="'+ data[cnt][0].id +'" class="testttt"';
                            html += 'style="cursor:pointer">'+ data[cnt][0].name  +'</a></td></tr>';
                            
                            for(var i=1; i < length; i++) {
                                
                                html += '<tr class="moreModels11Search row'+ id +'Collapse"style="display:none" > ';
                                html += '<td><input class="radioModelName" data-id="'+ data[cnt][i].name +'" value="'+ data[cnt][i].id +'" type="checkbox" name="optradio">  ' + '   ' + data[cnt][i].name +'</td>';  
                                html += '</tr>';
                            }
                        }
                    }
                   
                    html += '</table>'; 
                    $('.company-model-body-search').append(html); 
                    $('#company-model-modal-search').modal('show'); 
                    $("#makeSearch").val($make_id);
                    $make = $("#makeSearch").val();
                    
                    $('.radioModelName').on('change', function() {
                        model_id.push($(this).val());
                        modelName.push($(this).attr('data-id'));
                    });
                    $('.rr').on('click', function() {
                    //$(document).on('click', '.rr' , function() { 
                        var rel = $(this).attr('rel');
                        $('.row' + rel + 'Collapse').slideToggle();
                    });

                    $(".add-multiple-images-company").change(function() { 
                        readURL(this);
                    });
                }
            });
        });
    
        


        $(document).on('change', '.selectAllModels', function() { 
            if($(".selectAllModels").is(":checked")) {
                $(".selectAllModelsIcon").css("color", "green");
                $(".selectAllModelsIcon").css("font-weight", "bold");
                $(".radioModelName").attr('checked', true);
                $("#all_model").val($(this).attr('data-id'));
                $(this).data('clicked', true);
            } else {
                $(".selectAllModelsIcon").css("color", "black");
                $(".selectAllModelsIcon").css("font-weight", "normal");
                $(".radioModelName").removeAttr('Checked'); 
                $("#all_model").val('');
                $(this).data('clicked', false);
            }
        });


        function checkValue(value,arr){
            var status = 'Not exist';
            
            for(var i=0; i<arr.length-1; i++){
                var name = arr[i];
                if(name == value){
                status = 'Exist';
                break;
                }
            }

            return status;
        }

        var counter = 1,c;
        $('.company-model-modal-submit-search').on('click', function() {
            c=0;
            if (counter <= 3) {
                var logoPath = "/images/logos/" + companyLogo;

                if($(".selectAllModels").data('clicked')) {
                    console.log($("#all_model").val());
                    $('#searchBodyTypeLogoChoosen').append('<div style="padding:0.3rem"class="control-group text-center d-flex flex-row">'
                        + '<div class="col-md-3 col-sm-3"><img  src="'+ logoPath +' " '
                        + ' width="'+ logoWidth +'"; height="'+ logoHeight+'";></div>'
                        + '<div class="col-md-3 col-sm-3"><label>'+ $companyName +'</label></div>'
                        + '<div class="col-md-3 col-sm-3"><label>تم اختيار جميع الموديلات</label></div>'
                        + '<div class="col-md-2 col-sm-2"><button class="btn remove-image-logo"'
                        + 'type="button"><i class="fa fa-window-close" style="color:black; font-size:1.5em;">'
                        + '</i> </button></div></div>'
                    );
                    counter++;
                }

                if(model_id.length <=3) {
                        
                    for( i = 0; i<model_id.length; i++) {
                        modelId.push(model_id[i]);
                        models.push(modelName[i]);
                        console.log('last model : ' +  models[models.length-1]);
                        console.log('models: ' + models);
                        console.log('models length: ' + models.length);
                        console.log('models ids: ' + modelId);
                        if(models.length >1) {
                            status = checkValue(models[models.length-1], models);
                            console.log('status : ' + status);
                        }
                        if(status == "Exist") {
                            swal("",  "تم اختيار هذا الموديل مسبقا", "error");
                        } else {
                            $('[name="model_id"]').val(modelId[i]);
                            $('#searchBodyTypeLogoChoosen').append('<div style="padding:0.3rem"class="control-group text-center d-flex flex-row">'
                                + '<div class="col-md-3 col-sm-3"><img  src="'+ logoPath +' " '
                                + ' width="'+ logoWidth +'"; height="'+ logoHeight+'";></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ $companyName +'</label></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ modelName[i] +'</label></div>'
                                + '<div class="col-md-2 col-sm-2"><button class="btn remove-image-logo"'
                                + 'type="button"><i class="fa fa-window-close" style="color:black; font-size:1.5em;">'
                                + '</i> </button></div></div>'
                            );
                            counter++;
                        }
                    }
                } else {
                    swal("",  "مسموح كحد أقصى البحث عن 3 موديلات", "warning");
                }
            } else {
                swal("",  "مسموح كحد أقصى البحث عن 3 موديلات", "warning");
            }
            $('#company-model-modal-search').modal('hide'); 
            $('[name=model_id]').val(JSON.stringify( modelId )); 
        });

        
        $('#searchBodyTypeLogoChoosen').on('click', '.remove-image-logo', function(){
            $(this).parents(".control-group").remove();
            counter--;
            modelId.pop(model_id[length-1]);
            models.pop(modelName[length-1]);
            $('.removingSearching').empty();
        });

        $("#resetSearch").click(function(){
            $('#searchForm').trigger("reset");
            $("#searchBodyTypeLogoChoosen").empty();
            counter = 1;
            $('#multiSelectpicker').selectpicker('refresh');
            
            $('#color-car-search-text').text("لم يحدد");
            $("#color-car-search").css('background-color','#f8f9fa');
            $("#color-car-search").css('color','black');

            $('#color-interior-search-text').text("لم يحدد");
            $("#color-interior-search").css('background-color','#f8f9fa');
            $("#color-interior-search").css('color','black');

        });
        
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/search/index.blade.php ENDPATH**/ ?>