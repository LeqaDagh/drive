

<?php $__env->startSection('content'); ?>

<div class="row" >
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="row">
        <?php if($vehicles): ?>
            <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="<?php echo e(route('vec-seller.information', $vehicle->id)); ?>" class="text-muted">
                    <div class="card p-1 mb-1 bg-white rounded">
                        <div class="card-body" style="padding: 0.1rem">
                            <div class="col-lg-12 col-md-12">
                                <div class="d-flex d-row">
                                    <div class="col-lg-12 col-md-12">
                                        <center>
                                            <?php if(!empty(\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first()) > 0): ?>
                                                <label style="color:#595959; font-weight: bold;">
                                                <?php echo e((\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first())->name); ?>, 
                                                </label>
                                            <?php endif; ?>
                                            <?php if(!empty(\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first()) > 0): ?>
                                                <label style="color:#595959; font-weight: bold;">
                                                <?php echo e((\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first())->name); ?>

                                                </label>
                                                <?php if($vehicle->stared == 'yes'): ?>
                                            <i class=" fa fa-star" style="color:#ffcc00"></i>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="d-flex d-row">

                                    <div id="carousel-vehicles-result<?php echo e($vehicle->id); ?>" class="text-center carousel slide" data-ride="carousel">
                                        <center>
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li data-target="#carousel-vehicles-result<?php echo e($vehicle->id); ?>" data-slide-to="<?php echo e($loop->index); ?>" 
                                                        class="<?php echo e($loop->first ? 'active' : ''); ?>"></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div style="width:100%; height: 150px; background-color:white"  class="carousel-item item <?php echo e($loop->first ? ' active' : ''); ?>" >
                                                        <a href="<?php echo e(route('vec-seller.all', $vehicle->id)); ?>">
                                                            <img src="<?php echo e('/storage/vehicles/'.$data->file_name); ?>" width="100%" 
                                                            height="150px" style="object-fit: contain ">
                                                        </a>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>

                                            <!-- Controls -->
                                            <a class="right carousel-control" href="#carousel-vehicles-result<?php echo e($vehicle->id); ?>" role="button" data-slide="prev">
                                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="left carousel-control" href="#carousel-vehicles-result<?php echo e($vehicle->id); ?>" role="button" data-slide="next">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </center> 
                                    </div>
                                   <!-- <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 ml-auto mr-auto">
                                    <?php if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0): ?>
                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                            src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name); ?>">
                                    <?php endif; ?>
                                    </div> -->


                                    <div class="col-lg-5 col-md-5">
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                السعر  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->price); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                القوة  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->hp); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                    ناقل الحركة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->gear == 'aut'): ?>
                                                أوتماتيك  
                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                يدوي    
                                                <?php else: ?> نصف أوتماتيك
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                    الوقود :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                بنزين  
                                                <?php else: ?> <?php if($vehicle->gear == 'des'): ?>
                                                ديزل  
                                                <?php else: ?> <?php if($vehicle->gear == 'binelec'): ?>
                                                بنزين / كهرباء  
                                                <?php else: ?> <?php if($vehicle->gear == 'deselec'): ?>
                                                ديزل / كهرباء 
                                                <?php else: ?> <?php if($vehicle->gear == 'gaz'): ?>
                                                غاز    
                                                <?php else: ?> كهرباء
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                سنة الانتاج  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->year_of_product); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                حالة المركبة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->vehicle_status == 'new'): ?>
                                                    جديد  
                                                <?php else: ?>
                                                    مستعمل
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?> 
        <div class="alert alert-warning">
            <strong></strong>لا يوجد نتائج!
        </div>
        <?php endif; ?>
        </div>
    </div>
   
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/search/result.blade.php ENDPATH**/ ?>