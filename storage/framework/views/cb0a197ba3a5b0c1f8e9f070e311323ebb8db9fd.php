

<?php $__env->startSection('content'); ?>

<div class="row" > 
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="row">
            <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card p-1 mb-1 bg-white rounded">
                        <div class="card-body ">
                            <div id="carousel-example-generic<?php echo e($vehicle->id); ?>" class="text-center carousel slide" 
                            data-ride="carousel">
                                <center>
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li data-target="#carousel-example-generic<?php echo e($vehicle->id); ?>" 
                                            data-slide-to="<?php echo e($loop->index); ?>" 
                                                class="<?php echo e($loop->first ? 'active' : ''); ?>"></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                    <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div style="width:100%; height: 160px; background-color:white" 
                                         class="carousel-item item <?php echo e($loop->first ? ' active' : ''); ?>" >
                                            <a href="<?php echo e(route('vec-seller.all', $vehicle->id)); ?>">
                                                <img src="<?php echo e('/storage/vehicles/'.$data->file_name); ?>" width="100%" 
                                                height="160px" style="object-fit: contain" >
                                            </a>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>

                                    <!-- Controls -->
                                    <a class="right carousel-control" href="#carousel-example-generic<?php echo e($vehicle->id); ?>" role="button" data-slide="prev">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="left carousel-control" href="#carousel-example-generic<?php echo e($vehicle->id); ?>" role="button" data-slide="next">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </center>
                            </div>

                            <a href="<?php echo e(route('stared.show', $vehicle->id)); ?>" class="text-muted">
                                <div class="row">
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto" style="padding: 0.4rem"  >
                                        <h6 style="color:#595959; font-weight: bold; font-size:13px">
                                        <?php echo e($vehicle->name); ?>  <?php echo e($vehicle->model_name); ?>

                                        </h6>
                                    </div>   
                                </div> 
                                <div class="d-flex flex-row">
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                        <i class=" fa fa-star" style="color:#ffcc00"></i>
                                        <i class=" fa fa-star" style="color:#ffcc00"></i>
                                        <i class=" fa fa-star" style="color:#ffcc00"></i>
                                        <i class=" fa fa-star" style="color:#ffcc00"></i>
                                        <i class=" fa fa-star" style="color:#ffcc00"></i>
                                    </div>   
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                    <?php if($vehicle->vehicle_status == 'new'): ?>
                                        جديد  
                                    <?php else: ?>
                                        مستعمل
                                    <?php endif; ?>
                                    </div>  
                                </div>
                                <div class="text-center d-flex flex-row">
                                    <div class=" col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
                                        <i class="fa fa-circle 0" style="color: #<?php echo e(substr($vehicle->body_color, 1)); ?>">
                                        <span style="color:black">اللون الخارجي</span>
                                        </i>

                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
                                        <i class="fa fa-circle 0" style="color: #<?php echo e(substr($vehicle->interior_color, 1)); ?>">
                                            <span style="color:black">اللون الداخلي</span>
                                        </i>
                                    </div> 
                                </div> 
                                <hr>

                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="">
                                            <img src="<?php echo e(url('/images/icons/price.png')); ?>" width=20>
                                        </div>
                                        <div class="">
                                            <label> السعر</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px"><?php echo e($vehicle->first_payment); ?> </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ">
                                        <div class="">
                                            <img src="<?php echo e(url('/images/icons/power.png')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label>القوة</label>
                                        </div>
                                        <div class="">
                                            <label  style="font-size:13px"><?php echo e($vehicle->power); ?> </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="">
                                            <?php if($vehicle->gear == 'aut'): ?>
                                                <img src="<?php echo e(url('/images/icons/automaticgear.png')); ?>" width=20>
                                            <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                <img src="<?php echo e(url('/images/icons/normalgear.png')); ?>" width=20>   
                                            <?php else: ?>  <img src="<?php echo e(url('/images/icons/semigear.png')); ?>" width=20>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        
                                        </div>
                                        <div class="">
                                            <label > ناقل الحركة</label>
                                        </div>
                                        <div class="">
                                            <label  style="font-size:13px">
                                                <?php if($vehicle->gear == 'aut'): ?>
                                                أوتماتيك  
                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                يدوي    
                                                <?php else: ?> نصف أوتماتيك
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    </div> 
                                </div> 
                                <hr>

                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="">
                                            <img src="<?php echo e(url('/images/icons/fuel.jpeg')); ?>" width=20>
                                        </div>
                                        <div class="">
                                            <label> الوقود</label>
                                        </div>
                                        <div class="">
                                            <label  style="font-size:13px">
                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                بنزين  
                                                <?php else: ?> <?php if($vehicle->fuel == 'des'): ?>
                                                ديزل  
                                                <?php else: ?> <?php if($vehicle->fuel == 'binelec'): ?>
                                                بنزين / كهرباء  
                                                <?php else: ?> <?php if($vehicle->fuel == 'deselec'): ?>
                                                ديزل / كهرباء 
                                                <?php else: ?> <?php if($vehicle->fuel == 'gaz'): ?>
                                                غاز    
                                                <?php else: ?> كهرباء
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="">
                                            <img src="<?php echo e(url('/images/icons/8757.png')); ?>" width=20>
                                        </div>
                                        <div class="">
                                            <label >سنة الانتاج</label>
                                        </div>
                                        <div class="">
                                            <label  style="font-size:13px"><?php echo e($vehicle->year_of_product); ?> </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="">
                                            <img src="<?php echo e(url('/images/icons/seat.jpeg')); ?>" width=20>
                                        </div>
                                        <div class="">
                                            <label> عدد المقاعد</label>
                                        </div>
                                        <div class="">
                                            <label  style="font-size:13px">
                                                <?php if($vehicle->num_of_seats == 's1'): ?>
                                                2+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's2'): ?>
                                                3+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's3'): ?>
                                                4+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's4'): ?>
                                                5+1    
                                                <?php else: ?> 6+1
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            <label>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/stared/index.blade.php ENDPATH**/ ?>