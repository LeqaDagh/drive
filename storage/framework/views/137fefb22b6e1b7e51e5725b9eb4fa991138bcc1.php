

<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="<?php echo e(url('/css/select2.css')); ?>">
<div class="row">
    <div class="col-md-12">
        <center>
            <div class="col-md-4">
                <div id="errorAdding" style="font-size:13px" class="alert alert-danger">يرجى البحث عن مركبتين على الأقل</div>
            </div>
        </center>
    </div>
</div>

<div class="row" > 
  <div class="col-lg-1 col-md-1 col-sm-1">
    
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10">
    <div class="row">
        <div class="main col-lg-6 col-md-6 col-sm-6">
          <form class="form" method="POST" id="compareFirstForm" enctype="multipart/form-data" novalidate>
          <?php echo csrf_field(); ?>
            <div class="card ">
                <div class="card-body ">
                    <div class="row" >
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="d-flex flex-row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select  name="make_id1" id="make_id1" class="make_id1 form-control" > 
                                        <option style="font-size:13px"></option>
                                            <?php $__currentLoopData = $makes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <?php if($data->name == "Hummer"): ?>
                                              <option  value="<?php echo e($data->id); ?>" 
                                                data-img_src="<?php echo e(url('/images/logos/'.$data->logo)); ?>"
                                                data-img_width="50px"
                                                data-img_height="<?php echo e($data->web_height); ?>">
                                                <?php echo e($data->name); ?>

                                                </option>
                                              <?php else: ?> 
                                                <option  value="<?php echo e($data->id); ?>" 
                                                data-img_src="<?php echo e(url('/images/logos/'.$data->logo)); ?>"
                                                data-img_width="<?php echo e($data->web_width); ?>"
                                                data-img_height="<?php echo e($data->web_height); ?>">
                                                <?php echo e($data->name); ?>

                                                </option>
                                              <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select style="font-size:12px" name="model_id1" id="model_id1" 
                                        class="text-center form-control"> 
                                            <option class="optionText" selected disabled>الموديل</option>
                                        </select> 
                                    </div>
                                </div>
                                
                            </div>
                            <div class="d-flex flex-row">
                              <div style="font-size:13px" class="col-lg-12">
                                  الهيكل
                              </div>
                            </div>
                             
                           
                             <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2">

                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10">
                                  <div class=" row">
                                
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="hatchback card-body" style="">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body1[]" value="hat" >
                                                        <img src="<?php echo e(url('/images/body-type/hatchback.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    hatchback
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="coupe card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body1[]" value="cou" >
                                                        <img src="<?php echo e(url('/images/body-type/coupe.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    coupe
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="sedan card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body1[]" value="sed" >
                                                        <img src="<?php echo e(url('/images/body-type/sedan.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    sedan
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="station card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body1[]" value="sta" >
                                                        <img src="<?php echo e(url('/images/body-type/station.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    station
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="sport-car card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body1[]" value="spo" >
                                                        <img src="<?php echo e(url('/images/body-type/sport-car.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    sport-car
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="cabriolet card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body1[]" value="cab" >
                                                        <img src="<?php echo e(url('/images/body-type/cabriolet.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    cabriolet
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="pickup card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body1[]" value="pic" >
                                                        <img src="<?php echo e(url('/images/body-type/pickup.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    pickup
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="off-road card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body1[]" value="off" >
                                                        <img src="<?php echo e(url('/images/body-type/off-road.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    off-road
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card-stats">
                                            <center>
                                                <div class="station-wagon card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body1[]" value="stw" >
                                                        <img src="<?php echo e(url('/images/body-type/station-wagon.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    station-wagon
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1">

                                </div>
                             </div>
                              
                           

                            <div class="d-flex flex-row" style="padding-top:0.8rem" >
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="font-size:13px;padding:0.8rem" >
                                    سنة الانتاج 
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control form-control-sm" type="number" id="range_year_from1" 
                                  style="font-size:13px;"name="range_year_from1" placeholder="من ">
                                </div>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                
                                <i style="font-size:22px;padding:0.8rem" class="fa fa-arrow-circle-o-left"></i>
                                
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control" type="number" id="range_year_to1"
                                  style="font-size:13px;" name="range_year_to1" placeholder="  الى ">
                                </div>
                              </div>
                            </div>

                            <div class="d-flex flex-row">
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="font-size:13px;padding:0.8rem" >
                                السعر 
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control form-control-sm" type="number" id="range_price_from1" 
                                  style="font-size:13px;"name="range_price_from1" placeholder="من ">
                                </div>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                
                                <i style="font-size:22px;padding:0.8rem" class="fa fa-arrow-circle-o-left"></i>
                                
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control" type="number" id="range_price_to1"
                                  style="font-size:13px;" name="range_price_to1" placeholder="  الى ">
                                </div>
                              </div>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem" >
                                    ناقل الحركة 
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear1" type="checkbox" name="gears[]" value="aut">
                                        <label for="gear1" class="choice-button-text">
                                        <img src="<?php echo e(url('/images/icons/automaticgear.png')); ?>" style="filter: brightness(10%) sepia(20%) saturate(10);" width=25>
                                        أوتماتيك</label>
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear2" type="checkbox" name="gears[]" value="man" >
                                        <label for="gear2" class="choice-button-text"> 
                                        <img src="<?php echo e(url('/images/icons/normalgear.png')); ?>" style="filter: brightness(10%) sepia(20%) saturate(10);"width=25> يدوي
                                        </label>
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear3" type="checkbox" name="gears[]" value="sau" >
                                        <label for="gear3" class="choice-button-text">
                                        <img src="<?php echo e(url('/images/icons/semigear.png')); ?>"style="filter: brightness(10%) sepia(20%) saturate(10);" width=25>نصف أوتماتيك
                                        </label>
                                    </div> 
                                </div>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem" >
                                    الوقود 
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen"  id="fuel1" type="checkbox" name="fuel1[]" value="bin" data-parsley-required-message="الوقود  مطلوب"
                                        required>
                                        <label for="fuel1" class="choice-button-text">بنزين</label>
                                        
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel2" type="checkbox" name="fuel1[]" value="des" >
                                        <label for="fuel2" class="choice-button-text">ديزل</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel5" type="checkbox" name="fuel1[]" value="gaz" >
                                        <label for="fuel5" class="choice-button-text">غاز</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel6" type="checkbox" name="fuel1[]" value="elec" >
                                        <label for="fuel6" class="choice-button-text">كهرباء</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel3" type="checkbox" name="fuel1[]" value="binelec" >
                                        <label for="fuel3" class="choice-button-text">بنزين / كهرباء</label>
                                    
                                    </div> 
                                </div>
                                
                            </div>
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem;visibility:hidden" >
                                    الوقود
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel4" type="checkbox" name="fuel1[]" value="deselec">
                                        <label for="fuel4" class="choice-button-text">ديزل / كهرباء</label>
                                
                                    </div> 
                                </div>
                                
                            </div>
                           <!-- <div class="d-flex flex-row">
                                <div class=" col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker " multiple data-live-search="true"
                                            title="ناقل الحركة" name="gears[]">
                                            <option value="aut">أوتماتيك</option>
                                            <option value="man">يدوي</option>
                                            <option value="sau">نصف أوتماتيك</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker " 
                                            multiple data-live-search="true" title="الوقود" name="fuel1[]">
                                            <option value="bin">بنزين</option>
                                            <option value="des">ديزل</option>
                                            <option value="binelec">بنزين / كهرباء</option>
                                            <option value="deselec">ديزل / كهرباء</option>
                                            <option value="gaz">غاز</option>
                                            <option value="elec">كهرباء</option>
                                        </select>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div> 
            </div> </br>
            <div class="d-flex flex-row">
              <div class="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
                  <center>
                      <button type="submit"  class="btn btn-danger btn-block" >
                          <span>بحث </span>
                      </button>
                  </center>
              </div>
            </div>
          </form></br>

          <div id="firstCardVehicles" class="card" style="display:none"> 
              <div class="card-body ">
                  <div class="row" >
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <div  class="firstCardVehiclesAppending row">
                              
                          </div>
                      </div>
                  </div>
              </div> 
          </div> 
        </div> 

        <div class="main col-lg-6 col-md-6 col-sm-6">
          <form class="form" id="compareSecondForm" method="POST" enctype="multipart/form-data" novalidate>
          <?php echo csrf_field(); ?>
            <div class="card ">
                <div class="card-body ">
                    <div class="row" >
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="d-flex flex-row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                      <select name="make_id2" id="make_id2" class="make_id2 form-control"> 
                                            <option ></option>
                                            <?php $__currentLoopData = $makes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <?php if($data->name == "Hummer"): ?>
                                              <option  value="<?php echo e($data->id); ?>" 
                                                data-img_src="<?php echo e(url('/images/logos/'.$data->logo)); ?>"
                                                data-img_width="50px"
                                                data-img_height="<?php echo e($data->web_height); ?>">
                                                <?php echo e($data->name); ?>

                                                </option>
                                              <?php else: ?> 
                                                <option  value="<?php echo e($data->id); ?>" 
                                                data-img_src="<?php echo e(url('/images/logos/'.$data->logo)); ?>"
                                                data-img_width="<?php echo e($data->web_width); ?>"
                                                data-img_height="<?php echo e($data->web_height); ?>">
                                                <?php echo e($data->name); ?>

                                                </option>
                                              <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select style="font-size:12px" name="model_id2" id="model_id2" class="text-center form-control"> 
                                            <option selected disabled>الموديل</option>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row">
                              <div style="font-size:13px" class="col-lg-12">
                                  الهيكل
                              </div>
                            </div>
                             
                           
                             <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2">

                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10">
                                  <div class=" row">
                                
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="hatchback card-body" style="">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body2[]" value="hat" >
                                                        <img src="<?php echo e(url('/images/body-type/hatchback.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    hatchback
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="coupe card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body2[]" value="cou" >
                                                        <img src="<?php echo e(url('/images/body-type/coupe.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    coupe
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="sedan card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body2[]" value="sed" >
                                                        <img src="<?php echo e(url('/images/body-type/sedan.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    sedan
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1 ">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="station card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body2[]" value="sta" >
                                                        <img src="<?php echo e(url('/images/body-type/station.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    station
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="sport-car card-body ">
                                                    <label>
                                                        <input class="radiochoosen" type="checkbox" name="body2[]" value="spo" >
                                                        <img src="<?php echo e(url('/images/body-type/sport-car.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    sport-car
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="cabriolet card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body2[]" value="cab" >
                                                        <img src="<?php echo e(url('/images/body-type/cabriolet.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    cabriolet
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="pickup card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body2[]" value="pic" >
                                                        <img src="<?php echo e(url('/images/body-type/pickup.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px" class="stats">
                                                    pickup
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class=" card-stats">
                                            <center>
                                                <div class="off-road card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body2[]" value="off" >
                                                        <img src="<?php echo e(url('/images/body-type/off-road.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    off-road
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <div class="card-stats">
                                            <center>
                                                <div class="station-wagon card-body ">
                                                    <label>
                                                        <input type="checkbox" class="radiochoosen"  name="body2[]" value="stw" >
                                                        <img src="<?php echo e(url('/images/body-type/station-wagon.png')); ?>" width=70>
                                                    </label>
                                                </div>
                                                <div style="font-size:10px"class="stats">
                                                    station-wagon
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1">

                                </div>
                             </div>


                             <div class="d-flex flex-row" style="padding-top:0.8rem" >
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="font-size:13px;padding:0.8rem" >
                                    سنة الانتاج 
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control form-control-sm" type="number"  
                                  style="font-size:13px;"name="range_year_from2" placeholder="من ">
                                </div>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                
                                <i style="font-size:22px;padding:0.8rem" class="fa fa-arrow-circle-o-left"></i>
                                
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control" type="number" 
                                  style="font-size:13px;" name="range_year_to2" placeholder="  الى ">
                                </div>
                              </div>
                            </div>

                            <div class="d-flex flex-row">
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="font-size:13px;padding:0.8rem" >
                                السعر 
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control form-control-sm" type="number" 
                                  style="font-size:13px;"name="range_price_from2" placeholder="من ">
                                </div>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                
                                <i style="font-size:22px;padding:0.8rem" class="fa fa-arrow-circle-o-left"></i>
                                
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group">
                                  <input class="text-center form-control" type="number" 
                                  style="font-size:13px;" name="range_price_to2" placeholder="  الى ">
                                </div>
                              </div>
                            </div>


                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem" >
                                    ناقل الحركة 
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear21" type="checkbox" name="gears2[]" value="aut">
                                        <label for="gear21" class="choice-button-text">
                                        <img src="<?php echo e(url('/images/icons/automaticgear.png')); ?>" style="filter: brightness(10%) sepia(20%) saturate(10);" width=25>
                                        أوتماتيك</label>
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear22" type="checkbox" name="gears2[]" value="man" >
                                        <label for="gear22" class="choice-button-text"> 
                                        <img src="<?php echo e(url('/images/icons/normalgear.png')); ?>" style="filter: brightness(10%) sepia(20%) saturate(10);"width=25> يدوي
                                        </label>
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen" id="gear23" type="checkbox" name="gears2[]" value="sau" >
                                        <label for="gear23" class="choice-button-text">
                                        <img src="<?php echo e(url('/images/icons/semigear.png')); ?>"style="filter: brightness(10%) sepia(20%) saturate(10);" width=25>نصف أوتماتيك
                                        </label>
                                    </div> 
                                </div>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem" >
                                    الوقود 
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <input class="radiochoosen"  id="fuel21" type="checkbox" name="fuel2[]" value="bin" data-parsley-required-message="الوقود  مطلوب"
                                        required>
                                        <label for="fuel21" class="choice-button-text">بنزين</label>
                                        
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel22" type="checkbox" name="fuel2[]" value="des" >
                                        <label for="fuel22" class="choice-button-text">ديزل</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel23" type="checkbox" name="fuel2[]" value="gaz" >
                                        <label for="fuel23" class="choice-button-text">غاز</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel26" type="checkbox" name="fuel2[]" value="elec" >
                                        <label for="fuel26" class="choice-button-text">كهرباء</label>
                                    
                                    </div> 
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel24" type="checkbox" name="fuel2[]" value="binelec" >
                                        <label for="fuel24" class="choice-button-text">بنزين / كهرباء</label>
                                    
                                    </div> 
                                </div>
                                
                            </div>
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div style="font-size:13px;padding:0.8rem;visibility:hidden" >
                                    الوقود
                                    </div>
                                </div>
                                <div class="p-1">
                                    <div class="form-group">
                                    <input class="radiochoosen"  id="fuel27" type="checkbox" name="fuel2[]" value="deselec">
                                        <label for="fuel27" class="choice-button-text">ديزل / كهرباء</label>
                                
                                    </div> 
                                </div>
                                
                            </div>


                            <!--<div class="d-flex flex-row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker " multiple data-live-search="true"
                                            title="ناقل الحركة" name="gear2[]">
                                            <option value="aut">أوتماتيك</option>
                                            <option value="man">يدوي</option>
                                            <option value="sau">نصف أوتماتيك</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker pull-right " 
                                            multiple data-live-search="true" title="الوقود" name="fuel2[]">
                                            <option value="bin">بنزين</option>
                                            <option value="des">ديزل</option>
                                            <option value="binelec">بنزين / كهرباء</option>
                                            <option value="deselec">ديزل / كهرباء</option>
                                            <option value="gaz">غاز</option>
                                            <option value="elec">كهرباء</option>
                                        </select>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div> 
            </div>  </br>
            <div class="d-flex flex-row">
              <div class="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
                  <center>
                      <button type="submit" class="btn btn-danger btn-block" >
                          <span>بحث </span>
                      </button>
                  </center>
              </div>
            </div>
          </form></br>

          <div id="secondCardVehicles" class="card" style="display:none"> 
              <div class="card-body ">
                  <div class="row" >
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <div  class="secondCardVehiclesAppending row">
                              
                          </div>
                      </div>
                  </div>
              </div> 
          </div> 
        </div>   
    </div> 
    </br>
    <form class="form" method="POST" action="<?php echo e(route ('vehicle.compare')); ?>" novalidate>
        <?php echo csrf_field(); ?>
        <input type="text" name="id1" id="firstIdVechile" hidden>
        <input type="text" name="id2" id="secondIdVechile" hidden>
      <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
              <center>
                  <button type="submit" style="bottom:1.6rem; width:25%;background-color:#2c5cb8"
                  class="sticky  compareButton text-center btn btn-primary btn-block" >
                      <span>مقارنة </span>
                  </button>
</center>
          </div>
        
      </div>
    </form>
  </div>  
  <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>

<script>

  function custom_template(obj){
        if (!obj.id) { return obj.text; }
        var data = $(obj.element).data();
        var text = $(obj.element).text();
        if(data && data['img_src']){
          img_src = data['img_src'];

            width = data['img_width'];
            height = data['img_height'];
            obj = $("<div><img src=\"" + img_src + "\" style=\"width:"+ width +"; height:20px;\"/><span style=\"font-size:8pt;margin-bottom:0;float:left;\">" 
            + text + "</span></div>");

            return obj;
        }
  }

  function  templateSelection() {
    template = $("<span style=\"font-size:13px\" selected> العلامة التجارية</span>");
  	return template;
  }

  var options = {
      'placeholder': 'العلامة التجارية',
      'templateSelection': custom_template,
      'templateResult': custom_template,
  };
  
    /*$('#make_id1').select2({
        templateSelection: custom_template,
        templateResult: custom_template,
    });*/
  $('.make_id1').select2(options);
  $('.make_id2').select2(options);
  $('.select2-container--default .select2-selection--single').css({'height': '39px'});
  

  
  //$('.select2-container--default .select2-selection--single').css({'height': '39px'});

    $('#make_id1').change(function() {
        make_id1 = $(this).val();
        console.log(make_id1);
        var html="";
        $.ajax({
            url: '/compare/'+ make_id1,
            method:"GET",
            dataType:"json",
            
            success: function(data) {
                $('#model_id1').empty();
                var datalength = data.length;
                console.log(data);
                console.log(datalength);
                $('#model_id1').append('<option disabled selected>الموديل</option>');
                for(var count=0; count < datalength; count++) {
                    console.log(data[count].name);
                    $('#model_id1').append($('<option>', { 
                        value: data[count].id,
                        text : data[count].name,
                    }));
                }
                $('#model_id1').change(function() {
                    id = $(this).val();
                    name = $("#model_id1 option:selected").text();
                    console.log('name' + name);
                    $.ajax({
                        url: '/model/'+ id,
                        method:"GET",
                        dataType:"json",
                        success: function(data) {
                            console.log(data);
                            $('#model_id1').empty();
                            $('#model_id1').append('<option value="'+ id +'" disabled selected>'+ name+ '<span></span> ');
                            for(var i=0; i < data.length; i++) {
                                console.log(data[i].name);
                                $('#model_id1').append($('<option>', { 
                                    value: data[i].id,
                                    text : data[i].name
                                }));
                            }
                        }
                    });
                });
            }
        });
    });

    $('#make_id2').change(function() {
        make_id2 = $(this).val();
        console.log(make_id2);
        $.ajax({
            url: '/compare/'+ make_id2,
            method:"GET",
            dataType:"json",
            
            success: function(data) {
                $('#model_id2').empty();
                var datalength = data.length;
                $('#model_id2').append($('<option selected disapled>', { 
                    text : 'الموديل'
                }));
                for(var count=0; count < datalength; count++) {
                    console.log(data[count].name);
                    $('#model_id2').append($('<option>', { 
                        value: data[count].id,
                        text : data[count].name
                    }));
                }
            }
        });
    });

    $('#compareFirstForm').on('submit', function(event){
        var data =  $(this).serialize();
        console.log(data);
        var html="", fuel = "كهرباء", gear = "نصف أوتماتيك", used= "مستعمل", image = "", price="";
        event.preventDefault();
        $.ajax({
        url : '/compare',
        method:"POST",
        data: data,
        dataType:"json",
        success:function(data)
        {
            $("#firstCardVehicles").css("display", "block");
            for(var count= 0; count < data.length; count++) {

            if(data[count].first_payment == null) price= "";
            image = "/storage/vehicles/"+ data[count].file_name;
            if(data[count].gear=="aut") gear= "أوتماتيك";
            else if(data[count].gear=="man") gear= "يدوي";

            if(data[count].fuel=="bin") fuel = "بنزين";
            else if(data[count].fuel=="des") fuel = "ديزل";
            else if(data[count].fuel=="binelec") fuel = "بنزين / كهرباء";
            else if(data[count].fuel=="deselec") fuel = "ديزل / كهرباء";
            else if(data[count].fuel=="gaz") fuel = "غاز";

            html += '<div class="checkFirstCardCompare col-lg-6 col-md-6 col-sm-6">';
            html += '<a id="checkFirstCardCompare" data-id="'+ data[count].id +'">';
            html += '<div id="checkFirstCard'+ data[count].id +'" class="card p-1 mb-1 rounded">';
            html += '<div class="card-body" style="padding: 0.1rem"><div class="col-lg-12 col-md-12">';
            html += '<div class="row"><div class="col-lg-12 col-md-12"><center><label style="font-size:12px; color:#595959; font-weight: bold;">'+ data[count].name +', ';
            html += ''+ data[count].model_name +'</label></center></div></div></div>';
            html += '<div class="col-lg-12 col-md-12"><div class="row"><div style="width:100%; height: 150;" ';
            html += 'class="col-lg-7 col-md-7 ml-auto mr-auto"><img class="img-responsive" style="width:100%; ';
            html += 'height: 150px;object-fit:contain" src="'+ image +'">';
            html += '</div><div class="col-lg-5 col-md-5"><div class="row"><label style="color: black; font-size:12px">';
            html += 'السعر  : <span style=" font-size:10px">'+ price +'</span></label></div>';
            html += '<div class="row"><label style="color: black; font-size:12px">القوة  :<span style="font-size:10px">';
            html += ''+ data[count].hp +'</span></label></div><div class="row"><label style="color: black; font-size:12px">';
            html += 'ناقل الحركة : <span style="font-size:10px">'+ gear +'</span> </label> </div>';
            html += '<div class="row"><label style="color: black; font-size:11px">الوقود : <span style="font-size:10px">';
            html += ''+ fuel +' </span> </label></div><div class="row"><label style="color: black; font-size:11px">';
            html += 'سنة الانتاج  : <span style="font-size:10px">'+ data[count].year_of_product +'</span></label></div>';
            html += '<div class="row"><label style="color: black; font-size:11px">حالة المركبة :  <span style="font-size:10px">';
            html += 'جديد</span> </label></div></div></div></div></div> </div></a></div>';

            }
            $('.firstCardVehiclesAppending').empty().append(html);
            var id1 = 0;
            $('.checkFirstCardCompare').on('click', 'a', function(e){  
                id1 = $(this).attr('data-id');
                console.log(id1);
                $("#firstIdVechile").val(id1);
            //  alert($("#firstIdVechile").val());
            
                $("#checkFirstCard"+id1).toggleClass("bg-primary text-white");
            });
        },
        });
    });

    $('#compareSecondForm').on('submit', function(event){
        var data =  $(this).serialize();
        console.log(data);
        var html="", fuel = "كهرباء", gear = "نصف أوتماتيك", used= "مستعمل", image = "", price="";
        event.preventDefault();
        $.ajax({
        url : '/vehicle/compare1',
        method:"POST",
        data: data,
        dataType:"json",
        success:function(data)
        {
            $("#secondCardVehicles").css("display", "block");
            for(var count= 0; count < data.length; count++) {

            if(data[count].first_payment == null) price= "";
            image = "/storage/vehicles/"+ data[count].file_name;
            if(data[count].gear=="aut") gear= "أوتماتيك";
            else if(data[count].gear=="man") gear= "يدوي";

            if(data[count].fuel=="bin") fuel = "بنزين";
            else if(data[count].fuel=="des") fuel = "ديزل";
            else if(data[count].fuel=="binelec") fuel = "بنزين / كهرباء";
            else if(data[count].fuel=="deselec") fuel = "ديزل / كهرباء";
            else if(data[count].fuel=="gaz") fuel = "غاز";

            html += '<div class="checkSecondCardCompare col-lg-6 col-md-6 col-sm-6">';
            html += '<a id="checkSecondCardCompare" data-id="'+ data[count].id +'">';
            html += '<div id="checkSecondCard'+ data[count].id +'" class="card p-1 mb-1 rounded">';
            html += '<div class="card-body" style="padding: 0.1rem"><div class="col-lg-12 col-md-12">';
            html += '<div class="row"><div class="col-lg-12 col-md-12"><center><label style="font-size:12px; color:#595959; font-weight: bold;">'+ data[count].name +', ';
            html += ''+ data[count].model_name +'</label></center></div></div></div>';
            html += '<div class="col-lg-12 col-md-12"><div class="row"><div style="width:100%; height: 150;" ';
            html += 'class="col-lg-7 col-md-7 ml-auto mr-auto"><img class="img-responsive" style="width:100%; ';
            html += 'height: 150px;object-fit:contain" src="'+ image +'">';
            html += '</div><div class="col-lg-5 col-md-5"><div class="row"><label style="color: black; font-size:12px">';
            html += 'السعر  : <span style=" font-size:10px">'+ price +'</span></label></div>';
            html += '<div class="row"><label style="color: black; font-size:12px">القوة  :<span style="font-size:10px">';
            html += ''+ data[count].hp +'</span></label></div><div class="row"><label style="color: black; font-size:12px">';
            html += 'ناقل الحركة : <span style="font-size:10px">'+ gear +'</span> </label> </div>';
            html += '<div class="row"><label style="color: black; font-size:11px">الوقود : <span style="font-size:10px">';
            html += ''+ fuel +' </span> </label></div><div class="row"><label style="color: black; font-size:11px">';
            html += 'سنة الانتاج  : <span style="font-size:10px">'+ data[count].year_of_product +'</span></label></div>';
            html += '<div class="row"><label style="color: black; font-size:11px">حالة المركبة :  <span style="font-size:10px">';
            html += 'جديد</span> </label></div></div></div></div></div> </div></a></div>';

            }
            $('.secondCardVehiclesAppending').empty().append(html);
            var id2 = 0;
            $('.checkSecondCardCompare').on('click', 'a', function(e){  
                id2 = $(this).attr('data-id');
                $("#secondIdVechile").val(id2);
                
                $("#checkSecondCard"+id2).toggleClass("bg-primary text-white");
            });
        },
        });
    });

  
    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/compare/index.blade.php ENDPATH**/ ?>