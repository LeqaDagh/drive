
<div class="float-right row" id="addCar" class="addCar">
    <div class="col-md-12" id="viewCompanyTitle">
        <label>الشركة </label>
        <input style="border: 0 !important;  box-shadow: none !important;background-color: white 
            !important;cursor: default !important;" disabled >
        <input id="make" name="make_id" style="display:none; border: 0 !important; 
            box-shadow: none !important;background-color: white !important;cursor: default !important;"
        placeholder="<?php echo e(__('  ')); ?>" type="text" data-parsley-required-message="   الشركة مطلوب"
        disabled required>
    </div>
</div>
<div id="breakAllCompany">
    </br></br></br>
</div>

<div id="bodyTypeLogos" class="justify-content-around flex-wrap row">
    <?php $__currentLoopData = $firstMakes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-lg-3 col-md-3 col-sm-3 logos">
        <div class=" card-stats">
            <center>
                <div class="card-body ">
                    <a class="modelsCars" data-id="<?php echo e($data->id); ?>" >
                        <div >
                            <img src="<?php echo e(url('/images/logos/'.$data->logo)); ?>" width=<?php echo e($data->web_width); ?>; 
                            height=<?php echo e($data->web_height); ?>;>
                        </div>
                    </a>
                </div>
                <div class="stats">
                    <?php echo e($data->name); ?>

                </div>
            </center>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div></br>

<div class="row" >
    <div class="col-lg-2 col-md-2 col-sm-2">
    
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9">
        <div id="bodyTypeLogoChoosen" class="d-flex flex-row">
            
        </div></br>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>


<div class="row" id="viewAllCompany">
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 ml-auto mr-auto">
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="toggle btn btn-primary btn-block"  data-toggle="modal" 
                    data-target="#logos-modal" style="background:#0070B0">
                    <span>  اظهار كافة الشركات </span><i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div></br>


<div class="modal" id="logos-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="modal-title">الشركات</h5>
            </div>
            <div class="modal-body">
                <div class="justify-content-around flex-wrap row">
                    <?php $__currentLoopData = $makes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 logos">
                        <div class=" card-stats">
                            <center>
                                <div class="card-body ">
                                    <a class="modelsCars" data-id="<?php echo e($data->id); ?>">
                                        <div >
                                            <img src="<?php echo e(url('/images/logos/'.$data->logo)); ?>"  width=<?php echo e($data->web_width); ?>; 
                            height=<?php echo e($data->web_height); ?>;>
                                        </div>
                                    </a>
                                </div>
                                <div class="stats">
                                <?php echo e($data->name); ?>

                                </div>
                            </center>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" type="button" class="text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(window).resize(function() {

        if ($(this).width() < 500) {
            $('.logos').removeClass( "col-lg-3 col-md-3 col-sm-3" ).addClass( "p-1" );
        } else if ($(this).width() > 500) {
            $('.logos').removeClass( "p-2" ).addClass( "col-lg-3 col-md-3 col-sm-3" );
        }

    });

</script><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/vehicle/logos.blade.php ENDPATH**/ ?>