

<?php $__env->startSection('content'); ?>
    <?php if(isset($canNotCompare)): ?> 
        <div class="row">
            <div class="col-md-12">
                <center>
                    <div class="col-md-5">
                        <div id="errorAdding" class="alert alert-danger">يرجى اختيار مركبتين على الأقل</div>
                    </div>
                </center>
            </div>
        </div>
    <?php endif; ?>

    <?php if(isset($vehicle)): ?> 
    <?php if(isset($vehicle2)): ?>

    <div class="row" > 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
        <center>
            <div class="d-flex flex-row">
                <div class="main col-lg-5 col-md-5 ml-auto mr-auto">
                    <div class="">
                        <div class="row">
                            <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                <h6 >
                                <?php if(isset($make[0])): ?>
                                    <?php echo e($make[0]->name); ?>, <?php echo e($make[0]->model_name); ?>

                                </h6>
                                <?php endif; ?>
                            </div>   
                        </div> 
                        <div class=" ">
                            <div class="card-body ">
                            <?php if(isset($collection)): ?>
                                <div class="text-center slideshow-container2">
                                    <?php $__currentLoopData = $collection; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="mySlides ">
                                        <a href="<?php echo e(route('vec-seller.all', $vehicle->id)); ?>">
                                            <img class="img-responsive" src="<?php echo e(strstr($data->thumb_path, '/storage/')); ?>" 
                                            style="width:100%; height: 180px; object-fit: contain">
                                        </a>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <div class="text-center" >
                                    <?php for($i = 1; $i <= count($collection); $i++): ?>
                                        <span class="dot" onclick="currentSlide(<?php echo e($i); ?>)"></span> 
                                    <?php endfor; ?>
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="main col-lg-5 col-md-5 ml-auto mr-auto">
                    <div class="">
                        <div class="row">
                            <div class="text-center col-lg-12 col-md-12 ml-auto mr-auto">
                                <h6>
                                <?php if(isset($make2[0])): ?>
                                <?php echo e($make2[0]->name); ?>, <?php echo e($make2[0]->model_name); ?>

                                </h6>
                                <?php endif; ?>
                            </div>   
                        </div> 
                        <div class=" ">
                            <div class="card-body ">
                            <?php if(isset($collection2)): ?>
                                <div class="text-center slideshow-container2">
                                    <?php $__currentLoopData = $collection2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="mySlides2">
                                        <a href="<?php echo e(route('vec-seller.all', $vehicle->id)); ?>">
                                            <img class="img-responsive" src="<?php echo e(strstr($data->thumb_path, '/storage/')); ?>" 
                                            style="width:100%; height: 180px; object-fit: contain">
                                        </a>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <div class="text-center" >
                                    <?php for($i = 1; $i <= count($collection2); $i++): ?>
                                        <span class="dot2" onclick="currentSlide2(<?php echo e($i); ?>)"></span> 
                                    <?php endfor; ?>
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>  
            </div> 
        </center>
        </div>  
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div> 

    <div class="row" > 
        <div class="col-md-10">
            

            
        </div>
        
    </div>


    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> اللون الخارجي</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <i class="fa fa-circle fa-2x" style="color: #<?php echo e(substr($vehicle->body_color, 1)); ?>"></i>
                </div>
                <div class="text-center col-md-6">
                    <i class="fa fa-circle fa-2x" style="color: #<?php echo e(substr($vehicle2->body_color, 1)); ?>"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> اللون الداخلي</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <i class="fa fa-circle fa-2x" style="color: #<?php echo e(substr($vehicle->interior_color, 1)); ?>"></i>
                </div>
                <div class="text-center col-md-6">
                    <i class="fa fa-circle fa-2x" style="color: #<?php echo e(substr($vehicle2->interior_color, 1)); ?>"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> القوة </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareHeaders"><?php echo e($vehicle->power); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->power); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  ناقل الحركة</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle->gear == 'aut'): ?>
                        أوتماتيك  
                        <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                        يدوي    
                        <?php else: ?> نصف أوتماتيك
                        <?php endif; ?>
                        <?php endif; ?>
                    </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle2->gear == 'aut'): ?>
                        أوتماتيك  
                        <?php else: ?> <?php if($vehicle2->gear == 'man'): ?>
                        يدوي    
                        <?php else: ?> نصف أوتماتيك
                        <?php endif; ?>
                        <?php endif; ?>
                    </label>
                </div>
            </div>
        </div>
        
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الوقود</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle->fuel == 'bin'): ?>
                        بنزين  
                        <?php else: ?> <?php if($vehicle->gear == 'des'): ?>
                        ديزل  
                        <?php else: ?> <?php if($vehicle->gear == 'binelec'): ?>
                        بنزين / كهرباء  
                        <?php else: ?> <?php if($vehicle->gear == 'deselec'): ?>
                        ديزل / كهرباء 
                        <?php else: ?> <?php if($vehicle->gear == 'gaz'): ?>
                        غاز    
                        <?php else: ?> كهرباء
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle2->fuel == 'bin'): ?>
                        بنزين  
                        <?php else: ?> <?php if($vehicle2->gear == 'des'): ?>
                        ديزل  
                        <?php else: ?> <?php if($vehicle2->gear == 'binelec'): ?>
                        بنزين / كهرباء  
                        <?php else: ?> <?php if($vehicle2->gear == 'deselec'): ?>
                        ديزل / كهرباء 
                        <?php else: ?> <?php if($vehicle2->gear == 'gaz'): ?>
                        غاز    
                        <?php else: ?> كهرباء
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> سنة الانتاج </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle->year_of_product); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->year_of_product); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  سنة الترخيص</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle->year_of_work); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->year_of_work); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  عدد المقاعد </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle->num_of_seats == 's1'): ?>
                        2+1  
                        <?php else: ?> <?php if($vehicle->gear == 's2'): ?>
                        3+1  
                        <?php else: ?> <?php if($vehicle->gear == 's3'): ?>
                        4+1  
                        <?php else: ?> <?php if($vehicle->gear == 's4'): ?>
                        5+1    
                        <?php else: ?> 6+1
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    <label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle2->num_of_seats == 's1'): ?>
                        2+1  
                        <?php else: ?> <?php if($vehicle2->gear == 's2'): ?>
                        3+1  
                        <?php else: ?> <?php if($vehicle2->gear == 's3'): ?>
                        4+1  
                        <?php else: ?> <?php if($vehicle2->gear == 's4'): ?>
                        5+1    
                        <?php else: ?> 6+1
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    <label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> نظام الدفع</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <?php if($vehicle->drivetrain_system == 'fbf'): ?>
                        <label class="fontSizeCompareValue">4X4</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">2X4</label>
                    <?php endif; ?>
                </div>
                <div class="text-center col-md-6">
                    <?php if($vehicle2->drivetrain_system == 'fbf'): ?>
                        <label class="fontSizeCompareValue">4X4</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">2X4</label>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> نوع الدفع </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <?php if($vehicle->drivetrain_type == 'fro'): ?>
                        <label class="fontSizeCompareValue">أمامي</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">خلفي</label>
                    <?php endif; ?>
                </div>
                <div class="text-center col-md-6">
                    <?php if($vehicle2->drivetrain_type == 'fro'): ?>
                        <label class="fontSizeCompareValue">أمامي</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">خلفي</label>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">   الهيكل</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <?php if($vehicle->model_name == 'hat'): ?>
                        <label class="fontSizeCompareValue">hatchback</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'cou'): ?>
                        <label class="fontSizeCompareValue">coupe</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'sed'): ?>
                        <label class="fontSizeCompareValue">sedan</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'sta'): ?>
                        <label class="fontSizeCompareValue">station</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'spo'): ?>
                        <label class="fontSizeCompareValue">sport-car</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'cab'): ?>
                        <label class="fontSizeCompareValue">cabriolet</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'pic'): ?>
                        <label class="fontSizeCompareValue">pickup</label>
                    <?php else: ?> <?php if($vehicle->model_name == 'off'): ?>
                        <label class="fontSizeCompareValue">off-road</label>
                    <?php else: ?> <label class="fontSizeCompareValue">station-wagon</label>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="text-center col-md-6">
                    <?php if($vehicle2->model_name == 'hat'): ?>
                        <label class="fontSizeCompareValue">hatchback</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'cou'): ?>
                        <label class="fontSizeCompareValue">coupe</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'sed'): ?>
                        <label class="fontSizeCompareValue">sedan</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'sta'): ?>
                        <label class="fontSizeCompareValue">station</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'spo'): ?>
                        <label class="fontSizeCompareValue">sport-car</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'cab'): ?>
                        <label class="fontSizeCompareValue">cabriolet</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'pic'): ?>
                        <label class="fontSizeCompareValue">pickup</label>
                    <?php else: ?> <?php if($vehicle2->model_name == 'off'): ?>
                        <label class="fontSizeCompareValue">off-road</label>
                    <?php else: ?> <label class="fontSizeCompareValue">station-wagon</label>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> المسافة المقطوعة </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle->mileage); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->mileage); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> عدد المفاتيح </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle->num_of_keys == 'kes1'): ?>
                        1 
                        <?php else: ?> <?php if($vehicle->gear == 'kes2'): ?>
                        2 
                        <?php else: ?> 
                        3   
                        <?php endif; ?>
                        <?php endif; ?>
                    <label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue">
                        <?php if($vehicle2->num_of_keys == 'kes1'): ?>
                        1 
                        <?php else: ?> <?php if($vehicle2->gear == 'kes2'): ?>
                        2 
                        <?php else: ?> 
                        3   
                        <?php endif; ?>
                        <?php endif; ?>
                    <label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text"> الأصل</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <?php if($vehicle->origin == 'spc'): ?>
                        <label class="fontSizeCompareValue">خصوصي</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">عمومي</label>
                    <?php endif; ?>
                </div>
                <div class="text-center col-md-6">
                    <?php if($vehicle2->origin == 'spc'): ?>
                        <label class="fontSizeCompareValue">خصوصي</label>
                    <?php else: ?> 
                        <label class="fontSizeCompareValue">عمومي</label>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">   الاٍعفاء الجمركي</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle->customs_exemption); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->customs_exemption); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  تاريخ اٍنتهاء الرخصة </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="d-flex flex-row">
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle->license_date); ?> </label>
                </div>
                <div class="text-center col-md-6">
                    <label class="fontSizeCompareValue"><?php echo e($vehicle2->license_date); ?> </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-primary btn-block " style="background-color:#0875ba">
                        <span class="fontSizeCompareHeaders" style=" color:white">    اٍضافات خارجية </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if(isset($furniture[0]) && isset($furniture2[0]) && $furniture[0]->equipment_value !== null && $furniture2[0]->equipment_value !== null): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">    نوع الفرش </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($furniture[0]) && $furniture[0]->equipment_value !== null): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if($furniture[0]->equipment_value == 'ext_int_furniture_leat'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                                        <label for="ext_int_furniture1" class="fontSizeCompareValue choice-button-text">جلد</label>
                                        <?php else: ?> <?php if($furniture[0]->equipment_value == 'ext_int_furniture_colt'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">قماش</label>
                                        <?php else: ?> <?php if($furniture[0]->equipment_value == 'ext_int_furniture_lndc'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">جلد + قماش</label>
                                        <?php else: ?> <?php if($furniture[0]->equipment_value == 'ext_int_furniture_sprt'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">رياضي</label>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">مخمل</label>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($furniture2[0]) && $furniture2[0]->equipment_value !== null): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if($furniture2[0]->equipment_value == 'ext_int_furniture_leat'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                                        <label for="ext_int_furniture1" class="fontSizeCompareValue choice-button-text">جلد</label>
                                        <?php else: ?> <?php if($furniture2[0]->equipment_value == 'ext_int_furniture_colt'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">قماش</label>
                                        <?php else: ?> <?php if($furniture2[0]->equipment_value == 'ext_int_furniture_lndc'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">جلد + قماش</label>
                                        <?php else: ?> <?php if($furniture2[0]->equipment_value == 'ext_int_furniture_sprt'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">رياضي</label>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">مخمل</label>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($sunroof[0]) && $sunroof[0]->equipment_value!== null)  ||   (isset($sunroof2[0]) && $sunroof2[0]->equipment_value !== null)): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  فتحة السقف </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($sunroof[0]) && $sunroof[0]->equipment_value !== null): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if($sunroof[0]->equipment_value == 'ext_int_sunroof_norm'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                                        <label for="ext_int_furniture1" class="fontSizeCompareValue choice-button-text">عادية</label>
                                        <?php else: ?> <?php if($sunroof[0]->equipment_value == 'ext_int_sunroof_pano'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">بانوراما</label>
                                        <?php else: ?> <?php if($sunroof[0]->equipment_value == 'ext_int_sunroof_conv'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">سقف متحرك</label>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($sunroof2[0]) && $sunroof2[0]->equipment_value !== null): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if($sunroof2[0]->equipment_value == 'ext_int_sunroof_norm'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                                        <label for="ext_int_furniture1" class="fontSizeCompareValue choice-button-text">عادية</label>
                                        <?php else: ?> <?php if($sunroof2[0]->equipment_value == 'ext_int_sunroof_pano'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">بانوراما</label>
                                        <?php else: ?> <?php if($sunroof2[0]->equipment_value == 'ext_int_sunroof_conv'): ?>
                                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                                        <label for="ext_int_furniture2" class="fontSizeCompareValue choice-button-text">سقف متحرك</label>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($steering) && $steering!== '')  ||   (isset($steering2) && $steering2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  المقود </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($steering) && $steering !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($steering, 'ext_int_steering_refi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">غيارات مقود</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($steering, 'ext_int_steering_stco'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحكم مقود</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($steering, 'ext_int_steering_hste'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تدفئة مقود</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($steering2) && $steering2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($steering2, 'ext_int_steering_refi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">غيارات مقود</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($steering2, 'ext_int_steering_stco'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحكم مقود</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($steering2, 'ext_int_steering_hste'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تدفئة مقود</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($seats) && $seats!== '') ||   (isset($seats2) && $seats2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  المقاعد </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($seats) && $seats !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_htst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تدفئة مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_clst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تبريد مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_mast'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مساج مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_spst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مقاعد رياضية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_elst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحكم كهرباء</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_mest'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">ذاكرة مقاعد</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($seats2) && $seats2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($seats2, 'ext_int_seats_htst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تدفئة مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats2, 'ext_int_seats_clst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تبريد مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats2, 'ext_int_seats_mast'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مساج مقاعد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats2, 'ext_int_seats_spst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مقاعد رياضية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats2, 'ext_int_seats_elst'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحكم كهرباء</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($seats, 'ext_int_seats_mest'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">ذاكرة مقاعد</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($screens) && $screens!== '')  ||   (isset($screens2) && $screens2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الشاشات </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($screens) && $screens !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($screens, 'ext_int_screens_frsc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">شاشة امامية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens, 'ext_int_screens_basc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">شاشات خلفية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens, 'ext_int_screens_blue'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">بلوتوث</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens, 'ext_int_screens_ebph'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قاعدة بلوتوث للهاتف</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens, 'ext_int_screens_usb'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">USB</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($screens2) && $screens2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($screens2, 'ext_int_screens_frsc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">شاشة امامية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens2, 'ext_int_screens_basc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">شاشات خلفية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens2, 'ext_int_screens_blue'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">بلوتوث</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens2, 'ext_int_screens_ebph'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قاعدة بلوتوث للهاتف</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($screens2, 'ext_int_screens_usb'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">USB</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($glass) && $glass!== '')  ||   (isset($glass2) && $glass2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الزجاج </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($glass) && $glass !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($glass, 'ext_int_glass_egls'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">كهربائي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($glass, 'ext_int_glass_lsrd'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> تعتيم ليزر</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($glass2) && $glass2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($glass2, 'ext_int_glass_egls'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">كهربائي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($glass2, 'ext_int_glass_lsrd'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> تعتيم ليزر</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  اضافات داخلية أخرى </span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($intOthers) && $intOthers !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($intOthers, 'ext_int_other_eio1'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">زر تشغيل</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers, 'ext_int_other_eio2'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة داخلية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers, 'ext_int_other_eio3'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام صوتي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers, 'ext_int_other_eio4'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍغلاق مركزي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers, 'ext_int_other_eio5'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">وسائد هوائية</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($intOthers2) && $intOthers2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($intOthers2, 'ext_int_other_eio1'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">زر تشغيل</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers2, 'ext_int_other_eio2'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة داخلية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers2, 'ext_int_other_eio3'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام صوتي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers2, 'ext_int_other_eio4'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍغلاق مركزي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($intOthers2, 'ext_int_other_eio5'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">وسائد هوائية</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-primary btn-block " style="background-color:#0875ba">
                        <span class="fontSizeCompareHeaders" style="color:white">    اٍضافات داخلية </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($lights) && $lights!== '')  ||   (isset($lights2) && $lights2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الاٍضاءة </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($lights) && $lights !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($lights, 'ext_ext_light_znon'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">زينون</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($lights, 'ext_ext_light_ledl'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> ليد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($lights, 'ext_ext_light_fogl'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">ضباب</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($lights2) && $lights2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($lights2, 'ext_ext_light_znon'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">زينون</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($lights2, 'ext_ext_light_ledl'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> ليد</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($lights2, 'ext_ext_light_fogl'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">ضباب</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($mirrors) && $mirrors!== '')  ||   (isset($mirrors2) && $mirrors2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  المرايا </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($mirrors) && $mirrors !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($mirrors, 'ext_ext_mirrors_limi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة مرايا</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($mirrors, 'ext_ext_mirrors_clel'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> اٍغلاق كهرباء</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($mirrors2) && $mirrors2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($mirrors2, 'ext_ext_mirrors_limi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة مرايا</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($mirrors2, 'ext_ext_mirrors_clel'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> اٍغلاق كهرباء</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($cameras) && $cameras!== '')  ||   (isset($cameras2) && $cameras2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الكاميرات   </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($cameras) && $cameras !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($cameras, 'ext_ext_cameras_frnt'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">أمامية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($cameras, 'ext_ext_cameras_back'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> خلفية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($cameras, 'ext_ext_cameras_tsdc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> 360 درجة</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($cameras2) && $cameras2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($cameras2, 'ext_ext_cameras_frnt'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">أمامية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($cameras2, 'ext_ext_cameras_back'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> خلفية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($cameras2, 'ext_ext_cameras_tsdc'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> 360 درجة</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php if((isset($sensors) && $sensors!== '')  ||   (isset($sensors2) && $sensors2 !== '')): ?>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-gray btn-block disabled" style="background-color:#e1e1d0">
                        <span class="fontSizeCompareHeaders gray-text">  الحساسات </span>
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($sensors) && $sensors !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_frot'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">امامي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_bake'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">خلفي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_tsds'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">360 درجة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_rain'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مطر</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_ligh'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_colb'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مانع اٍصطدام</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_blin'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">النقطة العمياء</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_sele'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحديد مسار</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_sign'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قارئ اٍشارات</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors, 'ext_ext_sensors_self'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍصطفاف ذاتي</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($sensors2) && $sensors2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_frot'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">امامي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_bake'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">خلفي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_tsds'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">360 درجة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_rain'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مطر</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_ligh'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍضاءة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_colb'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">مانع اٍصطدام</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_blin'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">النقطة العمياء</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_sele'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">تحديد مسار</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_sign'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قارئ اٍشارات</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($sensors2, 'ext_ext_sensors_self'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">اٍصطفاف ذاتي</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-primary btn-block " style="background-color:#0875ba">
                            <span class="fontSizeCompareHeaders" style="color:white">    اضافات أخرى </span>
                        </a>
                    </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ml-auto mr-auto">
                    <div class="d-flex flex-row">
                        <?php if(isset($genOther) && $genOther !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_fing'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">بصمة أبواب</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_remo'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">صدّام أمامي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_elec'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قيادة ذاتية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_ecos'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام ECO</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_abss'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام ABS</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_navi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام خوارط</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_door'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">أبواب شفط</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_main'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">دفتر صيانة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_safe'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">رادار قياس مسافة الأمان</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_serv'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> دفتر خدمات</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther, 'ext_gen_other_crui'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">محدد سرعة</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($genOther2) && $genOther2 !== ''): ?>
                        <div class="text-center col-md-6">
                            <div class="d-flex flex-row">
                                <div class="p-1">
                                    <div class="form-group">
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_fing'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">بصمة أبواب</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_remo'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">صدّام أمامي</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_elec'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">قيادة ذاتية</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_ecos'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام ECO</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_abss'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام ABS</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_navi'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">نظام خوارط</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_door'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">أبواب شفط</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_main'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">دفتر صيانة</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_safe'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">رادار قياس مسافة الأمان</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_serv'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text"> دفتر خدمات</label>
                                        <?php endif; ?>
                                        <?php if(strlen(strpos($genOther2, 'ext_gen_other_crui'))): ?>
                                        <input class="radiochoosen" type="radio" checked>
                                        <label class="fontSizeCompareValue choice-button-text">محدد سرعة</label>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>

    <script>
        // Images Slider for information
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}    
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";  
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";  
            dots[slideIndex-1].className += " active";
        }

        // Images Slider for information
        var slideIndex2 = 1;
        showSlides2(slideIndex2);

        function plusSlides2(n2) {
            showSlides2(slideIndex2 += n2);
        }

        function currentSlide2(n2) {
            showSlides2(slideIndex2 = n2);
        }

        function showSlides2(n2) {
            var i2;
            var slides2 = document.getElementsByClassName("mySlides2");
            var dots2 = document.getElementsByClassName("dot2");
            if (n2 > slides2.length) {slideIndex2 = 1}    
            if (n2 < 1) {slideIndex2 = slides2.length}
            for (i2 = 0; i2 < slides2.length; i2++) {
                slides2[i2].style.display = "none";  
            }
            for (i2 = 0; i2 < dots2.length; i2++) {
                dots2[i2].className = dots2[i2].className.replace(" active", "");
            }
            slides2[slideIndex2-1].style.display = "block";  
            dots2[slideIndex2-1].className += " active";
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/compare/result.blade.php ENDPATH**/ ?>