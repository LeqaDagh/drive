


<?php $__env->startSection('content'); ?>
<style>

.cardStyle {padding:0.5rem;border-radius: 10px;background:#0070B0;
    width:70%}
</style>
<div class="row" >
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class=" ">
                <div class="">
                    <center>
                        <h5>
                        المزيد
                        </h5>
                    </center>
                </div> <hr>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <?php if(auth()->guard()->check()): ?>
                            <a id="toggleProfile" style="cursor:pointer;">
                                <div class="cardStyle card text-center h-100" >
                                    <div class="card-block">
                                        <h3>
                                        <i  style="color:white" class="material-icons" >person</i>
                                        </h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">حسابي</h6>
                                </div>
                            </a>
                            <?php endif; ?>

                            <?php if(auth()->guard()->guest()): ?>
                                <a href="<?php echo e(route('login')); ?>">
                                    <div class="cardStyle card text-center h-100" >
                                        <div class="card-block">
                                            <h3><i style="color:white"  class="material-icons" >transit_enterexit</i></h3>
                                        </div>
                                        <h6 style="font-size:14px;color:white"  class="card-title">تسجيل دخول</h6>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>  
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="<?php echo e(route('all-vehicle.index')); ?>">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons" >commute</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">المعارض</h6>
                                </div>
                            </a>
                        </div>  
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="<?php echo e(route('contact.index')); ?>">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons" >
                                        contact_phone
                                        </i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">اٍتصل بنا</h6>
                                </div>
                            </a>
                        </div> 
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="<?php echo e(route('about-us.index')); ?>">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons"  >info</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">من نحن</h6>
                                </div>
                            </a>
                        </div> 
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="<?php echo e(route('used.index')); ?>">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" id="toggleProfile" class="material-icons"  >
                                        lock</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title"> اٍتفاقية الاٍستخدام</h6>
                                </div>
                            </a>
                        </div> 
                    </div> </br>
                 
                </div>
                
            </div>
        
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/more/index.blade.php ENDPATH**/ ?>