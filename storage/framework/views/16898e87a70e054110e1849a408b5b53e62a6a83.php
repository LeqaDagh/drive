

<?php $__env->startSection('content'); ?>
<div class="row"> 
    <div class="col-lg-1 col-md-1 col-sm-1">   
    </div>
   
    <div class="col-lg-10 col-md-10 col-sm-10"> 
        <div class="row">
        <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-12 col-md-10 col-sm-10">
                <div class="card " style="padding: 0.8rem">
                <a href="<?php echo e(route('vec-seller.information', $vehicle->id)); ?>" class="text-muted">
                    <div class="card-body " style="padding: 0.4rem">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div style="padding:0.8rem"class="row">
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                        <h5 style="color:#595959; font-weight: bold;">
                                        <?php echo e($vehicle->name); ?> <?php echo e($vehicle->model_name); ?></h5>
                                    </div>   
                                </div>
                                <div id="carousel-vehicles<?php echo e($vehicle->id); ?>" class="text-center carousel slide" data-ride="carousel">
                                    <center>
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li data-target="#carousel-vehicles<?php echo e($vehicle->id); ?>" data-slide-to="<?php echo e($loop->index); ?>" 
                                                    class="<?php echo e($loop->first ? 'active' : ''); ?>"></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <?php $__currentLoopData = \App\Media::where('fileable_id', '=', $vehicle->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div style="width:120%; height: 210px; background-color:white"  class="carousel-item item <?php echo e($loop->first ? ' active' : ''); ?>" >
                                                    <a href="<?php echo e(route('vec-seller.all', $vehicle->id)); ?>">
                                                        <img src="<?php echo e('/storage/vehicles/'.$data->file_name); ?>" width="120%" 
                                                        height="210px" style="object-fit: contain ">
                                                    </a>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>

                                        <!-- Controls -->
                                        <a class="right carousel-control" href="#carousel-vehicles<?php echo e($vehicle->id); ?>" role="button" data-slide="prev">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="left carousel-control" href="#carousel-vehicles<?php echo e($vehicle->id); ?>" role="button" data-slide="next">
                                            <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </center> 
                                </div>
                                
                                <div class="row" > 
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                    <?php if($vehicle->vehicle_status == 'new'): ?>
                                        جديد  
                                    <?php else: ?>
                                        مستعمل
                                    <?php endif; ?>
                                    </div>  
                                </div>
                                <div class="text-center d-flex flex-row">
                                    <div class=" col-lg-6 col-md-6 ml-auto mr-auto">
                                        <i class="fa fa-circle " style="color: #<?php echo e(substr($vehicle->body_color, 1)); ?>"></i>
                                        <span style="font-size:14px;">اللون الخارجي </span> 
                                    </div> 
                                    <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                        <i class="fa fa-circle " style="color: #<?php echo e(substr($vehicle->interior_color, 1)); ?>"></i>
                                        <span style="font-size:14px;">اللون الداخلي </span> 
                                    </div> 
                                </div> 
                            </div>
                            <div style="padding:0.9rem"class="col-lg-6 col-md-6 col-sm-6">
                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="<?php echo e(url('/images/icons/price.png')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> السعر</label>
                                        </div>
                                        <div class="">
                                            <?php if($vehicle->price_type == 'price_type_types_fp1'): ?>
                                            <label style="font-size:13px"><?php echo e($vehicle->first_payment); ?> </label>
                                            <?php else: ?> <?php if($vehicle->price_type == 'price_type_types_fp2'): ?>
                                            <label style="font-size:13px"><?php echo e($vehicle->first_payment); ?> </label>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="<?php echo e(url('/images/icons/power.png')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px">القوة</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px"><?php echo e($vehicle->power); ?> </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <?php if($vehicle->gear == 'aut'): ?>
                                                <img src="<?php echo e(url('/images/icons/automaticgear.png')); ?>" width=30>
                                            <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                <img src="<?php echo e(url('/images/icons/normalgear.png')); ?>" width=30>   
                                            <?php else: ?>  <img src="<?php echo e(url('/images/icons/semigear.png')); ?>" width=30>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> ناقل الحركة</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                <?php if($vehicle->gear == 'aut'): ?>
                                                أوتماتيك  
                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                يدوي    
                                                <?php else: ?> نصف أوتماتيك
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    </div> 
                                </div><hr>
                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="<?php echo e(url('/images/icons/fuel.jpeg')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> الوقود</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                بنزين  
                                                <?php else: ?> <?php if($vehicle->fuel == 'des'): ?>
                                                ديزل  
                                                <?php else: ?> <?php if($vehicle->fuel == 'binelec'): ?>
                                                بنزين / كهرباء  
                                                <?php else: ?> <?php if($vehicle->fuel == 'deselec'): ?>
                                                ديزل / كهرباء 
                                                <?php else: ?> <?php if($vehicle->fuel == 'gaz'): ?>
                                                غاز    
                                                <?php else: ?> كهرباء
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="<?php echo e(url('/images/icons/8757.png')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px">سنة الانتاج</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px"><?php echo e($vehicle->year_of_product); ?> </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="<?php echo e(url('/images/icons/seat.jpeg')); ?>" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> عدد المقاعد</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                <?php if($vehicle->num_of_seats == 's1'): ?>
                                                2+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's2'): ?>
                                                3+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's3'): ?>
                                                4+1  
                                                <?php else: ?> <?php if($vehicle->num_of_seats == 's4'): ?>
                                                5+1    
                                                <?php else: ?> 6+1
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            <label>
                                        </div>
                                    </div> 
                                </div> 
                                <hr>
                                <div style="padding-top:0.7rem"class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="text-center d-flex flex-row">
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a  data-id="<?php echo e($vehicle->id); ?>" class="shareInformationLink"
                                                style="cursor:pointer;">
                                                <i  class=" material-icons" style="color:blue">share</i>
                                            </a>
                                            <p id="urlCopied" hidden></p>
                                        </div> 
                                       <!-- <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                                            <a data-id="<?php echo e($vehicle->id); ?>" id="compareVehicles" class="compareVehicles"
                                                style="cursor:pointer;">
                                                <i id="compareVehiclesIcon" class="compareVehiclesIcon material-icons" style="color:#004d4d">compare</i>
                                            </a>
                                        </div> -->
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="editVehicleById" 
                                            href="<?php echo e(route('vec-seller.edit', $vehicle->id)); ?>" >
                                                <i class="material-icons" style="color:gray">create</i>
                                            </a>
                                        </div> 
                                        
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="deleteVehicleById" data-toggle="modal" 
                                                data-target="#deleteModal" style="cursor:pointer;">
                                                <i class="material-icons" style="color:#cc0000">delete</i>
                                            </a>
                                        </div> 
                                        <div class="col-lg-2 col-md-2 col-sm-2  ml-auto mr-auto">
                                            <a id="editVehicleImagesById" href="<?php echo e(route('vec-seller.images', $vehicle->id)); ?>">
                                                <i class="material-icons" style="color:#660066">add_photo_alternate</i>
                                            </a>
                                        </div> 

                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="" data-toggle="modal" 
                                                data-target="#infoModal<?php echo e($vehicle->id); ?>" style="cursor:pointer;">
                                                <i class="material-icons" style="color:gray">info</i>
                                            </a>
                                        </div> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
                <div style="padding: 0.3rem"></div>
            </div>
            <div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="text-center modal-title" id="exampleModalLabel">حذف المركبة </h5>
                        </div>
                        <div class="modal-body">
                            <input id="id" name="id")>
                            <h6 class="text-center">تأكيد حذف المركية؟</h6>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <a type="button" class="btn btn-sm btn-info" data-dismiss="modal">الغاء</a>
                                <a href="<?php echo e(route('vehicle.delete', $vehicle->id)); ?>" class="btn btn-sm btn-danger">حذف</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal modal-secondary fade" id="infoModal<?php echo e($vehicle->id); ?>" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="text-center modal-header" 
                            style="background-color:#cc0000; padding:0.7rem">
                            <h6  style="color:white">
                             <?php echo e($vehicle->name); ?> , <?php echo e($vehicle->model_name); ?>

                            </h6>
                        </div>
                        <div class="modal-body" style="padding:0.9rem">
                            <div class="element<?php echo e($vehicle->id); ?>" id="select_txt">
                                
                                <span class="boxspan<?php echo e($vehicle->id); ?>" data-id="<?php echo e($vehicle->name); ?>"style="font-size:13px;">
                                <?php echo e($vehicle->name); ?> , <?php echo e($vehicle->model_name); ?>   <?php echo e($vehicle->year_of_product); ?> 
                                </span></br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  القوة </span>
                                <span class="boxspan"style="font-size:13px"> <?php echo e($vehicle->power); ?> ,  </span>
                                <span class="boxspan" style="font-size:13px;color:#595959; font-weight: bold;">عدد الأحصنة </span>
                                <span class="boxspan"style="font-size:13px"> <?php echo e($vehicle->hp); ?> </span></br>
                                
                                
                                <span class="boxspan"style="font-size:14px;color:#595959; font-weight: bold;">  المواصفات :  </span>
                                </br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  ناقل الحركة  :
                                </span>
                                <span class="boxspan"style="font-size:13px"> 
                                    <?php if($vehicle->fuel == 'bin'): ?>
                                    بنزين  
                                    <?php else: ?> <?php if($vehicle->fuel == 'des'): ?>
                                    ديزل  
                                    <?php else: ?> <?php if($vehicle->fuel == 'binelec'): ?>
                                    بنزين / كهرباء  
                                    <?php else: ?> <?php if($vehicle->fuel == 'deselec'): ?>
                                    ديزل / كهرباء 
                                    <?php else: ?> <?php if($vehicle->fuel == 'gaz'): ?>
                                    غاز    
                                    <?php else: ?> كهرباء
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </span></br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    الوقود :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    <?php if($vehicle->gear == 'aut'): ?>
                                    أوتماتيك  
                                    <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                    يدوي    
                                    <?php else: ?> نصف أوتماتيك
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </span></br>
                                
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    عدد المقاعد :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    <?php if($vehicle->num_of_seats == 's1'): ?>
                                    2+1  
                                    <?php else: ?> <?php if($vehicle->num_of_seats == 's2'): ?>
                                    3+1  
                                    <?php else: ?> <?php if($vehicle->num_of_seats == 's3'): ?>
                                    4+1  
                                    <?php else: ?> <?php if($vehicle->num_of_seats == 's4'): ?>
                                    5+1    
                                    <?php else: ?> 6+1
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </span></br>
                                
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    اللون الخارجي :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    <?php if($vehicle->body_color == 'c000000'): ?>
                                    أسود  
                                    <?php else: ?> <?php if($vehicle->body_color == 'c0bde16'): ?>
                                    أخضر    
                                    <?php else: ?> <?php if($vehicle->body_color == 'c120a0a'): ?>
                                    أسود مطفي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c0e103d'): ?>
                                    كحلي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c15b367'): ?>
                                    أزرق مخضر
                                    <?php else: ?> <?php if($vehicle->body_color == 'c1e14de'): ?>
                                    نيلي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c1e9dba'): ?>
                                    أزرق فاتح
                                    <?php else: ?> <?php if($vehicle->body_color == 'c215769'): ?>
                                    أزرق غامق
                                    <?php else: ?> <?php if($vehicle->body_color == 'c24070d'): ?>
                                    بنفسجي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c2a4221'): ?>
                                    زيتي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c362517'): ?>
                                    بني غامق
                                    <?php else: ?> <?php if($vehicle->body_color == 'c386352'): ?>
                                    أخضر غامق
                                    <?php else: ?> <?php if($vehicle->body_color == 'c544c4a'): ?>
                                    رمادي
                                    <?php else: ?> <?php if($vehicle->body_color == 'c6b6560'): ?>
                                    سكني
                                    <?php else: ?> <?php if($vehicle->body_color == 'c700d0d'): ?>
                                    أحمر قاني
                                    <?php else: ?> <?php if($vehicle->body_color == 'c70431e'): ?>
                                    بني
                                    <?php else: ?> <?php if($vehicle->body_color == 'c8fe813'): ?>
                                    فسفوري
                                    <?php else: ?> <?php if($vehicle->body_color == 'cba0909'): ?>
                                    خمري
                                    <?php else: ?> <?php if($vehicle->body_color == 'cc48f60'): ?>
                                    بني برونزي
                                    <?php else: ?> <?php if($vehicle->body_color == 'cc7c1bd'): ?>
                                    فضي
                                    <?php else: ?> <?php if($vehicle->body_color == 'cde6b18'): ?>
                                    برتقالي
                                    <?php else: ?> <?php if($vehicle->body_color == 'cf0e9e9'): ?>
                                    أبيض كريمي
                                    <?php else: ?> <?php if($vehicle->body_color == 'cfcba03'): ?>
                                    أصفر
                                    <?php else: ?> <?php if($vehicle->body_color == 'cff0000'): ?>
                                    أحمر
                                    <?php else: ?> <?php if($vehicle->body_color == 'cffadbb'): ?>
                                    زهري
                                    <?php else: ?> أبيض
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </span></br>
                            
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    اللون الداخلي :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    <?php if($vehicle->interior_color == 'c000000'): ?>
                                    أسود
                                    <?php else: ?> <?php if($vehicle->interior_color == 'c5e3b18'): ?>
                                    بني
                                    <?php else: ?> <?php if($vehicle->interior_color == 'c751010'): ?>
                                    خمري
                                    <?php else: ?> <?php if($vehicle->interior_color == 'c755196'): ?>
                                    بنفسجي
                                    <?php else: ?> <?php if($vehicle->interior_color == 'cb5b3b3'): ?>
                                    سكني
                                    <?php else: ?> <?php if($vehicle->interior_color == 'cc40a0a'): ?>
                                    أحمر
                                    <?php else: ?> <?php if($vehicle->interior_color == 'cc9902e'): ?>
                                    عسلي
                                    <?php else: ?> <?php if($vehicle->interior_color == 'cfbffd9'): ?>
                                    بيج
                                    <?php else: ?> أبيض
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </span>
                                </br>
                            
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  طريقة الدفعات :  </span>
                                <?php $__currentLoopData = \App\Equipment::where('vehicle_id', '=', $vehicle->id)->where('equipment_type', 'payment_method')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label style="font-size:13px">    
                                    <?php if($data->equipment_value == 'payment_method_types_pay1' ): ?>
                                    نقدا , 
                                    <?php endif; ?>
                                    <?php if($data->equipment_value == 'payment_method_types_pay2' ): ?>
                                    عن طريق البنك , 
                                    <?php endif; ?>
                                    <?php if($data->equipment_value == 'payment_method_types_pay3' ): ?>
                                    تقسيط مباشر , 
                                    <?php endif; ?>
                                    <?php if($data->equipment_value == 'payment_method_types_pay4' ): ?>
                                    دفعة أولى + شيكات شخصية </br>
                                    <?php endif; ?>
                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </br>
                                
                                <span class="boxspan"style="font-size:14px;color:#595959; font-weight: bold;">  للطلب أو الإستفسار  :  </span>
                                </br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">   رقم المحمول :  </span>
                                <label class="boxspan"style="font-size:13px">    
                                    <?php echo e((\App\User::where('id', '=', $vehicle->user_id)->select('seller_mobile')->first())->seller_mobile); ?>

                                </label>
                                </br>
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    العنوان :  </span>
                                <label class="boxspan"style="font-size:13px">    
                                    <?php echo e((\App\User::where('id', '=', $vehicle->user_id)->select('address')->first())->address); ?>

                                </label>
                                </br>
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;"> #Auto_and_Drive </span>
                            
                            </div>

                            <div class="text-center">
                                <a type="button"  data-id="<?php echo e($vehicle->id); ?>" class="code btn btn-sm" 
                                style="background-color:#2c5cb8; color:white">نسخ</a>
                            </div>
                           
                            <p id="informationCopied" hidden></p>
                        </div>
                        <div style=" padding:0.7rem" class="justify-content-center modal-footer">
                            <center>
                                <a type="button"  class="doneCopy btn btn-sm"  style="color:black">تم</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    
    <div class="col-lg-1 col-md-1 col-sm-1">
        
    </div>
    <h1 id='display' onClick='copyText(this)'>Text Sample</h1>
    
</div>

    <script>

        var ele = $("#informationCopied");
        var element = $("#urlCopied");

        $('.code').on('click', function() { 
            id = $(this).attr('data-id');
            copyText = document.querySelector('.element'+ id).textContent;
            console.log(copyText);
            
            ele.html(copyText);
            copyToClipboard(ele);
            swal("",  "تم نسخ المعلومات بنجاح");
            $('.doneCopy').on('click', function() { 
                
                $('#infoModal'+ id).modal('hide'); 
            });
        });
        
        function copyToClipboardURL(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }


        var urlV = window.location.href; 
        var arrV = urlV.split('vec-seller/')[0];    
        $(document).on('click', '.shareInformationLink', function() { 
            id = $(this).attr('data-id');
            newUrl = arrV + "vec-seller/information/"+ id;
            element.html(newUrl);
            copyToClipboardURL(element);
            swal("",  "تم نسخ الرابط بنجاح");
        });

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            $temp.focus();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/more/vehicles.blade.php ENDPATH**/ ?>