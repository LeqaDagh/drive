

<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-md-12">
        <center>
            <div class="col-md-5">
                <div id="errorAdding" class="alert alert-danger">يرجى اختيار مركبتين </div>
            </div>
        </center>
    </div>
</div>

<div class="row" > 
    <div class="col-md-12">
    <!--  -->
        <form class="form" method="POST" action="<?php echo e(route ('vehicle.compare')); ?>" novalidate>
            <?php echo csrf_field(); ?>
            <input type="text" name="id1" id="firstIdVechile" hidden>
            <input type="text" name="id2" id="secondIdVechile" hidden>
            <center>
                <div class="row">
                    <div class="main col-lg-6 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="row" >
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <?php $__empty_1 = true; $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="checkFirstCardCompare col-lg-6 col-md-6 col-sm-6">
                                                <a class="" id="checkFirstCardCompare" data-id="<?php echo e($vehicle->id); ?>">
                                                    <div id="checkFirstCard<?php echo e($vehicle->id); ?>" class="card p-1 mb-1 rounded">
                                                        <div class="card-body" style="padding: 0.1rem">
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <center>
                                                                        <?php if(!empty(\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first()) > 0): ?>
                                                                            <label>
                                                                            <?php echo e((\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first())->name); ?>, 
                                                                            </label>
                                                                        <?php endif; ?>
                                                                        <?php if(!empty(\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first()) > 0): ?>
                                                                            <label>
                                                                            <?php echo e((\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first())->name); ?>

                                                                            </label>
                                                                        <?php endif; ?>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="row">
                                                                    <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 ml-auto mr-auto">
                                                                    <?php if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0): ?>
                                                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                                                            src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name); ?>">
                                                                    <?php endif; ?>
                                                                    </div> 
                                                                    <div class="col-lg-5 col-md-5">
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                السعر  :  
                                                                                <span style=" font-size:10px"><?php echo e($vehicle->first_payment); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                القوة  :  
                                                                                <span style="font-size:10px"><?php echo e($vehicle->hp); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                    ناقل الحركة :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->gear == 'aut'): ?>
                                                                                أوتماتيك  
                                                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                                                يدوي    
                                                                                <?php else: ?> نصف أوتماتيك
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                    الوقود :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                                                بنزين  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'des'): ?>
                                                                                ديزل  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'binelec'): ?>
                                                                                بنزين / كهرباء  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'deselec'): ?>
                                                                                ديزل / كهرباء 
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'gaz'): ?>
                                                                                غاز    
                                                                                <?php else: ?> كهرباء
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                سنة الانتاج  :  
                                                                                <span style="font-size:10px"><?php echo e($vehicle->year_of_product); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                حالة المركبة :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->vehicle_status == 'new'): ?>
                                                                                    جديد  
                                                                                <?php else: ?>
                                                                                    مستعمل
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <strong>نأسف!</strong> لا يوجد نتائج
                                            </div>  
                                            <?php endif; ?> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>  
                    <div class="main col-lg-6 col-md-6 col-sm-6">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="row" >
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <?php $__empty_1 = true; $__currentLoopData = $vehicles2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="checkSecondCardCompare col-lg-6 col-md-6 col-sm-6">
                                                <a class="" id="checkSecondCardCompare" data-id="<?php echo e($vehicle->id); ?>">
                                                    <div id="checkSecondCard<?php echo e($vehicle->id); ?>" class="card p-1 mb-1 rounded">
                                                        <div class="card-body" style="padding: 0.1rem">
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <center>
                                                                        <?php if(!empty(\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first()) > 0): ?>
                                                                            <label>
                                                                            <?php echo e((\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first())->name); ?>, 
                                                                            </label>
                                                                        <?php endif; ?>
                                                                        <?php if(!empty(\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first()) > 0): ?>
                                                                            <label>
                                                                            <?php echo e((\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first())->name); ?>

                                                                            </label>
                                                                        <?php endif; ?>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="d-flex flex-row">
                                                                    <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 ml-auto mr-auto">
                                                                    <?php if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0): ?>
                                                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                                                            src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name); ?>">
                                                                    <?php endif; ?>
                                                                    </div> 
                                                                    <div class="col-lg-5 col-md-5">
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                السعر  :  
                                                                                <span style=" font-size:10px"><?php echo e($vehicle->first_payment); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                القوة  :  
                                                                                <span style="font-size:10px"><?php echo e($vehicle->hp); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:12px">
                                                                                    ناقل الحركة :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->gear == 'aut'): ?>
                                                                                أوتماتيك  
                                                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                                                يدوي    
                                                                                <?php else: ?> نصف أوتماتيك
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                    الوقود :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                                                بنزين  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'des'): ?>
                                                                                ديزل  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'binelec'): ?>
                                                                                بنزين / كهرباء  
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'deselec'): ?>
                                                                                ديزل / كهرباء 
                                                                                <?php else: ?> <?php if($vehicle->fuel == 'gaz'): ?>
                                                                                غاز    
                                                                                <?php else: ?> كهرباء
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                سنة الانتاج  :  
                                                                                <span style="font-size:10px"><?php echo e($vehicle->year_of_product); ?>  </span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="row">
                                                                            <label style="color: black; font-size:11px">
                                                                                حالة المركبة :  
                                                                                <span style="font-size:10px">
                                                                                <?php if($vehicle->vehicle_status == 'new'): ?>
                                                                                    جديد  
                                                                                <?php else: ?>
                                                                                    مستعمل
                                                                                <?php endif; ?>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <strong>نأسف!</strong> لا يوجد نتائج
                                            </div>  
                                            <?php endif; ?> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>   
                </div> 
            </center></br>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto">
                    <center>
                        <button type="submit" id="" class="btn btn-danger btn-block" >
                            <span>مقارنة </span>
                        </button>
                    </center>
                </div>
            </div>
        </form>
    </div>  
</div> 
<script>

    var id1 = 0, id2 = 0;
    $('.checkFirstCardCompare').on('click', 'a', function(e){  
        id1 = $(this).attr('data-id');
        $("#firstIdVechile").val(id1);
      //  alert($("#firstIdVechile").val());
    
        $("#checkFirstCard"+id1).toggleClass("bg-success text-white");
    });

    $('.checkSecondCardCompare').on('click', 'a', function(e){  
        id2 = $(this).attr('data-id');
        $("#secondIdVechile").val(id2);
        
        $("#checkSecondCard"+id2).toggleClass("bg-success text-white");
    });
    
    // compare vehicles.
        /*$(document).on('click', '#compareVehiclesLi', function(){ 
            id1 = $("#compareFirstVehicle").val();
            id2 = $("#compareSecondVehicle").val();
            console.log(id1);
            console.log(id2);
            var data =  $("#compareform").serialize();
            console.log(data);
            //window.open(arr, '_blank'); 
            $.ajax({
                url: '/vehicle/compare',
                method:"GET",
                dataType:"json",
                data: data,
                success: function(data) {
                    var url = window.location.origin;
                    var arr = url+ '/vehicle/compare';
                    location.href = arr;
                    console.log(data);
                }
            });
        });*/
        
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/compare/found.blade.php ENDPATH**/ ?>