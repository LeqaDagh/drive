

<?php $__env->startSection('content'); ?>
<div class="d-flex flex-row" >
    <div class="col-lg-12 col-md-12 col-sm-12" >
        <center>
        <div class="d-flex flex-row">
            <div class="card-body " style="padding: 0.3rem">
                <div  class="col-lg-7 col-md-7 col-sm-7">
                    <div id="carousel-example-generic-home" class="carousel slide" 
                        data-ride="carousel" >

                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php $__currentLoopData = $vehicleIds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicleId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                 
                                <li data-target="#carousel-example-generic-home" data-slide-to="<?php echo e($loop->index); ?>" 
                                    class="<?php echo e($loop->first ? 'active' : ''); ?>"></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php ($i = 0); ?>
                            <?php $__currentLoopData = $vehicleIds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicleId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <div style="width:110%; height: 220px; background-color:white" class="carousel-item item <?php echo e($loop->first ? ' active' : ''); ?>" >
                                    <a href="#">
                                    <img class="img-responsive" style="width:110%; height: 220px; object-fit: contain"
                                    src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicleId->id)->select('file_name')->first())->file_name); ?>">
                                    <?php ($i++); ?>
                                    </a>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic-home" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic-home" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <center>
                        <div class="col-md-6" id="" style="padding: 0.8rem">
                            <a href="<?php echo e(route('search.index')); ?>" class="btn btn-danger btn-block" 
                            style="cursor:pointer; color:white; height: 2.0rem" id="">
                                <i class="fa fa-search"></i><span >  اٍبدأ البحث</span>
                            </a>
                        </div> 
                    </center>
                </div>
            </div>
        </div></center>
    </div>
</div>

<div class="row" >
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="row">
            <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="<?php echo e(route('vec-seller.information', $vehicle->id)); ?>" class="text-muted">
                    <div class="card p-1 mb-1 bg-white rounded">
                        <div class="card-body" style="padding: 0.1rem">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <center>
                                            <label style="color:#595959; font-weight: bold;">
                                            <?php echo e($vehicle->name); ?>,  <?php echo e($vehicle->model_name); ?>

                                            <?php if($vehicle->stared == 'yes'): ?>
                                            <i class=" fa fa-star" style="color:#ffcc00"></i>
                                            <?php endif; ?>
                                            </label>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="d-flex flex-row">
                                    <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 col-sm-7 ml-auto mr-auto">
                                    <?php if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0): ?>
                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                            src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name); ?>">
                                    <?php endif; ?>
                                    </div> 
                                    <div class="col-lg-5 col-md-5 col-sm-5">
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                السعر  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->first_payment); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                القوة  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->hp); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                    ناقل الحركة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->gear == 'aut'): ?>
                                                أوتماتيك  
                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                يدوي    
                                                <?php else: ?> نصف أوتماتيك
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                    الوقود :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                بنزين  
                                                <?php else: ?> <?php if($vehicle->gear == 'des'): ?>
                                                ديزل  
                                                <?php else: ?> <?php if($vehicle->gear == 'binelec'): ?>
                                                بنزين / كهرباء  
                                                <?php else: ?> <?php if($vehicle->gear == 'deselec'): ?>
                                                ديزل / كهرباء 
                                                <?php else: ?> <?php if($vehicle->gear == 'gaz'): ?>
                                                غاز    
                                                <?php else: ?> كهرباء
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                سنة الانتاج  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->year_of_product); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                حالة المركبة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->vehicle_status == 'new'): ?>
                                                    جديد  
                                                <?php else: ?>
                                                    مستعمل
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>
<!--<div class="row" >
    <div class="col-lg-11 col-md-11 col-sm-11">
        <div class="row">
            <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="<?php echo e(route('vec-seller.information', $vehicle->id)); ?>" class="text-muted">
                    <div class="card p-1 mb-1 bg-white rounded">
                        <div class="card-body" style="padding: 0.1rem">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <center>
                                            <label>
                                            <?php echo e($vehicle->name); ?>,  <?php echo e($vehicle->model_name); ?>

                                            <?php if($vehicle->stared == 'yes'): ?>
                                            <i class=" fa fa-star" style="color:#ffcc00"></i>
                                            <?php endif; ?>
                                            </label>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 ml-auto mr-auto">
                                    <?php if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0): ?>
                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                            src="<?php echo e('/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name); ?>">
                                    <?php endif; ?>
                                    </div> 
                                    <div class="col-lg-5 col-md-5">
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                السعر  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->first_payment); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                القوة  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->hp); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                    ناقل الحركة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->gear == 'aut'): ?>
                                                أوتماتيك  
                                                <?php else: ?> <?php if($vehicle->gear == 'man'): ?>
                                                يدوي    
                                                <?php else: ?> نصف أوتماتيك
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                    الوقود :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->fuel == 'bin'): ?>
                                                بنزين  
                                                <?php else: ?> <?php if($vehicle->gear == 'des'): ?>
                                                ديزل  
                                                <?php else: ?> <?php if($vehicle->gear == 'binelec'): ?>
                                                بنزين / كهرباء  
                                                <?php else: ?> <?php if($vehicle->gear == 'deselec'): ?>
                                                ديزل / كهرباء 
                                                <?php else: ?> <?php if($vehicle->gear == 'gaz'): ?>
                                                غاز    
                                                <?php else: ?> كهرباء
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                سنة الانتاج  :  
                                                <span style="color: gray; font-size:10px"><?php echo e($vehicle->year_of_product); ?>  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                حالة المركبة :  
                                                <span style="color: gray; font-size:10px">
                                                <?php if($vehicle->vehicle_status == 'new'): ?>
                                                    جديد  
                                                <?php else: ?>
                                                    مستعمل
                                                <?php endif; ?>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>-->

    <script>

        $(window).scroll(function () {
            $("#btn_fixed").css({ top: '0px' });
            $("#top").css({ position: 'fixed'; bottom: '20px'; });
        });

        // Images Slider for information
        var slideVehicleIndex = 1;
        setInterval(showVehicleSlides, 1000);
       // setTimeout(showVehicleSlides(slideVehicleIndex), 500);
        //showVehicleSlides(slideVehicleIndex);

        function plusVehicleSlides(m) {
            showVehicleSlides(slideVehicleIndex += m);
        }

        function currentVehicleSlide(m) {
            showVehicleSlides(slideVehicleIndex = m);
        }

        function showVehicleSlides(m) {
            var i;
            var slides = document.getElementsByClassName("vehicleSlides");
            var dots = document.getElementsByClassName("dotVehicle");
            if (m > slides.length) {slideVehicleIndex = 1}    
            if (m < 1) {slideVehicleIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";  
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideVehicleIndex-1].style.display = "block";  
            dots[slideVehicleIndex-1].className += " active";
            setInterval(showVehicleSlides, 1000);
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/home.blade.php ENDPATH**/ ?>