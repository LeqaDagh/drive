


<?php $__env->startSection('content'); ?>
<div class="limiter " >
    <div class="container-login100">
        <div class="wrap-login100 d-flex flex-row" style="padding: 0.8rem" >
            <form class="login100-form validate-form" method="POST" action="<?php echo e(route('login')); ?>">
                    <?php echo csrf_field(); ?>
                <span class="login100-form-title p-b-26">
                    <img src="<?php echo e(url('/logo.png')); ?>" width="120">
                </span>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>"
                        type="email"  name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="البريد الالكتروني"></span>
                    <?php if($errors->has('email')): ?>
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" 
                    name="password" type="password" required>
                    <span class="focus-input100 text-center " data-placeholder="كلمة المرور"></span>
                    <?php if($errors->has('password')): ?>
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>


                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit"  class="login100-form-btn btn-round mb-1">
                            <?php echo e(__('تسجيل دخول')); ?>

                        </button>
                    </div>
                </div>

                <div class="text-center p-t-25">
                    <span class="txt1">
                    ليس لديك حساب! سجل الان
                    </span>
                </div>

                <div class="text-center d-flex flex-row p-t-10">
                    <div class="col-lg-6">
                        <a  style="background:#928787; border:0" href="<?php echo e(route('register')); ?>"  class="p-1 btn btn-dark btn-block">
                            <span style="font-size:13px">
                            <i class="fa fa-user-plus"> </i> 
                                حساب شخصي 
                            </span> 
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a style="background:#928787; border:0" href="<?php echo e(route('seller')); ?>" class="p-1 btn btn-dark btn-block">
                            <span style="font-size:13px;">
                                <i class="fa fa-car"></i> 
                                حساب معرض / شركة 
                            </span>
                        </a>
                    </div>
                </div>

                <div class="text-center">
                    <span class="text-center txt1">
                    AutoAndDrive.com 						
                    </span>
                </div>
               
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', [
    'class' => 'login-page'
], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/auth/login.blade.php ENDPATH**/ ?>