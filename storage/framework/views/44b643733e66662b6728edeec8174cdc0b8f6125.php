

<?php $__env->startSection('content'); ?>
<style>

/* [2] Transition property for smooth transformation of images */
.img-hover-zoom img {
  transition: transform .5s ease;
}

/* [3] Finally, transforming the image when container gets hovered */
.img-hover-zoom:hover img {
  transform: scale(1.5);
}
</style>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <center>
            <h5>
                <?php echo e($make[0]->name); ?> <?php echo e($make[0]->model_name); ?>

            </h5>
        </center>
    </div>
</div>

<div class="row">
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="row">
            <?php $__currentLoopData = $collection; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3">
                <div class="">
                    <div class="card-body ">
                        <div>
                            <div class="text-center">
                                <div class="img-hover-zoom allVehicleImages">
                                    <a class="imageFullScreen"  data-id="<?php echo e($data->id); ?>" >
                                        <img class="img-responsive" src="<?php echo e(strstr($data->thumb_path, '/storage/')); ?>" 
                                        style="width:100%;  height: 140px; object-fit: contain">
                                    
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- The Modal -->
            <div class="modal fade" id="imageFullScreenModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" style="cursor:pointer;">&times;</a>
                        </div>
                        <div class="modal-body">
                            <img class="modal-content" id="fullScreenImage" >
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>
<script>
    $(document).on('click', '.imageFullScreen', function() { 
        id = $(this).attr('data-id');
        var html="";
        $.ajax({
            url: '/media/image/'+ id,
            method:"GET",
            dataType:"json",
            success: function(data) {
                var image = "/storage/vehicles/"+ data.file_name;
                $("#fullScreenImage").attr("src", image);
                $('#imageFullScreenModal').modal('show');
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/pages/more/all-images.blade.php ENDPATH**/ ?>