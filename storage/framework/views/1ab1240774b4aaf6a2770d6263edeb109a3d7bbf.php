<title>انشاء حساب شخصي</title>
<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="<?php echo e(url('/css/util.css')); ?>">
<link rel="stylesheet" href="<?php echo e(url('/css/main.css')); ?>">

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100" style="padding: 0.8rem">
            <form id="validate-personal-register-form" method="POST" action="<?php echo e(route('register')); ?>">
                <?php echo csrf_field(); ?>
                <center>
                    <span class="text-center p-b-22">
                    <?php echo e(__('حساب شخصي')); ?>

                    </span>
                </center></br>

                <div class="wrap-input100  validate-input p-t-3">
                    <input name="name" type="text" class="input100 text-center" value="<?php echo e(old('name')); ?>"
                    data-parsley-required-message="الاسم مطلوب" required autofocus>

                    <span class="focus-input100 text-center " data-placeholder="الاسم"></span>
                    <?php if($errors->has('name')): ?>
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong><?php echo e($errors->first('name')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <input name="mobile" type="text" class="input100 text-center"value="<?php echo e(old('mobile')); ?>" 
                    data-parsley-required-message="الجوال مطلوب" data-parsley-minlength="9" 
                    data-parsley-minlength-message="أقل طول للرقم 9 خانات" 
                    data-parsley-maxlength="10" data-parsley-maxlength-message="أكبر طول للرقم 10 خانات" data-parsley-type="digits"
                    data-parsley-type-message="رقم الجوال فقط أرقام" required >

                    <span class="focus-input100 text-center " data-placeholder="رقم المحمول"></span>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <select class="input100 text-center" name="region_id" data-parsley-required-message="المنطقة مطلوب" style="border:none; text-align-last: center;" required>
                        <option selected disabled>المنطقة</option>
                        <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="address" type="text" class="input100 text-center" value="<?php echo e(old('address')); ?>" data-parsley-required-message="العنوان مطلوب" required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="العنوان"></span>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="email" type="email" class="input100 text-center" value="<?php echo e(old('email')); ?>" data-parsley-required-message="البريد الالكتروني مطلوب" required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="البريد الالكتروني"></span>
                    <?php if($errors->has('email')): ?>
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" 
                    type="password" data-parsley-required-message="كلمة المرور مطلوب"  required>
                    <span class="focus-input100 text-center " data-placeholder="كلمة المرور"></span>
                    <?php if($errors->has('password')): ?>
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" name="password_confirmation" data-parsley-required-message="تأكيد كلمة المرور مطلوب" 
                     type="password" required>
                    <span class="focus-input100 text-center " data-placeholder="تأكيد كلمة المرور"></span>
                    <?php if($errors->has('password_confirmation')): ?>
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" id="submit-register-form" class="login100-form-btn btn-round mb-1">
                            تسجيل
                        </button>
                    </div>
                </div>

                <div class="text-center ">
                    <span class="txt1">
                        لديك حساب!
                    </span>

                    <a class="" href="<?php echo e(route('login')); ?>">
                        تسجيل دخول
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', [
'class' => 'register-page',
], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Leqa\Drive\AutoAndDrive\Drive\resources\views/auth/register.blade.php ENDPATH**/ ?>