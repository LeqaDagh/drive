<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('welcome');
});

//Auth::routes();
Route::resource('/home', 'HomeController');
Route::get('/', 'HomeController@index')->name('home');
Auth::routes();

Route::resource('profile', 'ProfileController');
Route::get('/profile/images/{id}', ['as' => 'profile.images', 'uses' => 'ProfileController@companyImages']);
Route::resource('search', 'SearchController');
Route::post('/vehicle/search', ['as' => 'vehicle.search', 'uses' => 'SearchController@search']);
Route::resource('/compare', 'CompareController');
Route::post('/vehicle/compare1', 'CompareController@secondCompare');
Route::post('/vehicle/compare', ['as' => 'vehicle.compare', 'uses' => 'CompareController@compare']);
Route::resource('/stared', 'StaredController');
Route::resource('/contact', 'ContactController');
Route::resource('/used', 'UsingController');
Route::resource('/about-us', 'AboutUsController');
Route::resource('/vehicle', 'VehicleController');
Route::get('/vehicle/delete/{id}', ['as' => 'vehicle.delete', 'uses' => 'VehicleController@destroy']);
Route::resource('/all-vehicle', 'VehicleShopController');
Route::resource('/model', 'ModelsController');
//Route::get('/make/model/{id}', 'CompareController@secondCompare');
Route::resource('/make', 'MakeController');
Route::get('/media/{id}','MediaController@destroy');
Route::post('/media/images', ['as' => 'media.images', 'uses' => 'MediaController@addMoreImages']);
Route::get('/media/image/{id}', 'MediaController@getImage');
Route::resource('media', 'MediaController');
Route::resource('more', 'MoreController');
//Route::put('/vec-seller/edit/{id}', ['as' => 'vec-seller.update', 'uses' => 'VehicleSellerController@update']);
Route::resource('vec-seller', 'VehicleSellerController');
Route::get('/vec-seller/images/{id}', ['as' => 'vec-seller.images', 'uses' => 'VehicleSellerController@vehicleImages']);
Route::get('/vec-seller/information/{id}', ['as' => 'vec-seller.information', 'uses' => 'VehicleSellerController@vehicleInformation']);
Route::get('/vec-seller/all/{id}', ['as' => 'vec-seller.all', 'uses' => 'VehicleSellerController@vehicleAllImages']);
Route::post('/vec-seller/images/{id}', ['as' => 'vec-seller.images', 'uses' => 'VehicleSellerController@store']);
Route::get('/seller', 'SellerController@index')->name('seller');
Route::post('/seller', 'SellerController@create');
Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	//Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::patch('/profile/update/{user}', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::patch('/profile/password/{user}', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});
