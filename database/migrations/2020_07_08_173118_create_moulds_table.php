<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMouldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moulds', function (Blueprint $table) {
            $table->id();
            $table->integer('make_id')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('logo')->nullable();
            $table->integer('order');
            $table->string('review')->nullable();
            $table->integer('parent')->nullable();
            $table->string('visible')->default("no");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moulds');
    }
}
