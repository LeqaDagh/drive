@extends('layouts.app')

@section('content')
<style>
.form-group input[type='text'] {
    font-size:14px
}
.form-group input[type='number'] {
    font-size:14px
}
</style>
    @guest
        <div class="row" > 
            <div class="col-lg-12 col-md-12 col-sm-12" >
                <div class="">
                    <center>
                        <i class="fa fa-edit fa-4x"></i>
                    </center>
                </div>
                <div style="padding:0.8rem" class="">
                    <center>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <label for="">هل تريد اضافة مركبة؟</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <a style="color:blue" href="{{ route('login') }}">تسجيل دخول</a>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    @endguest

    @auth()
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2">
        
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div id="formVehicle" class="row" style="display:none"> 
                <div id="vehicleError">
                </div>
                
                <form class="form" method="POST" id="validate-vehicle-form" >
                @csrf
                    <div class="card ">
                        <div class="card-header ">
                            <center>
                                <h5>
                                اضافة مركبة
                                </h5>
                            </center>
                        </div>
                        <div class="card-body ">
                            
                            
                            @include('pages.vehicle.logos') 
                            

                            @include('pages.vehicle.body-type') 

                            <div class="row">
                                <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                    <div class="form-group">
                                        <input class="text-center form-control"
                                        placeholder="{{ __('عنوان اضافي') }}" type="text" name="extra_title"
                                        value="" >
                                        
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control" id="datepicker" name="year_of_product" placeholder="{{ __('سنة الانتاج') }}" 
                                        type="text" data-parsley-required-message=" سنة الانتاج مطلوب" required>
                                
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control" id="datepicker1" name="year_of_work"
                                        placeholder="{{ __('سنة الترخيص') }}" type="text" data-parsley-required-message=" سنة الترخيص مطلوب" required>
                                    </div> 
                                    <div class="form-group">
                                        <input class="text-center form-control" min="20000" data-parsley-min-message="يجب أن يكون السعر أعلى من 20000"
                                        name="price" placeholder="{{ __('السعر') }}" data-parsley-type="integer"
                                        data-parsley-type-message="يجب أن يكون السعر عددا صحيحا" type="number" data-parsley-required-message="السعر  مطلوب"required>
                                
                                    </div>
                                    @if($user->can_ignor_price == "yes" && $user->can_show_first_payment == "no" )
                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' طريقة عرض السعر') }}" type="text" disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price1" type="radio" 
                                                name="price_type" value="price_type_types_fp1" >
                                                <label for="price1" class="choice-button-text">عرض السعر</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price2" type="radio" name="price_type" 
                                                value="price_type_types_fp2" >
                                                <label for="price2" class="choice-button-text">عدم عرض السعر</label>
                                            </div> 
                                        </div>
                                    </div>
                                    @endif

                                    @if($user->can_ignor_price == "no" && $user->can_show_first_payment == "yes" )
                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' طريقة عرض السعر') }}" type="text" disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price1" type="radio" 
                                                name="price_type" value="price_type_types_fp1" >
                                                <label for="price1" class="choice-button-text">عرض السعر</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price2" type="radio" name="price_type" 
                                                value="price_type_types_fp2" >
                                                <label for="price2" class="choice-button-text">عدم عرض السعر</label>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control"
                                        placeholder="{{ __('الدفعة الأولى') }}" type="text" name="first_payment" 
                                        value=""  >
                                    </div>
                                    @endif

                                    @if($user->can_ignor_price == "yes" && $user->can_show_first_payment == "yes" )
                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' طريقة عرض السعر') }}" type="text" disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price1" type="radio" 
                                                name="price_type" value="price_type_types_fp1" >
                                                <label for="price1" class="choice-button-text">عرض السعر</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price2" type="radio" name="price_type" 
                                                value="price_type_types_fp2" >
                                                <label for="price2" class="choice-button-text">عدم عرض السعر</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="price2" type="radio" name="price_type" 
                                                value="price_type_types_fp2" >
                                                <label for="price2" class="choice-button-text"> عرض الدفعة الأولى</label>
                                            </div> 
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <input class="text-center form-control" data-parsley-required-message="القوة  مطلوب"
                                        placeholder="{{ __('قوة المحرك') }}" type="number" name="power" required>
                                    </div> 
                                    <div class="form-group">
                                        <input name="hp" class="text-center form-control" data-parsley-required-message=" عدد الأحصنة مطلوب"
                                        placeholder="{{ __(' عدد الأحصنة') }}" type="number"  data-parsley-range="[60, 1000]"
                                        data-parsley-range-message="عدد الأحصنة يجب أن تكون ما بين 60 و 1000" required>
                                    </div>    
                                    <div class="form-group">
                                        <input name="mileage" class="text-center form-control" data-parsley-required-message="المسافة المقطوعة  مطلوب"
                                        placeholder="{{ __('المسافة المقطوعة') }}" type="number" data-parsley-range="[0, 1000000]"
                                        data-parsley-range-message="المسافة المقطوعة يجب أن تكون ما بين 0 و مليون" required >
                                    </div>   

                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __('ناقل الحركة') }}" type="text" disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="gear1" type="radio" name="gear" value="aut" data-parsley-required-message="ناقل الحركة  مطلوب"
                                        required>
                                                <label for="gear1" class="choice-button-text">أوتماتيك</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="gear2" type="radio" name="gear" value="man" >
                                                <label for="gear2" class="choice-button-text">يدوي</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen" id="gear3" type="radio" name="gear" value="sau" >
                                                <label for="gear3" class="choice-button-text">نصف أوتماتيك</label>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __('الوقود') }}" type="text" disabled>
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="fuel1" type="radio" name="fuel" value="bin" data-parsley-required-message="الوقود  مطلوب"
                                                required>
                                                <label for="fuel1" class="choice-button-text">بنزين</label>
                                                
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="fuel2" type="radio" name="fuel" value="des" >
                                                <label for="fuel2" class="choice-button-text">ديزل</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="fuel3" type="radio" name="fuel" value="binelec" >
                                                <label for="fuel3" class="choice-button-text">بنزين / كهرباء</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="fuel4" type="radio" name="fuel" value="deselec">
                                                <label for="fuel4" class="choice-button-text">ديزل / كهرباء</label>
                                        
                                            </div> 
                                        </div>
                                        
                                    </div>
                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="fuel5" type="radio" name="fuel" value="gaz" >
                                                <label for="fuel5" class="choice-button-text">غاز</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="fuel6" type="radio" name="fuel" value="elec" >
                                                <label for="fuel6" class="choice-button-text">كهرباء</label>
                                            
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                    

                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' نظام الدفع') }}" type="text"  disabled>
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="drivetrains1" type="radio" name="drivetrain_system" value="fbf" data-parsley-required-message="نظام الدفع مطلوب"
                                        required >
                                                <label for="drivetrains1" class="choice-button-text">4X4</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="drivetrains2" type="radio" name="drivetrain_system" value="tbf" >
                                                <label for="drivetrains2" class="choice-button-text">2X4</label>
                                            
                                            </div> 
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' نوع الدفع') }}" type="text"  disabled>
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="drivetrain_type1" type="radio" name="drivetrain_type" data-parsley-required-message="نوع الدفع مطلوب"
                                            value="fro" required>
                                                <label for="drivetrain_type1" class="choice-button-text">أمامي</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                            <input class="radiochoosen"  id="drivetrain_type2" type="radio" name="drivetrain_type" value="bak" >
                                                <label for="drivetrain_type2" class="choice-button-text">خلفي</label>
                                            
                                            </div> 
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="input-group ">
                                            <input class="form-control" type="text" id="inputGroup" 
                                            placeholder="{{ __(' طريفة الدفعات') }}"   disabled>
                                            <input class="input-group-addon" type="checkbox" 
                                            id="price_type_check" > 
                                        
                                        </div>
                                    </div> 

                                    

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="price_type_checkbox  radiochoosen"  id="priceType1" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay1" data-parsley-required-message="طريقة الدفعات مطلوب"
                                            required>
                                                <label for="priceType1" class="choice-button-text">نقدا</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="price_type_checkbox radiochoosen"  id="priceType2" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay2" >
                                                <label for="priceType2" class="choice-button-text">عن طريق البنك</label>
                                            
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="price_type_checkbox radiochoosen"  id="priceType3" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay3" >
                                                <label for="priceType3" class="choice-button-text">تقسيط مباشر</label>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="price_type_checkbox radiochoosen"  id="priceType4" type="checkbox"
                                                name="payment_method[]" value="payment_method_types_pay4" >
                                                <label for="priceType4" class="choice-button-text">دفعة أولى + شيكات شخصية</label>
                                            
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' عدد المقاعد') }}" type="text"  disabled>
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="seat1" type="radio" name="num_of_seats" value="s1" data-parsley-required-message="عدد المقاعد مطلوب"
                                            required>
                                                <label for="seat1" class="choice-button-text">2+1</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="seat2" type="radio" name="num_of_seats" value="s2" >
                                                <label for="seat2" class="choice-button-text">3+1</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="seat3" type="radio" name="num_of_seats" value="s3" >
                                                <label for="seat3" class="choice-button-text">4+1</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="seat4" type="radio" name="num_of_seats" value="s4" >
                                                <label for="seat4" class="choice-button-text">5+1</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="seat5" type="radio" name="num_of_seats" value="s5" >
                                                <label for="seat5" class="choice-button-text">6+1</label>
                                            </div> 
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <input class="form-control" name=""
                                        placeholder="{{ __(' حالة المركبة') }}" type="text"  disabled>
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                    
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="radio1" type="radio" name="vehicle_status" value="new" data-parsley-required-message=" حالة المركبة مطلوب"
                                            required>
                                                <label for="radio1"class="choice-button-text">جديد</label>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <input class="radiochoosen"  id="radio2" type="radio" name="vehicle_status" value="usd" >
                                                <label for="radio2" class="choice-button-text">مستعمل</label>
                                            </div> 
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-lg-6 col-sm-4 ml-auto mr-auto">

                                            <div class="form-group">
                                                <label>لون السيارة </label>
                                                <input class="form-control" name="body_color" id="body_color" style="display:none; border: 0 !important;
                                                box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                                type="text" data-parsley-required-message="  لون السيارة مطلوب"
                                                disabled required>
                                            </div> 
                                            
                                            <div class="form-group">
                                                
                                                <a class="btn btn-light btn-block" data-toggle="modal" 
                                                    data-target="#car-color-modal" id="color-car"
                                                    style="border-radius: 30px;">
                                                    <i id="cars-color" class="fa fa-check-circle-o">
                                                        <span id="color-car-text" > لم يحدد</span>
                                                    </i> 
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 col-sm-4 ml-auto mr-auto">
                                            <div class="form-group">
                                                <label>لون الفرش </label>
                                                <input class="form-control" id="interior_color" name="interior_color" style="display:none; border: 0 !important;
                                                box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                                type="text" data-parsley-required-message="  لون الفرش مطلوب"
                                                disabled required>

                                            </div> 
                                            <div class="form-group">
                                                <a class=" btn btn-light btn-block" data-toggle="modal" 
                                                data-target="#interior-color-modal" id="color-interior"
                                                style="border-radius: 30px;" >
                                                    <i id="interior-color" class="fa fa-check-circle-o">
                                                        <span class="color-interior-text"> لم يحدد</span>
                                                    </i> 
                                                </a>
                                            </div> 
                                        </div>

                                    </div>

                                </div>
                                <div class="col-lg-4 col-md-6 ml-auto mr-auto">

                                        
                                </div>
                            </div>

                            

                            <div class="row">
                                <div class="col-lg-12 ml-auto mr-auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            
                                            <a class="toggle btn btn-secondary btn-block" style="cursor:pointer">
                                                <span class="toggle-text"> خيارات أخرى</span><i id="toggleicon" class="fa fa-angle-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div></br>

                            @include('pages.vehicle.more') 

                        <div class="row">
                            <div class="col-lg-6 ml-auto mr-auto">
                                <center>
                                    <button type="submit" id="submit-vehicle-form" class="btn btn-danger btn-block" >
                                            <span>اِضافة </span>
                                        </button>
                                
                                </center>
                            </div>
                        </div></br>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
    
        </div>
     </div>
        
    
    @include('pages.vehicle.images') 
    @include('pages.vehicle.modals') 
    @endauth
    
@endsection