</br>
</br>
<div id="imagesForVehicle" class="row" style="display:none"> 
    <div class="col-md-6 ml-auto mr-auto"><center>
        <form class="form" method="POST" action="{{ route ('media.store') }}" enctype="multipart/form-data" id="upload-images-vehicle-form" >
        @csrf
            <div class="card ">
                <div class="card-header ">
                    <center>
                        <h6>
                        التحكم بالصور
                        </h6>
                    </center>
                </div>

                <div class="card-body ">
                    <input type="file" name="photos[]" id="ImagesVehicleUpload" style="display: none"
                        class="text-center form-control" multiple="multiple">

                    <input type="text" id="vehicleId" name="vehcile_id" hidden >
                    
                    <div class="row">
                        <div class="col-lg-8 col-md-8  col-sm-8  ml-auto mr-auto">
                            <div id="myImg" > 
                            </div>
                        </div>      
                    </div> 

                    <div class="container"> 
                        <div class="text-center"> 
                            <button class="btn btn-primary add-multiple-images" type="button" >اختر الصور
                            </button>
                            <button id="upload-multiple-images" class="btn btn-danger upload-multiple-images" type="submit" > رفع
                            </button>
                        </div> 
                    </div>
                </div>

            </div>
        </form>
    </div></center>
</div>


