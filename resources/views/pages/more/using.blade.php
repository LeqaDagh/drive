@extends('layouts.app')

@section('content')
    <div class="row" style=""> 
        <div class="col-lg-12 col-md-12 col-sm-12" >
            <div class="">
                <center>
                    <h5>
                    اٍتفاقية الاٍستخدام
                    </h5>
                </center>
            </div></br>
            <div class="row" >
                <center>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <div class="card text-left" >
                            <div class="card-body ">
                                <div >
                                    {!! html_entity_decode($used[0]->settings_value) !!}                            
                                </div>
                            </div>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </div>
@endsection