@extends('layouts.app')

@section('content')
<style>
.form-group input[type='text'] {
    font-size:14px
}
.form-group input[type='number'] {
    font-size:14px
}
</style>
    <div  class="row"> 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <form class="form" id="validate-vehicle-edit-form" >
                <div class="card ">
                    <div class="card-header ">
                        <center>
                            <h5>
                            تعديل مركبة
                            </h5>
                        </center>
                    </div>
                    <div class="card-body ">
                        <input type="text" id="vehicleId" value="{{ $vehicle[0]->id }}" hidden>
                        @include('pages.more.logos-edit') 

                        @include('pages.vehicle.logos')

                        @include('pages.more.body-type-edit') 

                        <div class="row">
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                <div class="form-group">
                                    <input class="text-center form-control"
                                    placeholder="{{ __('عنوان اضافي') }}" type="text" name="extra_title"
                                    value="{{ $vehicle[0]->extra_title }}" >
                                    
                                </div>
                                <div class="form-group">
                                    <input class="text-center form-control" id="datepicker" name="year_of_product"
                                     placeholder="{{ __('سنة الانتاج') }}" value="{{ $vehicle[0]->year_of_product }}"
                                    type="text" data-parsley-required-message=" سنة الانتاج مطلوب" required>
                            
                                </div>
                                <div class="form-group">
                                    <input class="text-center form-control" id="datepicker1" name="year_of_work"
                                    value="{{ $vehicle[0]->year_of_work }}"
                                    placeholder="{{ __('سنة الترخيص') }}" type="text" data-parsley-required-message=" سنة الترخيص مطلوب" required>
                                </div> 
                                <div class="form-group">
                                    <input class="text-center form-control" min="20000" data-parsley-min-message="يجب أن يكون السعر أعلى من 20000"
                                    name="price" placeholder="{{ __('السعر') }}" data-parsley-type="integer"
                                    value="{{ $vehicle[0]->price }}"
                                    data-parsley-type-message="يجب أن يكون السعر عددا صحيحا" type="number" data-parsley-required-message="السعر  مطلوب"required>
                            
                                </div>
                                <div class="form-group">
                                    <input class="text-center form-control"
                                    placeholder="{{ __('الدفعة الأولى') }}" type="text" name="first_payment" 
                                    value="{{ $vehicle[0]->first_payment }}"  >
                                </div>


                                <div class="form-group">
                                    <input class="text-center form-control" data-parsley-required-message="القوة  مطلوب"
                                    placeholder="{{ __('قوة المحرك') }}" type="number" name="power"
                                    value="{{ $vehicle[0]->power }}"  required>
                                </div> 
                                <div class="form-group">
                                    <input name="hp" class="text-center form-control" data-parsley-required-message=" عدد الأحصنة مطلوب"
                                    placeholder="{{ __(' عدد الأحصنة') }}" type="number"  data-parsley-range="[60, 1000]"
                                     data-parsley-range-message="عدد الأحصنة يجب أن تكون ما بين 60 و 1000" 
                                     value="{{ $vehicle[0]->hp }}" required>
                                </div>    
                                <div class="form-group">
                                    <input name="mileage" class="text-center form-control" data-parsley-required-message="المسافة المقطوعة  مطلوب"
                                    placeholder="{{ __('المسافة المقطوعة') }}" type="number" data-parsley-range="[0, 1000000]"
                                    value="{{ $vehicle[0]->mileage }}" 
                                    data-parsley-range-message="المسافة المقطوعة يجب أن تكون ما بين 0 و مليون" required >
                                </div>   

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __('ناقل الحركة') }}" type="text" disabled >
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear1" type="radio" name="gear" value="aut" data-parsley-required-message="ناقل الحركة  مطلوب"
                                        required {{ $vehicle[0]->gear == 'aut' ? 'checked' : '' }}>
                                            <label for="gear1" class="choice-button-text">أوتماتيك</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear2" type="radio" name="gear" value="man" 
                                            {{ $vehicle[0]->gear == 'man' ? 'checked' : '' }}>
                                            <label for="gear2" class="choice-button-text">يدوي</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen" id="gear3" type="radio" name="gear" value="sau"
                                            {{ $vehicle[0]->gear == 'sau' ? 'checked' : '' }} >
                                            <label for="gear3" class="choice-button-text">نصف أوتماتيك</label>
                                        </div> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __('الوقود') }}" type="text" disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="fuel1" type="radio" name="fuel" value="bin" data-parsley-required-message="الوقود  مطلوب"
                                            required {{ $vehicle[0]->fuel == 'bin' ? 'checked' : '' }}>
                                            <label for="fuel1" class="choice-button-text">بنزين</label>
                                            
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel2" type="radio" name="fuel" value="des" 
                                        {{ $vehicle[0]->fuel == 'des' ? 'checked' : '' }}>
                                            <label for="fuel2" class="choice-button-text">ديزل</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel3" type="radio" name="fuel" value="binelec"
                                        {{ $vehicle[0]->fuel == 'binelec' ? 'checked' : '' }} >
                                            <label for="fuel3" class="choice-button-text">بنزين / كهرباء</label>
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel4" type="radio" name="fuel" value="deselec"
                                        {{ $vehicle[0]->fuel == 'deselec' ? 'checked' : '' }}>
                                            <label for="fuel4" class="choice-button-text">ديزل / كهرباء</label>
                                       
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel5" type="radio" name="fuel" value="gaz"
                                        {{ $vehicle[0]->fuel == 'gaz' ? 'checked' : '' }} >
                                            <label for="fuel5" class="choice-button-text">غاز</label>
                                          
                                        </div> 
                                    </div>
                                    
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="fuel6" type="radio" name="fuel" value="elec" 
                                        {{ $vehicle[0]->fuel == 'elec' ? 'checked' : '' }}>
                                            <label for="fuel6" class="choice-button-text">كهرباء</label>
                                        
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __(' نظام الدفع') }}" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrains1" type="radio" name="drivetrain_system" value="fbf" data-parsley-required-message="نظام الدفع مطلوب"
                                     required {{ $vehicle[0]->drivetrain_system == 'fbf' ? 'checked' : '' }}>
                                            <label for="drivetrains1" class="choice-button-text">4X4</label>
                                         
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrains2" type="radio" name="drivetrain_system"
                                         value="tbf" {{ $vehicle[0]->drivetrain_system == 'tbf' ? 'checked' : '' }}>
                                            <label for="drivetrains2" class="choice-button-text">2X4</label>
                                          
                                        </div> 
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __(' نوع الدفع') }}" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="drivetrain_type1" type="radio" name="drivetrain_type" data-parsley-required-message="نوع الدفع مطلوب"
                                        value="fro" required {{ $vehicle[0]->drivetrain_type == 'fro' ? 'checked' : '' }}>
                                            <label for="drivetrain_type1" class="choice-button-text">أمامي</label>
                                         
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                        <input class="radiochoosen"  id="drivetrain_type2" type="radio" name="drivetrain_type"
                                         value="bak" {{ $vehicle[0]->drivetrain_type == 'bak' ? 'checked' : '' }} >
                                            <label for="drivetrain_type2" class="choice-button-text">خلفي</label>
                                        
                                        </div> 
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="input-group ">
                                        <input class="form-control" type="text" id="inputGroup" 
                                        placeholder="{{ __(' طريفة الدفعات') }}"   disabled>
                                        <input class="input-group-addon" type="checkbox" 
                                        id="price_type_check" >
                                       
                                    </div>
                                </div> 

                            
                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            @if (strlen(strpos($payment, 'payment_method_types_pay1')))
                                                <input class="price_type_checkbox radiochoosen" id="priceType1" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay1" checked>
                                                <label for="priceType1" class="choice-button-text">نقدا</label>
                                            @else
                                                <input class="price_type_checkbox radiochoosen" id="priceType1" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay1" >
                                                <label for="priceType1" class="choice-button-text">نقدا</label>
                                            @endif
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            @if (strlen(strpos($payment, 'payment_method_types_pay2')))
                                                <input class="price_type_checkbox radiochoosen" id="priceType2" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay2" checked>
                                                <label for="priceType2" class="choice-button-text">عن طريق البنك</label>
                                            @else
                                                <input class="price_type_checkbox radiochoosen" id="priceType2" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay2" >
                                                <label for="priceType2" class="choice-button-text">عن طريق البنك</label>
                                            @endif
                                          
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            @if (strlen(strpos($payment, 'payment_method_types_pay3')))
                                                <input class="price_type_checkbox radiochoosen" id="priceType3" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay3" checked>
                                                <label for="priceType3" class="choice-button-text">تقسيط مباشر</label>
                                            @else
                                                <input class="price_type_checkbox radiochoosen" id="priceType3" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay3" >
                                                <label for="priceType3" class="choice-button-text">تقسيط مباشر</label>
                                            @endif
                                        </div> 
                                    </div>
                                </div>

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            @if (strlen(strpos($payment, 'payment_method_types_pay4')))
                                                <input class="price_type_checkbox radiochoosen" id="priceType4" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay4" checked>
                                                <label for="priceType4" class="choice-button-text">دفعة أولى + شيكات شخصية</label>
                                            @else
                                                <input class="price_type_checkbox radiochoosen" id="priceType4" type="checkbox" 
                                                name="payment_method[]" value="payment_method_types_pay4" >
                                                <label for="priceType4" class="choice-button-text">دفعة أولى + شيكات شخصية</label>
                                            @endif
                                        </div> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __(' عدد المقاعد') }}" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat1" type="radio" name="num_of_seats" value="s1" data-parsley-required-message="عدد المقاعد مطلوب"
                                        required {{ $vehicle[0]->num_of_seats == 's1' ? 'checked' : '' }}>
                                            <label for="seat1" class="choice-button-text">2+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat2" type="radio" name="num_of_seats" value="s2"
                                            {{ $vehicle[0]->num_of_seats == 's2' ? 'checked' : '' }} >
                                            <label for="seat2" class="choice-button-text">3+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat3" type="radio" name="num_of_seats" value="s3"
                                            {{ $vehicle[0]->num_of_seats == 's3' ? 'checked' : '' }}>
                                            <label for="seat3" class="choice-button-text">4+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat4" type="radio" name="num_of_seats" value="s4"
                                            {{ $vehicle[0]->num_of_seats == 's4' ? 'checked' : '' }} >
                                            <label for="seat4" class="choice-button-text">5+1</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="seat5" type="radio" name="num_of_seats" value="s5"
                                            {{ $vehicle[0]->num_of_seats == 's5' ? 'checked' : '' }} >
                                            <label for="seat5" class="choice-button-text">6+1</label>
                                        </div> 
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input class="form-control" name=""
                                    placeholder="{{ __(' حالة المركبة') }}" type="text"  disabled>
                                    
                                </div> 

                                <div class="d-flex flex-row">
                                
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="radio1" type="radio" name="vehicle_status" value="new" data-parsley-required-message=" حالة المركبة مطلوب"
                                        required {{ $vehicle[0]->vehicle_status == 'new' ? 'checked' : '' }}>
                                            <label for="radio1"class="choice-button-text">جديد</label>
                                        </div> 
                                    </div>
                                    <div class="p-1">
                                        <div class="form-group">
                                            <input class="radiochoosen"  id="radio2" type="radio" name="vehicle_status" value="usd"
                                            {{ $vehicle[0]->vehicle_status == 'usd' ? 'checked' : '' }} >
                                            <label for="radio2" class="choice-button-text">مستعمل</label>
                                        </div> 
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-lg-6 col-sm-4 ml-auto mr-auto">

                                        <div class="form-group">
                                            <label>لون السيارة </label>
                                            <input class="form-control" name="body_color" id="body_color-edit" style="display:none; border: 0 !important;
                                             box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                            type="text" value="{{$vehicle[0]->body_color}}"
                                            data-parsley-required-message="  لون السيارة مطلوب" disabled required>
                                        </div> 
                                        
                                        <div class="form-group">
                                            
                                            <a class="btn btn-light btn-block" data-toggle="modal" 
                                                data-target="#car-color-modal-edit" id="color-car-edit"
                                                style="border-radius: 30px; background-color: #{{$bodyColor}}">
                                                @if($bodyColor == 'ffffff')
                                                    <i id="cars-color-edit" style="color:black" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أبيض</span>
                                                    </i>
                                                @else @if($bodyColor == 'c7c1bd' )
                                                    <i id="cars-color-edit" style="color:black" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > فضي</span>
                                                    </i> 
                                                @else @if($bodyColor == 'f0e9e9')
                                                    <i id="cars-color-edit" style="color:black" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أبيض كريمي</span>
                                                    </i> 
                                                @else @if($bodyColor == '000000' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أسود</span>
                                                    </i> 
                                                @else @if($bodyColor == '0bde16' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أخضر</span>
                                                    </i> 
                                                @else @if($bodyColor == '0e103d' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > كحلي</span>
                                                    </i> 
                                                @else @if($bodyColor == '120a0a' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أسود مطفي</span>
                                                    </i> 
                                                @else @if($bodyColor == '15b367' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أزرق مخضر</span>
                                                    </i> 
                                                @else @if($bodyColor == '1e14de' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > نيلي</span>
                                                    </i> 
                                                @else @if($bodyColor == '1e9dba' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أزرق فاتح</span>
                                                    </i> 
                                                @else @if($bodyColor == '215769' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أزرق غامق</span>
                                                    </i> 
                                                @else @if($bodyColor == '24070d' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > بنفسجي</span>
                                                    </i> 
                                                @else @if($bodyColor == '2a4221' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > زيتي</span>
                                                    </i> 
                                                @else @if($bodyColor == '362517' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > بني غامق</span>
                                                    </i> 
                                                @else @if($bodyColor == '386352' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أخضر غامق</span>
                                                    </i> 
                                                @else @if($bodyColor == '544c4a' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" >رمادي</span>
                                                    </i> 
                                                @else @if($bodyColor == '6b6560' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > سكني</span>
                                                    </i> 
                                                @else @if($bodyColor == '700d0d' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أحمر قاني</span>
                                                    </i> 
                                                @else @if($bodyColor == '70431e' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > بني</span>
                                                    </i> 
                                                @else @if($bodyColor == '8fe813' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > فسفوري</span>
                                                    </i> 
                                                @else @if($bodyColor == 'ba0909' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > خمري</span>
                                                    </i> 
                                                @else @if($bodyColor == 'c48f60' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > بني برونزي</span>
                                                    </i> 
                                                @else @if($bodyColor == 'c7c1bd' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > فضي</span>
                                                    </i> 
                                                @else @if($bodyColor == 'de6b18' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > برتقالي</span>
                                                    </i> 
                                                @else @if($bodyColor == 'fcba03' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أصفر</span>
                                                    </i> 
                                                @else @if($bodyColor == 'ff0000' )
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > أحمر</span>
                                                    </i> 
                                                @else 
                                                    <i id="cars-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-car-text-edit" > زهري</span>
                                                    </i> 
                                                @endif 
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                            </a>
                                        </div> 
                                    </div>
                                    <div class="col-lg-6 col-sm-4 ml-auto mr-auto">
                                        <div class="form-group">
                                            <label>لون الفرش </label>
                                            <input class="form-control" id="interior_color-edit" name="interior_color" style="display:none; border: 0 !important;
                                             box-shadow: none !important;background-color: white !important;cursor: default !important;"
                                            type="text" value="{{$vehicle[0]->interior_color}}"
                                            data-parsley-required-message="  لون الفرش مطلوب"  disabled required>

                                        </div> 
                                        <div class="form-group">
                                            <a class=" btn btn-light btn-block" data-toggle="modal" 
                                            data-target="#interior-color-modal-edit" id="color-interior-edit"
                                            style="border-radius: 30px; background-color: #{{$interiorColor}}" >
                                                @if($interiorColor == 'fbffd9' )
                                                    <i id="interior-color-edit" style="color:black" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" > بيج</span>
                                                    </i>  
                                                @else @if($interiorColor == 'fcfcfc' )
                                                    <i id="interior-color-edit" style="color:black" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" > أبيض</span>
                                                    </i>  
                                                @else @if($interiorColor == '000000' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" > أسود</span>
                                                    </i>  
                                                @else @if($interiorColor == '5e3b18' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" >  بني</span>
                                                    </i>  
                                                @else @if($interiorColor == '751010' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" >  خمري</span>
                                                    </i>  
                                                @else @if($interiorColor == '755196' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" >  بنفسجي</span>
                                                    </i>  
                                                @else @if($interiorColor == 'b5b3b3' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" >  سكني</span>
                                                    </i>  
                                                @else @if($interiorColor == 'c40a0a' )
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" > أحمر </span>
                                                    </i> 
                                                @else 
                                                    <i id="interior-color-edit" style="color:white" class="fa fa-check-circle-o">
                                                        <span id="color-interior-text-edit" > عسلي </span>
                                                    </i> 
                                                @endif 
                                                @endif
                                                @endif
                                                @endif
                                                @endif 
                                                @endif
                                                @endif
                                                @endif
                                            </a>
                                        </div> 
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-4 col-md-6 ml-auto mr-auto">

                                    
                            </div>
                        </div>

                        

                        <div class="row">
                            <div class="col-lg-12 ml-auto mr-auto">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <a class="toggle-edit btn btn-secondary btn-block" style="cursor:pointer">
                                            <span class="toggle-text"> خيارات أخرى</span><i id="toggleicon-edit" class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div></br>

                        @include('pages.more.more-edit') 

                    <div class="row">
                        <div class="col-lg-6 ml-auto mr-auto">
                            <center>
                                <button type="button" id="submit-vehicle-form-button" class="btn btn-danger btn-block" >
                                        <span>تعديل </span>
                                    </button>
                            
                            </center>
                        </div>
                    </div></br>
                </form>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
    @include('pages.more.modal-edit') 

    <script>
        // Update Vehicle form
        // Remove choosen logo & model in edit vehicle
        $("#bodyTypeLogos").css("display", "none");
        $("#viewAllCompany").css("display", "none");
        $("#bodyTypeLogosEdit").css("display", "none");
        $("#viewAllCompanyEdit").css("display", "none");
        $("#viewCompanyTitle").css("display", "none");
        $("#breakAllCompany").css("display", "none");
        $('.remove-image-logo-edit').on('click', function() {
            $("#bodyTypeLogoChoosenEdit").css("display", "none");
            $("#bodyTypeLogosEdit").fadeIn(200);
            $("#viewAllCompanyEdit").fadeIn(200);
            $("#viewCompanyTitle").css("display", "none");
            $("#breakAllCompany").css("display", "none");
        });


        var $bodyEdit = $("#bodyEdit").val();
        var $makeIdEdit = $("#makeEdit").val();
        var $modelIdEdit = $("#modelEdit").val();
        var carColorCodeValEdit = $("#bodyColorEdit").val();
        var interiorColorCodeEdit = $("#interiorColorEdit").val();
        var $companyName = "" , companyLogo = "", modelName = ""; 

        $(document).ready(function(){
            
            var carcolor="", carColorCode = "";
            $('.ul-car-color-edit').on('click', 'li', function(e){
                console.log(carColorCodeValEdit);
                var current = this;     
                carColorCode = $(this).attr("value");
                carColorCodeValEdit = carColorCode.substr(1);
                carColorCodeValEdit = 'c' + carColorCodeValEdit;
                console.log(carColorCodeValEdit);
                carcolor = $(this).attr('data-id');
                if(carColorCode.length > 0) {
                    $(this).find('.checkafterEdit').toggleClass('fa fa-check');
                }

                $("#chooseCarColorEdit").click(function () {  
                    $("#car-color-modal-edit").modal('hide');
                    $("#color-car-edit").css('background-color',carColorCode);
                    $("#color-car-edit").css('color','white');
                    $("#cars-color-edit span").text(carcolor);    
                    $("#body_color-edit").val(carColorCode);  
                    $(".checkafterEdit").removeClass('fa fa-check');
                });
                
            });

            var color="";
            $('.ul-interior-color-edit').on('click', 'li', function(e){  
                var current = this;     
                interiorColor = $(this).attr("value");
                interiorColorCodeEdit = interiorColor.substr(1);
                interiorColorCodeEdit = 'c' + interiorColorCodeEdit;
                color = $(this).attr('data-id');
                if(color.length > 0) {
                    $(this).find('.checkafter').toggleClass('fa fa-check');
                }
            });

            $(document).on('click', '.chooseinteriorColor', function(){ 
                $("#interior-color-modal-edit").modal('hide');
                $("#color-interior-edit").css('background-color',interiorColor);
                $("#color-interior-edit").css('color','white');
                $("#interior-color-edit span").text(color);    
                $("#interior_color-edit").val(interiorColorCodeEdit); 
                $(".checkafter").removeClass('fa fa-check');
            });

            // Body type value
            $('.bodyType input').on('change', function() {
                $bodyTypeVal = $('input[name=bodyEdit]:checked').val();
                $("#bodyEdit").val($bodyTypeVal);
                $bodyEdit = $("#bodyEdit").val();
            });
            
            
            // Type & Modal values for the car
            $(document).on('click', '.modelsCarsEdit', function(){ 
                id = $(this).attr('data-id');
                $(".company-model-body-edit").html("");
                var html="";
                $makeIdEdit = id;
                $.ajax({
                    url: '/make/'+ id,
                    method:"GET",
                    dataType:"json",
                    
                    success: function(data) {
                        $companyId = data[0].id;
                        $companyName = data[0].name;
                        companyLogo = data[0].logo;
                    }
                });

                $.ajax({
                    url: '/vehicle/'+ id,
                    method:"GET",
                    dataType:"json",
                    
                    success: function(data) {
                        var datalength = data.length;
                        html += '<table class="table table-bordered" >';
                        $(".company-model-title-edit").html($companyName);
                        for(var count=0; count < datalength; count++) {
                            
                            var length = data[count].length;
                            if(length == 1) {
                                var c = count;
                                html += '<tr>';
                                html += '<td><input class="radioModelName" data-id="'+ data[c][0].name +'" value="'+ data[c][0].id +'" type="radio" name="optradio">  ' + '   ' + data[c][0].name +'</td>';  
                                html += '</tr>';
                            } else {
                                var cnt = count;
                                
                                html += '<tr class="rr"><td><a class="testttt" style="cursor:pointer">'+ data[cnt][0].name  +'</a></td></tr>';
                                
                                for(var i=1; i < length; i++) {
                                   
                                    html += '<tr class="moreModels11" style="display:none;">';
                                    html += '<td><input class="radioModelName" data-id="'+ data[cnt][i].name +'" value="'+ data[cnt][i].id +'" type="radio" name="optradio">  ' + '   ' + data[cnt][i].name +'</td>';  
                                    html += '</tr>';
                                }
                            }
                        }
                        
                       
                        html += '</table>'; 
                        $('.company-model-body-edit').append(html); 
                        $('#company-model-modal-edit').modal('show'); 
                        $("#makeEdit").val($companyId);
                        $makeIdEdit = $("#makeEdit").val();
                      

                        $('.radioModelName').on('change', function() {
                            $modelIdEdit = $(this).val();
                            modelName = $(this).attr('data-id');
                            
                        });

                        $('.company-model-modal-submit-edit').on('click', function() {
                            $('#company-model-modal-edit').modal('hide'); 
                            $("#bodyTypeLogoChoosenEdit").css("display", "none");
                            $("#viewAllCompanyEdit").css("display", "none");
                            var logoPath = "/images/logos/"+ companyLogo;
                            $("#bodyTypeLogosEdit").css("display", "none");
                            $('#bodyTypeLogoChoosenNewValue').append('<div class="col-md-3 col-sm-3"> '
                                + '<img  src="'+ logoPath +' " width=55; height=55;></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ $companyName +'</label></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ modelName +'</label></div>'
                                + '<div class="col-md-3 col-sm-3"><button class="btn remove-image-logo" type="button"><i class="fa fa-window-close" style="color:black; font-size:1.5em;"></i> </button>'
                                + '</div>'
                            );
                            $('.remove-image-logo').on('click', function() {
                                $("#bodyTypeLogoChoosenNewValue").css("display", "none");
                                $("#bodyTypeLogosEdit").fadeIn(200);
                            });

                        });
                    }
                });
                
            });
            
            // Client Side validation for add vehicle
            // $('#validate-vehicle-edit-form').parsley();
            $('#submit-vehicle-form-button').on('click', function(event) {
                var data =  $("#validate-vehicle-edit-form").serialize()+ "&make_id=" + $makeIdEdit + "&mould_id=" + $modelIdEdit + 
                    "&body=" + $bodyEdit + "&body_color=" + carColorCodeValEdit + "&interior_color=" + interiorColorCodeEdit;
                    console.log($modelIdEdit);
                    console.log($makeIdEdit);
                    console.log($bodyEdit);
                    console.log(carColorCodeValEdit);
                    console.log(interiorColorCodeEdit);
                    console.log(data);
                var id = $("#vehicleId").val();
                event.preventDefault();
                //if($('#validate-vehicle-edit-form').parsley().isValid()) {
                    console.log(id);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        url : '/vec-seller/' + id,
                        method:"put",
                        data: data,
                        dataType:"json",
                        success:function(data) {
                            location.reload();
                        }
                    });
                //}
            });
            
        });
    </script>

@endsection

