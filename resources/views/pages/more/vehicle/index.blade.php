@extends('layouts.app')

@section('content')

<style>
.has-search .form-control {
    padding-right: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}

.main {
    width: 50%;
    margin: 20px auto;
}
</style>
    <div class="row"> 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <center>
                <h5>
                المعارض
                </h5>
                <div class="row">
                    <div class="main col-lg-5 col-md-5 ml-auto mr-auto">
                        <div class="form-group has-search ">
                            <form action="{{route('all-vehicle.store')}}" method="POST">
                                @csrf
                                <span class="fa fa-search form-control-feedback"></span>
                                <input type="text" name="seller_name" class="form-control " placeholder="بحث">
                                <button type="submit" hidden></button>
                            </form>
                        </div>
                    </div>  
                </div> 
            </center>

            <div class="row">
                @foreach($sellers as $seller)
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <a href="{{route('all-vehicle.show', $seller->id)}}" class="text-muted">
                        <div class="card p-1 mb-1 bg-white rounded">
                            <div class="card-body ">
                                <div class="d-flex flex-row" style="padding: 0.4rem">
                                    <div style="width:100%; height: 150;" class="col-lg-6 col-md-6 col-sm-6">
                                        @if(!empty(\App\Media::where('fileable_id', '=', $seller->id)->select('file_name')->first()) > 0)
                                            <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                                src="{{'/storage/sellers/'. (\App\Media::where('fileable_id', '=', $seller->id)->select('file_name')->first())->file_name}}">
                                        @endif
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="text-center row">
                                            <h6 style="color: black;font-size:14px">
                                                {{ $seller->seller_name }} 
                                            </h6>
                                        </div>
                                        <div class="row">
                                            <h6 style="color: gray;font-size:13px">
                                                {{ $seller->seller_mobile }} 
                                            </h6>
                                        </div></br>
                                        <div class="row">
                                            <h6 style="color: black;font-size:13px">
                                                {{ $seller->city }} , 
                                                <span style="color: gray;font-size:12px">
                                                    {{ $seller->seller_address }}
                                                </span>
                                            </h6>
                                        </div>
                                        <div class="row">
                                            <h6 style="color: gray; font-size:14px">
                                                {{ $seller->seller_email }} 
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <center>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <form action="tel:{{ $seller->seller_mobile }} ">
                                                    <button class="btn btn-primary btn-block" style="cursor:pointer;background-color:#2e5cb8;color:white;padding:0.1rem" 
                                                        type="submit"><i class="fa fa-phone"></i> اٍتصل</button>
                                                </form>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div> 
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>
@endsection