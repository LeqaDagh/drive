<!-- Companies Models Modal -->
<div class="modal" id="company-model-modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="company-model-title-search modal-title"></h5>
            </div>
            <div class="company-model-body-search modal-body" id="modal-body">
                
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" type="button" class="company-model-modal-submit-search text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>

<!-- Car Color Modal -->
<div class="modal" id="car-color-modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="modal-title">لون السيارة</h5>
            </div>
            <div class="modal-body">
                <ul float="right" id="ul-car-color-search" class="ul-car-color-search list-group list-group-flush">
                    
                    <li style="cursor:pointer" data-id="أسود" value="#000000"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#000000"> 
                            <span  class="color-modal-text"> أسود</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أخضر" value="#0bde16" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#0bde16"> 
                            <span class="color-modal-text"> أخضر</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    
                    <li style="cursor:pointer" data-id="كحلي " value="#0e103d" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#0e103d"> 
                            <span  class="color-modal-text"> كحلي </span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أسود مطفي" value="#120a0a" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#120a0a"> 
                            <span class="color-modal-text"> أسود مطفي</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أزرق مخضر" value="#15b367"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#15b367"> 
                            <span  class="color-modal-text"> أزرق مخضر</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="نيلي" value="#1e14de" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#1e14de"> 
                            <span  class="color-modal-text"> نيلي</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أزرق فاتح" value="#1e9dba" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#1e9dba"> 
                            <span class="color-modal-text"> أزرق فاتح</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                        
                    </li>
                    <li style="cursor:pointer" data-id="أزرق غامق" value="#215769"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#215769"> 
                            <span  class="color-modal-text"> أزرق غامق</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="بنفسجي" value="#24070d" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#24070d"> 
                            <span  class="color-modal-text"> بنفسجي</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="زيتي" value="#2a4221" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#2a4221"> 
                            <span class="color-modal-text"> زيتي</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="بني غامق" value="#362517"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#362517"> 
                            <span  class="color-modal-text"> بني غامق</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أخضر غامق" value="#386352" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#386352"> 
                            <span  class="color-modal-text"> أخضر غامق</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="رمادي" value="#544c4a" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#544c4a"> 
                            <span class="color-modal-text"> رمادي</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="سكني" value="#6b6560"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#6b6560"> 
                            <span  class="color-modal-text"> سكني</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أحمر قاني" value="#700d0d" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#700d0d"> 
                            <span  class="color-modal-text"> أحمر قاني</span> 
                        </i> 
                        <div class="checkafterSearch"></div>  
                    </li>
                    <li style="cursor:pointer" data-id="بني" value="#70431e" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#70431e"> 
                            <span class="color-modal-text"> بني</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="فسفوري" value="#8fe813"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#8fe813"> 
                            <span  class="color-modal-text"> فسفوري</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="خمري" value="#ba0909" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#ba0909"> 
                            <span  class="color-modal-text"> خمري</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="بني برونزي" value="#c48f60" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#c48f60"> 
                            <span class="color-modal-text"> بني برونزي</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="فضي" value="#c7c1bd"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#c7c1bd"> 
                            <span  class="color-modal-text"> فضي</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="برتقالي" value="#de6b18" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#de6b18"> 
                            <span  class="color-modal-text"> برتقالي</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أبيض كريمي" value="#f0e9e9" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#f0e9e9"> 
                            <span class="color-modal-text"> أبيض كريمي</span>
                        </i>    
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أصفر" value="#fcba03"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#fcba03"> 
                            <span  class="color-modal-text"> أصفر</span> 
                        </i> 
                        <div class="checkafterSearch"></div>  
                    </li>
                    <li style="cursor:pointer" data-id="أحمر" value="#ff0000" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#ff0000"> 
                            <span  class="color-modal-text"> أحمر</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="زهري" value="#ffadbb" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#ffadbb"> 
                            <span  class="color-modal-text"> زهري</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="أبيض" value="#ffffff" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle-o" style=""> 
                            <span  class="color-modal-text"> أبيض</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <div id="user"></div>
                </ul>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" id="chooseCarColorSearch" type="submit"
                 class="chooseCarColorSearch text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>


<!-- Interior Color Modal -->
<div class="modal" id="interior-color-modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="modal-title">لون الفرش</h5>
            </div>
            <div class="modal-body">
                <ul float="right" id="ul-interior-color-search" class="ul-interior-color-search list-group list-group-flush">
                    
                    <li style="cursor:pointer" data-id="أسود" value="#000000" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color: #000000"> 
                            <span class="color-modal-text"> أسود</span>
                        </i>  
                        <div class="checkafterSearch"></div> 
                    </li>
                    <li style="cursor:pointer" data-id="بني" value="#5e3b18"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#5e3b18"> 
                            <span  class="color-modal-text"> بني</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="خمري" value="#751010" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#751010"> 
                            <span  class="color-modal-text"> خمري</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="بنفسجي" value="#755196"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#755196"> 
                            <span  class="color-modal-text"> بنفسجي</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <li style="cursor:pointer" data-id="سكني" value="#b5b3b3" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#b5b3b3"> 
                            <span  class="color-modal-text"> سكني</span> 
                        </i>  
                        <div class="checkafterSearch"></div> 
                    </li>
                    <li style="cursor:pointer" data-id="أحمر" value="#c40a0a"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#c40a0a"> 
                            <span  class="color-modal-text"> أحمر</span> 
                        </i>  
                        <div class="checkafterSearch"></div> 
                    </li>
                    <li style="cursor:pointer" data-id="عسلي" value="#c9902e" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle" style="color:#c9902e"> 
                            <span  class="color-modal-text"> عسلي</span> 
                        </i>  
                        <div class="checkafterSearch"></div> 
                    </li>
                    <li style="cursor:pointer" data-id="بيج" value="#fbffd9"  class="list-group-item">
                        <i class="color-modal-icon fa fa-circle-o" style="color:#fbffd9"> 
                            <span  class="color-modal-text"> بيج</span> 
                        </i>  
                        <div class="checkafterSearch"></div> 
                    </li>
                    <li style="cursor:pointer" data-id="أبيض" value="#fcfcfc" class="list-group-item">
                        <i class="color-modal-icon fa fa-circle-o" > 
                            <span  class="color-modal-text"> أبيض</span> 
                        </i>   
                        <div class="checkafterSearch"></div>
                    </li>
                    <div id="user"></div>
                </ul>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" id="chooseinteriorColorSearch" type="submit"
                 class="chooseinteriorColorSearch text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>