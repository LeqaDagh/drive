@extends('layouts.app')

@section('content')

<div class="row" >
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="row">
        @if($vehicles)
            @foreach($vehicles as $vehicle)
            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="{{route('vec-seller.information', $vehicle->id)}}" class="text-muted">
                    <div class="card p-1 mb-1 bg-white rounded">
                        <div class="card-body" style="padding: 0.1rem">
                            <div class="col-lg-12 col-md-12">
                                <div class="d-flex d-row">
                                    <div class="col-lg-12 col-md-12">
                                        <center>
                                            @if(!empty(\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first()) > 0)
                                                <label style="color:#595959; font-weight: bold;">
                                                {{ (\App\Models::where('id', '=', $vehicle->mould_id)->select('name')->first())->name}}, 
                                                </label>
                                            @endif
                                            @if(!empty(\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first()) > 0)
                                                <label style="color:#595959; font-weight: bold;">
                                                {{ (\App\Make::where('id', '=', $vehicle->make_id)->select('name')->first())->name}}
                                                </label>
                                                @if ($vehicle->stared == 'yes')
                                            <i class=" fa fa-star" style="color:#ffcc00"></i>
                                            @endif
                                            @endif
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="d-flex d-row">

                                    <div id="carousel-vehicles-result{{$vehicle->id}}" class="text-center carousel slide" data-ride="carousel">
                                        <center>
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                @foreach(\App\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                                    <li data-target="#carousel-vehicles-result{{$vehicle->id}}" data-slide-to="{{ $loop->index }}" 
                                                        class="{{ $loop->first ? 'active' : '' }}"></li>
                                                @endforeach
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                @foreach(\App\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                                    <div style="width:100%; height: 150px; background-color:white"  class="carousel-item item {{ $loop->first ? ' active' : '' }}" >
                                                        <a href="{{route('vec-seller.all', $vehicle->id)}}">
                                                            <img src="{{'/storage/vehicles/'.$data->file_name}}" width="100%" 
                                                            height="150px" style="object-fit: contain ">
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>

                                            <!-- Controls -->
                                            <a class="right carousel-control" href="#carousel-vehicles-result{{$vehicle->id}}" role="button" data-slide="prev">
                                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="left carousel-control" href="#carousel-vehicles-result{{$vehicle->id}}" role="button" data-slide="next">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </center> 
                                    </div>
                                   <!-- <div style="width:100%; height: 150;" class="col-lg-7 col-md-7 ml-auto mr-auto">
                                    @if(!empty(\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first()) > 0)
                                        <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                            src="{{'/storage/vehicles/'. (\App\Media::where('fileable_id', '=', $vehicle->id)->select('file_name')->first())->file_name}}">
                                    @endif
                                    </div> -->


                                    <div class="col-lg-5 col-md-5">
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                السعر  :  
                                                <span style="color: gray; font-size:10px">{{ $vehicle->price }}  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                القوة  :  
                                                <span style="color: gray; font-size:10px">{{ $vehicle->hp }}  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:12px">
                                                    ناقل الحركة :  
                                                <span style="color: gray; font-size:10px">
                                                @if ($vehicle->gear == 'aut')
                                                أوتماتيك  
                                                @else @if ($vehicle->gear == 'man')
                                                يدوي    
                                                @else نصف أوتماتيك
                                                @endif
                                                @endif
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                    الوقود :  
                                                <span style="color: gray; font-size:10px">
                                                @if ($vehicle->fuel == 'bin')
                                                بنزين  
                                                @else @if ($vehicle->gear == 'des')
                                                ديزل  
                                                @else @if ($vehicle->gear == 'binelec')
                                                بنزين / كهرباء  
                                                @else @if ($vehicle->gear == 'deselec')
                                                ديزل / كهرباء 
                                                @else @if ($vehicle->gear == 'gaz')
                                                غاز    
                                                @else كهرباء
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                سنة الانتاج  :  
                                                <span style="color: gray; font-size:10px">{{ $vehicle->year_of_product }}  </span>
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label style="color: black; font-size:11px">
                                                حالة المركبة :  
                                                <span style="color: gray; font-size:10px">
                                                @if ($vehicle->vehicle_status == 'new')
                                                    جديد  
                                                @else
                                                    مستعمل
                                                @endif
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        @else 
        <div class="alert alert-warning">
            <strong></strong>لا يوجد نتائج!
        </div>
        @endif
        </div>
    </div>
   
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>

@endsection
