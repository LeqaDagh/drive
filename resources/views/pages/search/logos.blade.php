
<div class="float-right row" id="searchCar" class="searchCar">
    <div class="col-md-12" id="viewCompanyTitle">
        <label>الشركة </label>
        <input style="border: 0 !important;  box-shadow: none !important;background-color: white 
            !important;cursor: default !important;" disabled >
        <input id="makeSearch" name="make_id" style="display:none; border: 0 !important; 
            box-shadow: none !important;background-color: white !important;cursor: default !important;"
           type="text"  hidden >
           <input id="modelSearch" name="model_id" type="text"  hidden >
    </div>
</div>
<div class="appendedModelId" style="display:none">

</div>
<div id="searchBreakAllCompany">
    </br></br>
</div>

<div class=" justify-content-around flex-wrap row">
    @foreach($firstMakes as $key => $data)
    <div class="col-lg-3 col-md-3 col-sm-3 logos">
        <div class=" card-stats">
            <center>
                <div class="card-body ">
                    <a class="searchModelsCars" data-id="{{$data->id}}" >
                        <div >
                            <img src="{{url('/images/logos/'.$data->logo)}}" width={{$data->web_width}}; 
                            height={{$data->web_height}};>
                        </div>
                    </a>
                </div>
                <div class="stats">
                    {{$data->name}}
                </div>
            </center>
        </div>
    </div>
    @endforeach
</div>

<!--<center>
<div class="col-lg-12">
    <div id="searchBodyTypeLogos" class="d-flex justify-content-around  d-inline-flex flex-wrap row">
        @foreach($firstMakes as $key => $data)
        <div class="p-1 col-lg-3">
            <div class=" card-stats">
                <center>
                    <div class="card-body ">
                        <a class="searchModelsCars" data-id="{{$data->id}}" >
                            <div >
                                <img src="{{url('/images/logos/'.$data->logo)}}" width={{$data->web_width}}; 
                                height={{$data->web_height}};>
                            </div>
                        </a>
                    </div>
                    <div class="stats">
                        {{$data->name}}
                    </div>
                </center>
            </div>
        </div>
        @endforeach

    </div></br>
</div>
</center>-->
</br>
<div class="row" >
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div id="searchBodyTypeLogoChoosen" >
            
        </div></br>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>


<div class="row" id="viewAllCompanySearch">
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 ml-auto mr-auto">
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="toggle btn btn-primary btn-block"  style="background:#0070B0" data-toggle="modal" 
                    data-target="#logos-modal-search">
                    <span>  اظهار كافة الشركات </span><i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div></br>


<div class="modal" id="logos-modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="modal-title">الشركات</h5>
            </div>
            <div class="modal-body">
                <div class="justify-content-around flex-wrap row">
                    @foreach($makes as $key => $data)
                    <div class="col-lg-3 col-md-3 col-sm-3 logos">
                        <div class=" card-stats">
                            <center>
                                <div class="card-body ">
                                    <a class="searchModelsCars" data-id="{{$data->id}}">
                                        <div >
                                            <img src="{{url('/images/logos/'.$data->logo)}}" width={{$data->web_width}}; 
                            height={{$data->web_height}};>
                                        </div>
                                    </a>
                                </div>
                                <div class="stats">
                                {{$data->name}}
                                </div>
                            </center>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" type="button" class="text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(window).resize(function() {

        if ($(this).width() < 500) {
            $('.logos').removeClass( "col-lg-3 col-md-3 col-sm-3" ).addClass( "p-1" );
        } else if ($(this).width() > 500) {
            $('.logos').removeClass( "p-2" ).addClass( "col-lg-3 col-md-3 col-sm-3" );
        }

    });

</script>