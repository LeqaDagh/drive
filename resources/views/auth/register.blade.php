@extends('layouts.app', [
'class' => 'register-page',
])
<title>انشاء حساب شخصي</title>
@section('content')
<link rel="stylesheet" href="{{url('/css/util.css')}}">
<link rel="stylesheet" href="{{url('/css/main.css')}}">

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100" style="padding: 0.8rem">
            <form id="validate-personal-register-form" method="POST" action="{{ route('register') }}">
                @csrf
                <center>
                    <span class="text-center p-b-22">
                    {{ __('حساب شخصي') }}
                    </span>
                </center></br>

                <div class="wrap-input100  validate-input p-t-3">
                    <input name="name" type="text" class="input100 text-center" value="{{ old('name') }}"
                    data-parsley-required-message="الاسم مطلوب" required autofocus>

                    <span class="focus-input100 text-center " data-placeholder="الاسم"></span>
                    @if ($errors->has('name'))
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <input name="mobile" type="text" class="input100 text-center"value="{{ old('mobile') }}" 
                    data-parsley-required-message="الجوال مطلوب" data-parsley-minlength="9" 
                    data-parsley-minlength-message="أقل طول للرقم 9 خانات" 
                    data-parsley-maxlength="10" data-parsley-maxlength-message="أكبر طول للرقم 10 خانات" data-parsley-type="digits"
                    data-parsley-type-message="رقم الجوال فقط أرقام" required >

                    <span class="focus-input100 text-center " data-placeholder="رقم المحمول"></span>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <select class="input100 text-center" name="region_id" data-parsley-required-message="المنطقة مطلوب" style="border:none; text-align-last: center;" required>
                        <option selected disabled>المنطقة</option>
                        @foreach($regions as $key => $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="address" type="text" class="input100 text-center" value="{{ old('address') }}" data-parsley-required-message="العنوان مطلوب" required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="العنوان"></span>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="email" type="email" class="input100 text-center" value="{{ old('email') }}" data-parsley-required-message="البريد الالكتروني مطلوب" required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="البريد الالكتروني"></span>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" 
                    type="password" data-parsley-required-message="كلمة المرور مطلوب"  required>
                    <span class="focus-input100 text-center " data-placeholder="كلمة المرور"></span>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" data-parsley-required-message="تأكيد كلمة المرور مطلوب" 
                     type="password" required>
                    <span class="focus-input100 text-center " data-placeholder="تأكيد كلمة المرور"></span>
                    @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" style="display: block;" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" id="submit-register-form" class="login100-form-btn btn-round mb-1">
                            تسجيل
                        </button>
                    </div>
                </div>

                <div class="text-center ">
                    <span class="txt1">
                        لديك حساب!
                    </span>

                    <a class="" href="{{ route('login') }}">
                        تسجيل دخول
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection