@extends('layouts.app', [
    'class' => 'register-seller-page',
])

@section('content')
<link rel="stylesheet" href="{{url('/css/util.css')}}">
<link rel="stylesheet" href="{{url('/css/main.css')}}">

<div class="limiter" >
    <div class="container-login100" >
        <div class="wrap-login100" style="padding: 0.8rem">
            <form id="validate-seller-register-form" method="POST" enctype="multipart/form-data" action="{{ route('seller') }}">
            @csrf
                <center>
                    <span class="text-center p-b-22">
                        {{ __('حساب معرض / شركة') }}
                    </span>
                </center>
                

                <div class="text-center p-t-5">
                    <i class="material-icons" id="locationOff" style="color:red; font-size:25px"> location_off</i>
                </div>
                <div class="text-center ">
                    <input type="hidden" name="seller_lat" id="seller_lat" value="{{ old('seller_lat') }}" data-parsley-required-message="احداثي الطول والعرض  مطلوب" required>
                    <input type="hidden" name="seller_lon" id="seller_lon" value="{{ old('seller_lon') }}" data-parsley-required-message="احداثي الطول والعرض  مطلوب" required>
                    <button class="p-1 btn btn-primary btn-round p-t-5" type="button" onclick="getLocation()">
                      <span style="font-size:13px">
                        تحديد المكان  
                      </span>
                    </button>
                    
                    <i class="fa fa-check" id="check" style="color:green; font-size:20px; display: none;"> </i>
                </div>
               <div class="p-t-15"></div>
                <div class="wrap-input100  validate-input p-t-5">
                    <input name="name" type="text" class="input100 text-center" 
                       value="{{ old('name') }}" data-parsley-required-message="اسم المعرض مطلوب"required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="اسم المعرض"></span>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <input name="mobile" type="text" class="input100 text-center"value="{{ old('mobile') }}" 
                    data-parsley-required-message="رقم المعرض مطلوب" data-parsley-minlength="9" 
                    data-parsley-minlength-message="أقل طول للرقم 9 خانات" 
                    data-parsley-maxlength="10" data-parsley-maxlength-message="أكبر طول للرقم 10 خانات" data-parsley-type="digits"
                    data-parsley-type-message="رقم المعرض فقط أرقام" required >
                    <span class="focus-input100 text-center " data-placeholder="رقم المعرض"></span>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <input name="seller_name" type="text" class="input100 text-center" 
                       value="{{ old('seller_name') }}" data-parsley-required-message="اسم مدير المبيعات مطلوب"required autofocus>
                    <span class="focus-input100 text-center " data-placeholder="اسم مدير المبيعات"></span>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <input name="seller_mobile" type="text" class="input100 text-center"value="{{ old('seller_mobile') }}" 
                    data-parsley-required-message="رقم مدير المبيعات مطلوب" data-parsley-minlength="9" 
                    data-parsley-minlength-message="أقل طول للرقم 9 خانات" 
                    data-parsley-maxlength="10" data-parsley-maxlength-message="أكبر طول للرقم 10 خانات" data-parsley-type="digits"
                    data-parsley-type-message="رقم مدير المبيعات فقط أرقام" required >
                    
                    <span class="focus-input100 text-center " data-placeholder="رقم مدير المبيعات"></span>
                </div>

                <div class="wrap-input100  validate-input p-t-5">
                    <select class="input100 text-center" name="region_id" data-parsley-required-message="المنطقة مطلوب"
                    style="border:none; text-align-last: center;"required >
                        <option selected disabled>المنطقة</option>
                        @foreach($regions as $key => $data)
                            <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="address" type="text" class="input100 text-center " 
                       value="{{ old('address') }}" data-parsley-required-message="العنوان مطلوب" required autofocus>
                       <span class="focus-input100 text-center " data-placeholder="العنوان"></span>
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input name="email" type="email" class="input100 text-center "
                       value="{{ old('email') }}" data-parsley-required-message="البريد الالكتروني مطلوب" required autofocus>
                       <span class="focus-input100 text-center " data-placeholder="البريد الالكتروني"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center {{ $errors->has('password') ? ' is-invalid' : '' }}" 
                    name="password" type="password" data-parsley-required-message="كلمة المرور مطلوب" required>
                    <span class="focus-input100 text-center " data-placeholder="كلمة المرور"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input p-t-5">
                    <input class="input100 text-center {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" 
                    name="password_confirmation" data-parsley-required-message="تأكيد كلمة المرور مطلوب"  type="password" required>
                    <span class="focus-input100 text-center " data-placeholder="تأكيد كلمة المرور"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

 
                <input type="file" name="photos[]" id="ImagesCompany" style="display: none"
                    class="text-center form-control" multiple="multiple">
                
                <div class="text-center " id="companyImages" >
                        
                </div>
               

                <div class="control-group increment" >
                    <span id="spnFilePath"></span>
                    
                    <div class="text-center input-group-btn"> 
                        <button class="add-company-images" id="add-company-images" type="button">
                            <i class="fa fa-picture-o" style=" font-size:25px"></i>
                        </button>
                    </div>
                </div>


                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" id="submit-seller-register-form" class="login100-form-btn btn-round mb-1">
                            {{ __('تسجيل ') }}
                        </button>
                    </div>
                </div>

                <div class="text-center ">
                    <span class="txt1">
                     لديك حساب!
                    </span>

                    <a class="" href="{{ route('login') }}">
                    تسجيل دخول
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    let seller_lat = document.getElementById("seller_lat");
    let seller_lon = document.getElementById("seller_lon");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            seller_lat.value = '';
            seller_lon.value = '';
        }
    }

    function showPosition(position) {
        seller_lat.value = position.coords.latitude;
        seller_lon.value = position.coords.longitude;
        document.getElementById("locationOff").innerHTML ="location_on";
        document.getElementById("locationOff").style.color = 'green';
    }

    $(document).ready(function() {

        var fileupload = $("#ImagesCompany");
        var count = 0;

        $(".add-company-images").click(function() { 
            fileupload.click();
        });

        fileupload.change(function () {
            readURL(this);
        });

        function readURL(input) {
            var filesAmount = input.files.length;
        
            if (input.files ) {
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {

                        $('#companyImages').append('<div class="control-group d-flex flex-row"><div class=" col-lg-6 col-sm-6 "> '
                        + '<div id="picturebox"><img src=' + e.target.result + ' style="width:54px; height:54px;">'
                        + '</div></div><div class="col-lg-6 col-sm-6" id="imagebuttonadd" >'
                        + '<div class="input-group-btn"> '
                        + '<button class="btn remove-image" type="button"><i class="fa fa-minus-circle" style="color:#0875ba; font-size:0.7em;"></i> </button>'
                        + '</div></div></div>');
                        
                    }
                    
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $('#companyImages').on('click', '.remove-image', function(){
            $(this).parents(".control-group").remove();
        });
    });
</script>
@endsection

