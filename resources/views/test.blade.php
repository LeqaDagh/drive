<!doctype html>
<html lang="en">
  <head>
  	<title>Auto And Drive</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('/css/style.css')}}">
        <link rel="stylesheet" href="{{url('/css/bootstrap.min.css')}}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
     rel="stylesheet">
        
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar" class="order-last" class="img" style="background-image: url(images/bg_1.jpg);">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
        <div class="">
		  		<h1><a href="index.html" class="logo">الاسم <span></span></a></h1>
	        <ul class="list-unstyled components mb-5">
	          <li dir="rtl" >
	            <a href="#"><span class="fa fa-home mr-3"></span>  الرئيسية </a>
	          </li>
	          <li dir="rtl">
	              <a href="#"><span class="fa fa-search mr-3"></span>  بحث  </a>
	          </li>
	          <li dir="rtl" class="active">
              <a href="#"><span class="fa fa-edit mr-3"></span>  اضافة  </a>
	          </li>
	          <li dir="rtl">
              <a href="#"><span class="fa fa-cogs mr-3"></span> مقارنة  </a>
	          </li>
	          <li dir="rtl">
              <a href="#"><span class="fa fa-ellipsis-h mr-3"></span> المزيد </a>
	          </li>
	        </ul>

	      </div>

    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
          
        <h2 class="mb-4"> </h2>
        <div dir="rtl" class="content">
    <div ></div><br>
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="POST" >
                @csrf
                    <div class="card ">
                        <div class="card-header ">
                            <center>
                                <h5>
                                اضافة مركبة
                                </h5>
                            </center>
                        </div>
                        <div class="card-body ">
                            <div class="float-right" id="addCar" class="addCar">
                                الشركة
                            </div></br>
                            <div class="row">
                                @for ($i = 1; $i <= 12; $i++)
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <div class=" card-stats">
                                        <center>
                                            <div class="card-body ">
                                                <a href="#" data-toggle="modal" data-target="#company-model-modal">
                                                    <div >
                                                        <img src="{{url('/images/opel.png')}}" width=55>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="card-footer ">
                                                <div class="stats">
                                                    Benz
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                                @endfor

                                @for ($i = 1; $i <= 12; $i++)
                                <div class="target d-none col-lg-2 col-md-3 col-sm-3">
                                    <div class=" card-stats">
                                        <center>
                                            <div class="card-body ">
                                                <a href="#" data-toggle="modal" data-target="#login-modal">
                                                    <div >
                                                    <img src="{{url('/images/opel.png')}}" width=55>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="card-footer ">
                                                <div class="stats">
                                                    Opel
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                                @endfor
                            </div></br>

                            
                            <div class="row">
                                <div class="col-lg-12 ml-auto mr-auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="#" class="toggle btn btn-primary btn-block" >
                                                <span>  اظهار كافة الشركات </span><i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div></br>


                            <div class="float-right" id="addCar" class="addCar">
                                الهيكل
                            </div></br>
                            <div class="row">
                                @for ($i = 1; $i <= 9; $i++)
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <div class=" card-stats">
                                        <center>
                                            <div class="card-body ">
                                                <a  href="">
                                                    <div >
                                                    <img src="{{url('/images/offroad.png')}}" width=60>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="card-footer ">
                                                <div class="stats">
                                                    Offroad
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                                @endfor
                            </div></br>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                    <div class="form-group">
                                        <input class="text-center form-control"
                                        placeholder="{{ __('عنوان اضافي') }}" type="text" name=""
                                        value="" >
                                        
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control" id="datepicker"
                                        name="" placeholder="{{ __('سنة الانتاج') }}" type="text" >
                                
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control" id="datepicker1"
                                        placeholder="{{ __('سنة الترخيص') }}" type="text"  >
                                    </div> 
                                    <div class="form-group">
                                        <input class="text-center form-control" 
                                        name="" placeholder="{{ __('السعر') }}" 
                                        type="text" >
                                
                                    </div>
                                    <div class="form-group">
                                        <input class="text-center form-control"
                                        placeholder="{{ __('الدفعة الأولى') }}" type="text" name="" 
                                        value=""  >
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __('طريقة عرض السعر') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">عرض السعر</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">عدم عرض السعر</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">عرض الدفعة الأولى</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="text-center form-control" 
                                        placeholder="{{ __('القوة') }}" type="text"  >
                                    </div> 
                                    <div class="form-group">
                                        <input class="text-center form-control" 
                                        placeholder="{{ __(' عدد الأحصنة') }}" type="number"  >
                                    </div>    
                                    <div class="form-group">
                                        <input class="text-center form-control" 
                                        placeholder="{{ __('المسافة المقطوعة') }}" type="number"  >
                                    </div>   

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __('ناقل الحركة') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">أوتماتيك</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">عادي</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">نصف أوتماتيك</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __('الوقود') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">بنزين</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">ديزل</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">بنزين / كهرباء</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">ديزل / كهرباء</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">غاز</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">كهرباء</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                    

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __(' نظام الدفع') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">4X4</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">2X4</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>
                                  

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __(' نوع الدفع') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">أمامي</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">خلفي</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __(' طريفة الدفعات') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">نقدا</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">عن طريق البنك</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">تقسيط مباشر</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">دفعة أولى + شيكات شخصية</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __(' عدد المقاعد') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">2+1</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">3+1</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">4+1</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">5+1</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">6+1</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <input class="form-control"
                                        placeholder="{{ __(' حالة المركبة') }}" type="text" 
                                        disabled >
                                        
                                    </div> 

                                    <div class="d-flex flex-row">
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">جديد</span>
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="p-1">
                                            <div class="form-group">
                                                <a  class="btn bg-secondary btn-block" >
                                                    <span class="choice-button-text">مستعمل</span>
                                                </a>
                                            </div> 
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-6 col-sm-4 ml-auto mr-auto">

                                            <div class="form-group">
                                                <label for=""> لون السيارة</label>
                                            </div> 
                                            <div class="form-group">
                                                <a class="btn btn-light btn-block" data-toggle="modal" 
                                                data-target="#car-color-modal" id="color-car"
                                                style="border-radius: 30px;">
                                                    <i class="fa fa-check-circle-o">
                                                        <span id="color-car-text" > لم يحدد</span>
                                                    </i> 
                                               
                                                </a>
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 col-sm-4 ml-auto mr-auto">
                                            <div class="form-group">
                                                <label for=""> لون الفرش</label>
                                            </div> 
                                            <div class="form-group">
                                                <a class=" btn btn-light btn-block" style="border-radius: 30px;">
                                                    <i class="fa fa-check-circle-o">
                                                        <span> لم يحدد</span>
                                                    </i> 
                                                </a>
                                            </div> 
                                        </div>

                                    </div>

                                </div>
                                <div class="col-lg-4 col-md-6 ml-auto mr-auto">

                                       
                                </div>
                            </div>

                            

                            <div class="row">
                                <div class="col-lg-12 ml-auto mr-auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            
                                            <a class="toggle btn btn-secondary btn-block" style="cursor:pointer">
                                                <span class="toggle-text"> خيارات أخرى</span><i id="toggleicon" class="fa fa-angle-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div></br>

                            @include('pages.vehicle.more') 

                        <div class="row">
                            <div class="col-lg-6 ml-auto mr-auto">
                                <center>
                                    <button class="btn btn-danger btn-block" >
                                            <span>اِضافة </span>
                                        </button>
                                
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @include('pages.vehicle.modals') 

        
    </div>
</div>

    <script src="{{url('/js/jquery.min.js')}}"></script>
    <script src="{{url('/js/popper.js')}}"></script>
    
    <script src="{{url('/js/main.js')}}"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('/js/bootstrap.min.js')}}"></script>
    <script>
        
        $("#datepicker").datepicker( {
            minViewMode: 2,
            format: 'yyyy'
        });
        $("#datepicker1").datepicker( {
            minViewMode: 2,
            format: 'yyyy'
        });

        $(function(){
            $("#target").css("display","none");
            $(".toggle").click(function(){
                $("#target").slideToggle(1000);
                $("#toggleicon").toggleClass('fa fa-angle-up').toggleClass('fa fa-angle-down');
            })  
        })

        $("li").click(function () {  
            var current = this;     
            var a = $(this).attr("value");
            //var name = $(this).arr
            if(a.length > 0) {
                $(this).toggleClass('fa fa-check');
            }

            $("#chooseCarColor").click(function () {  
               
                $("#car-color-modal").modal('hide');
                $("#color-car").css('background-color',a);
                $("#color-car").css('color','white');
                $("#color-car-text").replaceWith(a);  
                $(".ul-car-color li").removeClass('fa fa-check');
            });
            
        });
    </script>

  </body>
</html>
        
    
