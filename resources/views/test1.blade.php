<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="{{url('/css/util.css')}}">
        <link rel="stylesheet" href="{{url('/css/main.css')}}">
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
                        <img src="{{url('/images/logo.jpeg')}}" width="150">
					</span>

                    <div class="wrap-input100 validate-input">
                        <input class="input100 text-center form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                            type="email" name="email" value="{{ old('email') }}" required autofocus>
                            <span class="focus-input100 text-center" data-placeholder="{{ __('البريد الالكتروني') }}"></span>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input">
                        <input class="input100 text-center form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
                        name="password" type="password" required>
                        
						<span class="focus-input100 text-center" data-placeholder="{{ __('كلمة المرور') }}"></span>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn btn btn-info btn-round mb-1">
                                {{ __('تسجيل دخول') }}
                            </button>

						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
                        ليس لديك حساب! سجل الان
                        </span>
                        
                        
                    </div>

                    <div class="text-center p-t-115">
                        <a class="btn btn-info btn-round" href="{{ route('register') }}" >
                            <i class="fa fa-user-plus"></i>  حساب شخصي
                        </a>
                        <a class="btn btn-info btn-round " href="{{ route('register') }}" >
                            <i class="fa fa-car"></i>  حساب معرض / شركة
                        </a>
                        
                    </div>
                    
                    <div class="text-center p-t-115">
						<span class="txt1">
                        AutoAndDrive.com 						
                        </span>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
</body>
</html>