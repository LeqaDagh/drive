<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Media;
use App\User;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Image;

class SellerController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the register page for seller.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $regions = Region::all();
        //dd($regions);
        return view('auth.register-seller')->with('regions', $regions);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'seller_lat' => ['required'],
            'seller_lon' => ['required'],
            'mobile' => ['required'],
            'seller_name' => ['required'],
            'seller_mobile' => ['required'],
            'region_id' => ['required'],
            'address' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'seller_lat' => $request['seller_lat'],
            'seller_lon' => $request['seller_lon'],
            'mobile' => $request['mobile'],
            'seller_name' => $request['seller_name'],
            'seller_mobile' => $request['seller_mobile'],
            'region_id' => $request['region_id'],
            'address' => $request['address'],
            'seller_region_id' => $request['region_id'],
            'seller_address' => $request['address'],
            'seller_order' => 0,
            'max_vehicles' => 0,
            'seller_email' => $request['email'],
            'can_show_first_payment' => 'no',
            'can_ignor_price' => 'no',
            'publish_status' => 'no',
            'user_status' => 'yes',
            'group' => 'se',
            'remember_token' => null,
            'api_token' => null,
        ]);

        if($request->photos) {
            foreach ($request->photos as $photo) {

                $original_name = $photo->getClientOriginalName();
                $extra_name  = uniqid().'_'.time().'_'.uniqid().'.'.$photo->extension();
                $encoded_base64_image = substr($photo, strpos($photo, ',') + 1);
                $resized_image = Image::make($photo->getRealPath());
                $relPath = 'storage/sellers/'; //your path inside public directory
                if (!file_exists(public_path($relPath))) { //Verify if the directory exists
                    mkdir(public_path($relPath), 666, true); //create it if do not exists
                }
                $resized_image->save(public_path('storage/sellers/'.$extra_name));
                $path = public_path('storage/sellers/'.$extra_name);
                $logo = public_path('opa_logo.png'); 
                $v_logo = public_path('v_logo.png');    
                $img = Image::make($path);
                $img->insert($logo, 'bottom-left', 10, 10);
                $pad = intval(($img->height()-500)/2);
                $img->insert($v_logo, 'top-right',10,$pad);
                
                $img->resize(800,null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $img->save($path);

                Media::create([
                    'disk_name' => 'sellers',
                    'file_name' => $extra_name,
                    'file_size' => '11',
                    'sort_order' => '1',
                    'field' => 'photos',
                    'description' => '',
                    'title' => $user->name,
                    'content_type' =>'11',
                    'fileable_id' =>  $user->id,
                    'fileable_type' => 'App\Make'
                ]);

            }
        }

        $this->guard()->login($user);

        return redirect('/home');
    }
}
