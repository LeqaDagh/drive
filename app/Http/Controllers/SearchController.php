<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\User;
use App\Equipment;
use App\Make;
use App\Models;
use App\Region;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the search page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $makes = Make::all();
        $firstMakes = Make::all()->take(8);
        $regions = Region::all();

        return view('pages.search.index', compact('makes', 'firstMakes', 'regions'));
    }

    // search 
    public function search(Request $request)
    {
       // dd($request->all());
       // $usersId = User::where('region_id', '=', $request['region_id'])->get('id');
        $query = Vehicle::query()
            ->where('is_deleted', 'no')
            ->orderBy('stared','desc');

        $searchOption = $request->search_option;
        if($searchOption) {
            if($searchOption == "ascPrice") {
                $query->orderBy('price','asc');
            } else if($searchOption == "desPrice") {
                $query->orderBy('price','desc');
            } else if($searchOption == "ascYear") {
                $query->orderBy('year_of_product','asc');
            } else if($searchOption == "desYear") {
                $query->orderBy('year_of_product','desc');
            } 
        }

        $queryUsers = User::query();
        $region = $request->has('region_id');
        if($region) {
            $queryUsers->where(function($queryUsers) use ($request) {
                foreach ($request->region_id as $region) {
                    $queryUsers->orWhere('region_id', $region);
                }
            });
        }

      /*  $makeId = $request->make_id;
        if ($makeId) {
            $query->where('make_id', '=', $makeId);
        }*/

        if($request->model_id) {
            $model_id = json_decode($request->model_id, true);
            if($model_id) {
                $query->where(function($query) use ($model_id) {
                    foreach ($model_id as $modelId) {
                        $query->orWhere('mould_id', $modelId);
                    }
                });
            }
        }
        
        $allModel = $request->all_model;
        if($allModel) {
            $logoName = Make::where("id", $allModel)->pluck("logo");
            $models = Models::where("logo", $logoName)->pluck("id");
            $query->where(function($query) use ($models) {
                foreach ($models as $item) {
                    $query->orWhere('mould_id', $item);
                }
            });
        }
        
        if($request->has('body')) {
            $query->where(function($query) use ($request) {
                foreach ($request->body as $body) {
                    $query->orWhere('body', $body);
                }
            });
        }

        $yearFrom = $request['range_year_from'];
        $yearTo = $request['range_year_to'];
        if ($yearFrom) {
            $query->whereBetween('year_of_product', [$yearFrom, $yearTo]);
        }

        $priceFrom = $request['range_price_min'];
        $priceTo = $request['range_price_max'];
        if ($priceFrom) {
            $query->whereBetween('price', [$priceFrom, $priceTo]);
        }

        $powerFrom = $request['range_power_min'];
        $powerTo = $request['range_power_max'];
        if ($powerFrom) {
            $query->whereBetween('power', [$powerFrom, $powerTo]);
        }

        $distanceFrom = $request['range_distance_min'];
        $distanceTo = $request['range_distance_max'];
        if ($distanceFrom) {
            $query->whereBetween('mileage', [$distanceFrom, $distanceTo]);
        }

        $hpFrom = $request['range_hp_min'];
        $hpTo = $request['range_hp_max'];
        if ($hpFrom) {
            $query->whereBetween('hp', [$hpFrom, $hpTo]);
        }

        $body_color = $request['body_color'];
        if ($body_color) {
            $query->where('body_color', $body_color);
        }

        $interior_color = $request['interior_color'];
        if ($interior_color) {
            $query->where('interior_color', $interior_color);
        }

        $gear = $request['gear'];
        if ($gear) {
            $query->where('gear', $gear);
        }

        $fuel = $request['fuel'];
        if ($fuel) {
            $query->where('fuel', $fuel);
        }

        $drivetrain_system = $request['drivetrain_system'];
        if ($drivetrain_system) {
            $query->where('drivetrain_system', $drivetrain_system);
        }

        $drivetrain_type = $request['drivetrain_type'];
        if ($drivetrain_type) {
            $query->where('drivetrain_type', $drivetrain_type);
        }

        $price_type = $request['price_type'];
        if ($price_type) {
            $query->where('price_type', $price_type);
        }

        $num_of_seats = $request['num_of_seats'];
        if ($num_of_seats) {
            $query->where('num_of_seats', $num_of_seats);
        }

        $vehicle_status = $request['vehicle_status'];
        if ($vehicle_status) {
            $query->where('vehicle_status', $vehicle_status);
        }

        $customs_exemption = $request['customs_exemption'];
        if ($customs_exemption) {
            $query->where('customs_exemption', $customs_exemption);
        }

        $license_date = $request['license_date'];
        if ($license_date) {
            $query->where('license_date', $license_date);
        }

        $previous_owners = $request['previous_owners'];
        if ($previous_owners) {
            $query->where('previous_owners', $previous_owners);
        }

        $origin = $request['origin'];
        if ($origin) {
            $query->where('origin', $origin);
        }

        $num_of_keys = $request['num_of_keys'];
        if ($num_of_keys) {
            $query->where('num_of_keys', $num_of_keys);
        }

        $desc = $request['desc'];
        if ($desc) {
            $query->where('desc', $desc);
        }

        // equinpments 
        $queryEquipments = Equipment::query();

        if($request->has('payment_method')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->payment_method as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        
        $ext_int_furniture = $request['ext_int_furniture'];
        if ($ext_int_furniture) {
            $queryEquipments->where('equipment_value', $ext_int_furniture);
        }

        $ext_int_sunroof = $request['ext_int_sunroof'];
        if ($ext_int_sunroof) {
            $queryEquipments->orWhere('equipment_value', $ext_int_sunroof);
        }

        $ext_ext_rims = $request['ext_ext_rims'];
        if ($ext_ext_rims) {
            $queryEquipments->orWhere('equipment_value', $ext_ext_rims);
        }

        if($request->has('ext_int_seats')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_int_seats as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }

        if($request->has('ext_int_steering')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_int_steering as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }

        if($request->has('ext_int_screens')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_int_screens as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_int_glass')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_int_glass as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_int_other')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_int_other as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_ext_light')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_ext_light as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_ext_mirrors')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_ext_mirrors as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_ext_cameras')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_ext_cameras as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_gen_other')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_gen_other as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_ext_sensors')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_ext_sensors as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }
        if($request->has('ext_ext_other')) {
            $queryEquipments->where(function($queryEquipments) use ($request) {
                foreach ($request->ext_ext_other as $item) {
                    $queryEquipments->orWhere('equipment_value', $item);
                }
            });
        }

        foreach($queryEquipments as $queryEquipment) {
            $query->where('id', $queryEquipment->vehicle_id);
        }
        
        if($region) {
            foreach($queryUsers as $queryUser) {
                $query->where('user_id', $queryUser->region_id);
            }
        } 

        if($searchOption) {
            $vehicles = $query->get();
        } else {
            $vehicles = $query->orderBy('price','asc')
            ->get();
        }
        
        return view('pages.search.result', compact('vehicles'));
    }

}
