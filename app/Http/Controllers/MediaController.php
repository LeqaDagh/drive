<?php

namespace App\Http\Controllers;

use App\Make;
use App\Models;
use App\Media;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Image;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }


    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $models
     * @return \Illuminate\View\View
     */
    public function show($id) { 
  
        $vechile = Make::where('id', $id)->pluck('name');

        return  $makes;

    }

    /**
     * Create a new Media for Vehicle.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    public function store(Request $request) {

        $vehicle = Vehicle::findOrFail($request->vehcile_id);

        foreach ($request->photos as $photo) {

            $original_name = $photo->getClientOriginalName();
            $extra_name  = uniqid().'_'.time().'_'.uniqid().'.'.$photo->extension();
            $encoded_base64_image = substr($photo, strpos($photo, ',') + 1);
            $resized_image = Image::make($photo->getRealPath());
            $resized_image->save(public_path('storage/vehicles/'.$extra_name));
            $path = public_path('storage/vehicles/'.$extra_name);
            $logo = public_path('opa_logo.png'); 
            $v_logo = public_path('v_logo.png');    
            $img = Image::make($path);
            $img->insert($logo, 'bottom-left', 10, 10);
            $pad = intval(($img->height()-500)/2);
            $img->insert($v_logo, 'top-right',10,$pad);
            
            $img->resize(800,null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save($path);

            Media::create([
                'disk_name' => 'vehicles',
                'file_name' => $extra_name,
                'file_size' => '11',
                'sort_order' => '1',
                'field' => 'photos',
                'description' => '',
                'title' => $vehicle->extra_title,
                'content_type' =>'11',
                'fileable_id' =>  $vehicle->id,
                'fileable_type' => 'App\Vehicle'
            ]);

        }
  
        return redirect()->back();
    }

    /**
     * Add images for Vehicle.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    public function addMoreImages(Request $request) {

        $vehicle = Vehicle::findOrFail($request->vehcile_id);
        
        foreach ($request->photos as $photo) {

            $original_name = $photo->getClientOriginalName();
            $extra_name  = uniqid().'_'.time().'_'.uniqid().'.'.$photo->extension();
            $encoded_base64_image = substr($photo, strpos($photo, ',') + 1);
            $resized_image = Image::make($photo->getRealPath());
            $resized_image->save(public_path('storage/vehicles/'.$extra_name));
            $path = public_path('storage/vehicles/'.$extra_name);
            $logo = public_path('opa_logo.png'); 
            $v_logo = public_path('v_logo.png');    
            $img = Image::make($path);
            $img->insert($logo, 'bottom-left', 10, 10);
            $pad = intval(($img->height()-500)/2);
            $img->insert($v_logo, 'top-right',10,$pad);
            
            $img->resize(800,null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save($path);

            Media::create([
                'disk_name' => 'vehicles',
                'file_name' => $extra_name,
                'file_size' => '11',
                'sort_order' => '1',
                'field' => 'photos',
                'description' => '',
                'title' => $vehicle->extra_title,
                'content_type' => '11',
                'fileable_id' =>  $vehicle->id,
                'fileable_type' => 'App\Vehicle'
            ]);

        }
  
        return redirect()->back();
    }

    /**
     * Delete media.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function destroy($id)
    {
        $media = Media::findOrFail($id);
        $media->delete();

        return back();
    }
    
    /**
     * Get specific media.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function getImage($id)
    {
        $photo = Media::findOrFail($id);
        
        $photo['thumb_path'] = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

		return $photo;
    }
}