<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\User;
use App\Media;
use App\Models;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $vehicles =  Vehicle::Join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->where('stared', '=', 'yes')
            ->where('is_deleted', 'no')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'moulds.name as model_name')
            ->orderBy('stared','desc')
            ->get();
        
        $vehicleIds = Vehicle::where('slider_id', '!=', "NULL")->get();
        
        return view('home', compact('vehicles', 'vehicleIds'));
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id) { 

        
        
    }
}
