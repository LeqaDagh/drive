<?php

namespace App\Http\Controllers;

use App\Make;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class ModelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }


    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $models
     * @return \Illuminate\View\View
     */
    public function show($id) { 
  
        $models = Models::where('parent', $id)->get();

        return  $models;
    }

}