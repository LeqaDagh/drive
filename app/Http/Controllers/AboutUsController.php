<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AboutUsController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the about-us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $aboutUs = Setting::where('settings_key', 'about_us')->get();

        return view('pages.more.about-us', compact('aboutUs'));
    }

}
