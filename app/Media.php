<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Media extends Model
{
    protected $table = 'media'; 
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'disk_name', 'file_name', 'file_size', 'content_type', 'title', 'description', 'field', 
        'fileable_id', 'fileable_type', 'sort_order'
    ];
}
