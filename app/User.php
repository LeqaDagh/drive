<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array '' => 0,
         
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'seller_lat', 'seller_lon', 'seller_name', 'seller_mobile', 
        'region_id', 'address', 'seller_region_id', 'seller_address', 'seller_order', 'max_vehicles', 'seller_email',
        'can_show_first_payment', 'can_ignor_price', 'publish_status', 'user_status', 'group', 'api_token',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
